package id.co.bsm.util;

import org.apache.camel.Exchange;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RequestUtil {
    private static final String HTTP_URI = "CamelHttpUri";

    public static String getRequestType(Exchange exchange) {
        List<String> type = new ArrayList<String>(Arrays.asList(exchange.getIn()
                .getHeader(HTTP_URI).toString().split("/")));

        return type.get(2).toUpperCase();
    }
}
