package id.co.bsm.util;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class DataDTOUtil implements Serializable {
    private static final long serialVersionUID = 1L;

    private ObjectMapper mapper = new ObjectMapper();
    private ObjectReader reader = mapper.reader();
    private ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();

    public static DataDTOUtil instance = new DataDTOUtil();

    public DataDTOUtil() {
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
    }

    public Object update(Object oldData, Object newData) {
        try {
            Field[] fields = newData.getClass().getDeclaredFields();
            Class<?> dtoClass = Class.forName(newData.getClass().getName());

            for (Field field : fields) {
                Method getMethod = new PropertyDescriptor(field.getName(), dtoClass).getReadMethod();
                Method setMethod = new PropertyDescriptor(field.getName(), dtoClass).getWriteMethod();

                Object oData = getMethod.invoke(oldData);
                Object nData = getMethod.invoke(newData);

                if (nData == null || oData == nData) continue;

                setMethod.invoke(oldData, nData);
            }
        } catch (Exception e) {}

        return oldData;
    }

    public static DataDTOUtil getInstance() {
        return instance;
    }
}
