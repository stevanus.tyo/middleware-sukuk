package id.co.bsm.util;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class JsonUtil {
    private ObjectMapper mapper = new ObjectMapper();

    public JsonUtil() {  }

    public <T> T jsonToObject(String _string, Class _class) throws Exception {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        JavaType listType = mapper.getTypeFactory().constructCollectionType(List.class, _class);

        return  mapper.readValue(_string, listType);
    }
}
