package id.co.bsm.util;

public class QueryAppend {
    String res = "";
    public QueryAppend() { }

    public QueryAppend(String res) {
        this.res = res;
    }

    public void add(String qry, Object value) {
        if (value == null) return ;
        if (res != "") res += "&";
        res += qry + "=" + value;
    }

    public String getRes() {
        return this.res;
    }
}
