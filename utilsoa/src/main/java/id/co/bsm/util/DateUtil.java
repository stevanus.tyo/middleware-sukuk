package id.co.bsm.util;

import id.co.bsm.service.error.BsmSoaException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtil {
    private SimpleDateFormat sbnDate = new SimpleDateFormat("yyyy-MM-dd");

    private SimpleDateFormat fullSbnDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private SimpleDateFormat coreDate = new SimpleDateFormat("yyyyMMdd");

    private DateTimeFormatter sbnDatel = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private DateTimeFormatter coreDatel = DateTimeFormatter.BASIC_ISO_DATE;

    public static DateUtil instance = new DateUtil();

    public DateUtil() {
    }

    public String parse(String _date) throws BsmSoaException {
        try {
            return sbnDate.format(coreDate.parse(_date));
        } catch (NullPointerException e) {
            throw new BsmSoaException(e);
        } catch (Exception e) {
            throw new BsmSoaException(e);
        }
    }

    public String parse(Date _date) throws BsmSoaException {
        try {
            return coreDate.format(_date);
        } catch (NullPointerException e) {
            throw new BsmSoaException(e);
        } catch (Exception e) {
            throw new BsmSoaException(e);
        }
    }

    public Date get(String date) {
        try {
            return coreDate.parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }

    public Date getFull(String date) {
        try {
            return fullSbnDate.parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }

    public String localParse(String _date) {
        return sbnDatel.format(coreDatel.parse(_date));
    }

    public String localParse(LocalDate _date) {
        return sbnDate.format(_date);
    }

    public LocalDate localDate(String _date) {
        return LocalDate.parse(_date, coreDatel);
    }

    public static DateUtil getInstance() {
        return instance;
    }
}
