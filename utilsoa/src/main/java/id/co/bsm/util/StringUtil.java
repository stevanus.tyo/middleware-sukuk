package id.co.bsm.util;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;

public class StringUtil {
    public static StringUtil instance = new StringUtil();
    private static String BLANK = "";

    public StringUtil() {  }

    public String replaceSbnChar(String _mssg) {
        return  _mssg.replaceAll("\\n", "").replaceAll("\\r", "")
                .replaceAll("\\t", "").replaceAll("\\s", "");
    }

    public String forLengthEmpty(String _txt, int _max, int _cnt) {
        int start = (_max * _cnt)-_max;
        int end = (_max * _cnt) - 1;

        if(_txt.length() < start) return BLANK;

        if(_txt.length() < end) end = _txt.length();

        return _txt.substring(start, end);
    }

    public String forLengthDuplicate(String _txt, int _max, int _cnt) {
        return null;
    }

    public String getCellString(Row _row, int _idx) {
        DataFormatter formatter = new DataFormatter();

        if (_row.getCell(_idx) == null) return null;
        return formatter.formatCellValue(_row.getCell(_idx));
    }

    public static StringUtil getInstance() {
        return instance;
    }
}
