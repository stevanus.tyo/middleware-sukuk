package id.co.bsm.enumeration;

public final class Constants {
    public static Long SBN_BANK_CODE = 125L;

    public static Long SBN_PARTICIPANT_ID = 542L;

    public static Long SBN_SUBREG_ID = 404L;

    public static char IS_DEFAULT = 'Y';

    public static String NEW_DATE = "20220101";

    public static String SBN_SEX = "SBN.SEX";

    public static String SBN_OCCUPATION = "SBN.OCCUPATION";

    public static String SBN_CITY = "SBN.CITY";

    public static String SBN_ACTIVATE = "activate";

    public static String SBN_DEACTIVATE = "deactivate";

    public enum ActionFlag {
        CREATION,
        UPDATE,
        DELETE
    }

    public enum LevelRisk {
        LOW,
        MEDIUM,
        HIGHT
    }

    public enum ActiveStatus {
        ACTIVE,
        INACTIVE
    }

    public enum InvestorClientType {
        DIRECT,
        NON_DIRECT
    }

    public enum InvestorStatus {
        NEW,
        PENDING,
        REGISTER,
        UNACTIVE,
        ACTIVE
    }

    public enum  Nationality {
        INDONESIA,
        WNA
    }

    public enum  StaticValue {
        ID,
        BBKP2,
        BSMDIDJA,
        F123
    }

    public enum InvestorType {
        INDIVIDUAL,
        CORPORATE
    }

    public enum  TransactionStatus {
        PAID,
        UNPAID,
        CANCELED
    }

    public enum RequestMethod {
        GET,
        HEAD,
        POST,
        PUT,
        PATCH,
        DELETE,
        OPTIONS,
        TRACE,
        WSDL
    }

    public enum HendlerEnum {
        URL,
        METHOD,
        TIME,
        NONCE,
        BODY,
        SIGN64,
        AUTH
    }

    public enum SbnRegister {
        INVESTOR,
        SUMBER_DANA,
        SURAT_BERHARGA,
        ACTIVATE,
        COMPLETE,
        FINISH
    }

    private Constants() {
    }
}
