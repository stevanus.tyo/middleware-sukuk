package id.co.bsm.enumeration;

public class ErrorMessage {
    public static String NO_INVESTOR = "Investor Tidak Ditemukan";

    public static String NO_ACTIVE_INVESTOR = "Investor Tidak Ditemukan atau tidak aktif";

    public static String NO_EXCHANGE_INVESTOR = "Investor belum mempunyai SRE";

    public static String NO_SID_INVESTOR = "Investor Tidak Memiliki SID";

    public static String NO_ORDER = "Investor belum melakukan pemesanan";

    public static String ORDER_SERIES_MANDATORY = "Kode Series Mandatory";

    public static String REG_COMPLETE = "Investor Telah Melakukan Registrasi";
}
