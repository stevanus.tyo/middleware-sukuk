package id.co.bsm.service.error;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ErrorProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws BsmSoaException {

        BsmSoaException cause = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, BsmSoaException.class);

        if (cause == null) { // no error
            return;

        } else { // error
            String[] causes = new String[0];
            if(cause.getMessage()!=null)
                cause.getMessage().split(Pattern.quote("."));

            ErrorDetails error = new ErrorDetails();
            error.setStatus("ERROR");
            error.setErrorCode(((cause.getErrorDetail()!=null)
                    ?((cause.getErrorDetail().getCode()!=null)
                    ?cause.getErrorDetail().getCode():102):101));
            error.setErrorMessage(((cause.getErrorDetail()!=null)?
                    cause.getErrorDetail().getMessage():
                    causes[causes.length-1]));
            error.setErrorDetail(((cause.getErrorDetail()!=null)?
                    cause.getErrorDetail().getErrors():null));

            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 500);
            exchange.getOut().setBody(error);

            cause.printStackTrace();
        }
    }
}
