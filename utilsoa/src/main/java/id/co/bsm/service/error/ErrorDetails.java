package id.co.bsm.service.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.bsm.service.dto.SbnErrorDetailsDTO;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Error")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ErrorDetails implements Serializable {
    @JsonProperty("Status")
    private String status;

    @JsonProperty("ErrorCode")
    private Integer errorCode;

    @JsonProperty("Message")
    private String errorMessage;

    @JsonProperty("MessageDetails")
    private SbnErrorDetailsDTO errorDetail;

    public String getStatus() {
        return status;
    }

    public ErrorDetails() { }

    public ErrorDetails(String status, Integer errorCode, String errorMessage, SbnErrorDetailsDTO errorDetail) {
        this.status = status;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.errorDetail = errorDetail;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public SbnErrorDetailsDTO getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(SbnErrorDetailsDTO errorDetail) {
        this.errorDetail = errorDetail;
    }
}

