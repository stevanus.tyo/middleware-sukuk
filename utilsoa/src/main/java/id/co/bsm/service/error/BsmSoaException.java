package id.co.bsm.service.error;

import id.co.bsm.service.dto.SbnErrorResponseDTO;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "BsmSoaException")
public class BsmSoaException extends Exception implements Serializable {
    private SbnErrorResponseDTO errorDetail;

    public BsmSoaException(String emssg) {
        super(emssg);
    }

    public BsmSoaException(Exception ex) {
        super(ex);
    }

    public BsmSoaException(String emssg, Throwable err) {
        super(emssg, err);
    }

    public BsmSoaException(SbnErrorResponseDTO errorDetails) {
        this.errorDetail = errorDetails;
    }

    private BsmSoaException(String message, SbnErrorResponseDTO errorDetails) {
        super(message);
        this.errorDetail = errorDetails;
    }

    public SbnErrorResponseDTO getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(SbnErrorResponseDTO errorDetail) {
        this.errorDetail = errorDetail;
    }
}
