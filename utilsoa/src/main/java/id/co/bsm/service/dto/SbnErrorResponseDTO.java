package id.co.bsm.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnErrorResponseDTO {
    @JsonProperty("Code")
    private Integer code;

    @JsonProperty("Message")
    private String message;

    @JsonProperty("Errors")
    private SbnErrorDetailsDTO errors;

    public SbnErrorResponseDTO() { }

    public SbnErrorResponseDTO(Integer code, String message, SbnErrorDetailsDTO errors) {
        this.code = code;
        this.message = message;
        this.errors = errors;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SbnErrorDetailsDTO getErrors() {
        return errors;
    }

    @JsonSetter("Errors")
    public void setErrors(SbnErrorDetailsDTO errors) {
        this.errors = errors;
    }
}
