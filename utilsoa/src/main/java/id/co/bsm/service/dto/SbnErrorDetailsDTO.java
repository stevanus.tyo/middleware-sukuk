package id.co.bsm.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnErrorDetailsDTO {
    @JsonProperty("Investor.NoIdentitas")
    private List invIdentitas = new ArrayList();

    @JsonProperty("Pemesanan.TrxId")
    private List orderTrxId = new ArrayList();

    public SbnErrorDetailsDTO() {  }

    public SbnErrorDetailsDTO(List invIdentitas) {
        this.invIdentitas = invIdentitas;
    }

    public List getInvIdentitas() {
        return invIdentitas;
    }

    public void setInvIdentitas(List invIdentitas) {
        this.invIdentitas = invIdentitas;
    }

    public List getOrderTrxId() {
        return orderTrxId;
    }

    public void setOrderTrxId(List orderTrxId) {
        this.orderTrxId = orderTrxId;
    }
}
