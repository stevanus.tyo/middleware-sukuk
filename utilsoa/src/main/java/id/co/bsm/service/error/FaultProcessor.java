package id.co.bsm.service.error;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.cxf.interceptor.Fault;

public class FaultProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        Fault fcause = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Fault.class);
        BsmSoaException cause = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, BsmSoaException.class);
        if (cause != null && fcause == null)
            fcause = new Fault(cause);

        if (fcause == null) {
            return;
        } else {
            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 500);
            exchange.getOut().setFault(true);
            exchange.getOut().setBody(fcause);
        }
    }
}
