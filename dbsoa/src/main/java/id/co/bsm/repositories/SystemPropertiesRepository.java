package id.co.bsm.repositories;

import id.co.bsm.domain.SystemProperties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SystemPropertiesRepository extends JpaRepository<SystemProperties, String> {
}
