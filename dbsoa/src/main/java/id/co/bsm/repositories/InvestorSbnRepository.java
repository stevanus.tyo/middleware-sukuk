package id.co.bsm.repositories;

import id.co.bsm.domain.InvestorSbn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InvestorSbnRepository extends JpaRepository<InvestorSbn, String> {
    @Query("select t_investor_sbn from InvestorSbn t_investor_sbn where t_investor_sbn.investor.sidNum =:sid")
    InvestorSbn findInvestorSbnByInvestorSidNum(@Param("sid") String sid);
}
