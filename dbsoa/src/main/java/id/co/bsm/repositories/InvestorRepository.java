package id.co.bsm.repositories;

import id.co.bsm.domain.Investor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InvestorRepository extends JpaRepository<Investor, String> {
    String INVESTOR_BY_CIF_CACHE = "investorByCifNum";
    String INVESTOR_BY_SID_CACHE = "investorBySidNum";
    String INVESTOR_ACTIVE_BY_CIF_CACHE = "investorByCifNum";

    @Cacheable(cacheNames = INVESTOR_BY_CIF_CACHE)
    @Query("select t_investor from Investor t_investor where t_investor.cifNum =:cif")
    Investor findInvestorByCifNum(@Param("cif") String cif);

    @Cacheable(cacheNames = INVESTOR_BY_SID_CACHE)
    @Query("select t_investor from Investor t_investor where t_investor.sidNum =:sid")
    Investor findInvestorBySidNum(@Param("sid") String sid);

    @Cacheable(cacheNames = INVESTOR_ACTIVE_BY_CIF_CACHE)
    @Query("select t_investor from Investor t_investor where t_investor.cifNum =:cif and t_investor.investorStatus =:status")
    Investor findInvestorByCifNumAndInvestorStatus(@Param("cif") String cif, @Param("status") String status);
}
