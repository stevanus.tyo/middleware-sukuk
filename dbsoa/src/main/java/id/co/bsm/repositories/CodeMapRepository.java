package id.co.bsm.repositories;

import id.co.bsm.domain.CodeMap;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import static id.co.bsm.repositories.InvestorRepository.INVESTOR_BY_CIF_CACHE;

@Repository
public interface CodeMapRepository extends JpaRepository<CodeMap, String> {
    String CODEMAP_BY_KEY_CACHE = "codeMapByCodeNameAndSysCode";

    String CODEMAP_DEFAULT_KEY_CACHE = "codeMapByCodeNameAndIsDefault";

    @Cacheable(cacheNames = CODEMAP_BY_KEY_CACHE)
    @Query("select t_code_map from CodeMap t_code_map where t_code_map.codeKey.codeName =:keyname and t_code_map.codeKey.sysCode =:syscode")
    CodeMap findCodeMapByCodeNameAndSysCode(@Param("keyname") String keyname, @Param("syscode") String syscode);

    @Cacheable(cacheNames = CODEMAP_DEFAULT_KEY_CACHE)
    @Query("select t_code_map from CodeMap t_code_map where t_code_map.codeKey.codeName =:keyname and t_code_map.isDefault =:isDefault")
    CodeMap findCodeMapByCodeNameAndIsDefault(@Param("keyname") String keyname, @Param("isDefault") char isDefault);
}