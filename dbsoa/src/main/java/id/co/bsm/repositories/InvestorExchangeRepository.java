package id.co.bsm.repositories;

import id.co.bsm.domain.InvestorExchange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvestorExchangeRepository extends JpaRepository<InvestorExchange, String> {
}
