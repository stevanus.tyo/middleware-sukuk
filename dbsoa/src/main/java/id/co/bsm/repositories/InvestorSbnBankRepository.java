package id.co.bsm.repositories;

import id.co.bsm.domain.InvestorSbnBank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InvestorSbnBankRepository extends JpaRepository<InvestorSbnBank, String> {
    @Query("select t_investor_sbn_bank from InvestorSbnBank t_investor_sbn_bank where t_investor_sbn_bank.accountNo =:acctno")
    InvestorSbnBank findByAccountNo(@Param("acctno") String acctno);
}
