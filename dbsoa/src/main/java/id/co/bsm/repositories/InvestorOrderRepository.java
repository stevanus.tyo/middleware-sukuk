package id.co.bsm.repositories;

import id.co.bsm.domain.InvestorOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvestorOrderRepository extends JpaRepository<InvestorOrder, String> {
}
