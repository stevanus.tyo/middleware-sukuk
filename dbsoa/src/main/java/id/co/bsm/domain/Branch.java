package id.co.bsm.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Branch")
@XmlAccessorType(value = XmlAccessType.FIELD)
@Entity
@Table(name = "M_BRANCH")
public class Branch {
    @Id
    @Column(name = "CODE", length = 100)
    private String code;

    @Column(name = "NAME", length = 100, nullable = false)
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
