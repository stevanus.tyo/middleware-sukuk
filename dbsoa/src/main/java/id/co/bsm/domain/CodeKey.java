package id.co.bsm.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CodeKey implements Serializable {
    private static final long serialVersionUID = 1L;

    @Size(max = 50)
    @Column(name = "CODE_NAME")
    private String codeName;

    @Size(max = 20)
    @Column(name = "SYS_CODE")
    private String sysCode;

    @Size(max = 20)
    @Column(name = "USER_CODE")
    private String userCode;

    public CodeKey() { }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getSysCode() {
        return sysCode;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
}
