package id.co.bsm.domain;

import id.co.bsm.domain.generator.BaseSeqGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@XmlRootElement(name = "InvestorSbn")
@XmlAccessorType(value = XmlAccessType.FIELD)
@Table(name = "T_INVESTOR_SBN")
@Entity
public class InvestorSbn implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "inv_sbn_seq")
    @GenericGenerator(name = "inv_sbn_seq", strategy = "id.co.bsm.domain.generator.BaseSeqGenerator", parameters = {
            @org.hibernate.annotations.Parameter(name = BaseSeqGenerator.VALUE_PREFIX_PARAMETER, value = "ISBN"),
            @org.hibernate.annotations.Parameter(name = BaseSeqGenerator.SEQUENCE_PARAM, value = "seq_inv_sbn")
    })
    @Column(name = "ID", length = 20)
    private String id;

    @Column(name = "NAME", length = 150)
    private String name;

    @Column(name = "KTP_NO", length = 20)
    private String ktpNo;

    @Column(name = "BRITH_PLACE", length = 50)
    private String birthPlace;

    @Column(name = "BRITH_DATE")
    private Date birthDate;

    @Column(name = "SEX", length = 10)
    private String sex;

    @Column(name = "OCCUPATION", length = 10)
    private String occupation;

    @Column(name = "CITY", length = 10)
    private String city;

    @Column(name = "STATUS", length = 50)
    private String status;

    @Column(name = "ADDRESS", length = 250)
    private String address;

    @Column(name = "HOME_PHONE", length = 50)
    private String homePhone;

    @Column(name = "MOBILE_PHONE", length = 50)
    private String mobilePhone;

    @Column(name = "EMAIL", length = 50)
    private String email;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="SEQ_NUMBER", unique = true, nullable = false)
    private Investor investor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKtpNo() {
        return ktpNo;
    }

    public void setKtpNo(String ktpNo) {
        this.ktpNo = ktpNo;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }
}
