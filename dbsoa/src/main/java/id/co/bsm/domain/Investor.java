package id.co.bsm.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.co.bsm.domain.generator.BaseSeqGenerator;
import id.co.bsm.enumeration.Constants;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@XmlRootElement(name = "Investor")
@XmlAccessorType(value = XmlAccessType.FIELD)
@Entity
@Table(name = "M_INVESTOR")
public class Investor implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "investor_seq")
    @GenericGenerator(name = "investor_seq"
            , strategy = "id.co.bsm.domain.generator.BaseSeqGenerator"
            , parameters = {
                @org.hibernate.annotations.Parameter(name = BaseSeqGenerator.VALUE_PREFIX_PARAMETER, value = "INV"),
                @org.hibernate.annotations.Parameter(name = BaseSeqGenerator.SEQUENCE_PARAM, value = "seq_investor")
            }
    )
    @Column(name = "SEQ_NUMBER", updatable = false, length = 50)
    private String seqNum;

    @NotNull
    @Column(name = "CIF_NUMBER", unique = true, length = 50)
    private String cifNum;

    @NotNull
    @Column(name = "SID_NUMBER", unique = true, length = 50)
    private String sidNum;

    @Column(name = "BANK_CODE", length = 20)
    private String bankCode = Constants.StaticValue.BSMDIDJA.name();

    @Column(name = "CUST_NAME", length = 200, nullable = false)
    private String custName;

    @Column(name = "ACCT_NUMBER", length = 50)
    private String acctNumber;

    @Column(name = "LEVEL_RISK", length = 20, nullable = false)
    private String levelRisk = Constants.LevelRisk.LOW.name();

    @Column(name = "ACTION", length = 10)
    private String action = Constants.ActionFlag.CREATION.name();

    @Column(name = "INV_TYPE", length = 15)
    private String investorType = Constants.InvestorType.INDIVIDUAL.name();

    @Column(name = "INV_CLINET_TYPE", length = 15)
    private String investorClientType = Constants.InvestorClientType.DIRECT.name();

    @Column(name = "ACCT_LOCAL_CODE", length = 10)
    private String accountLocalCode = Constants.StaticValue.BBKP2.name();

    @Column(name = "ACCT_CLINET_CODE", length = 30, nullable = false)
    private String accountClientCode = Constants.StaticValue.F123.name();

    @Column(name = "ACCT_TAX_CODE", length = 30, nullable = false)
    private String accountTaxCode = "1010";

    @Column(name = "DIRECT_SID", length = 50)
    private String directSid;

    @Column(name = "ASSET_OWNER", length = 50)
    private Integer assetOwner = 1;

    @OneToOne(mappedBy = "investor" ,cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(nullable = true)
    private InvestorProfile investorProfile;

    @OneToOne(mappedBy = "investor" ,cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private InvestorBank investorBank;

    @OneToOne(mappedBy = "investor" ,cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private InvestorSbn investorSbn;

    @JsonManagedReference
    @XmlElementWrapper(name = "exchange")
    @OneToMany(mappedBy = "investors", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<InvestorExchange> exchange;

    @JsonManagedReference
    @XmlElementWrapper(name = "sbndana")
    @OneToMany(mappedBy = "investors", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<InvestorSbnBank> sbndana;

    @JsonManagedReference
    @XmlElementWrapper(name = "orders")
    @OneToMany(mappedBy = "investors", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<InvestorOrder> orders;

    @Column(name = "STATUS", length = 20)
    private String investorStatus = Constants.InvestorStatus.NEW.name();

    public String getSeqNum() {
        return seqNum;
    }

    public void setSeqNum(String seqNum) {
        this.seqNum = seqNum;
    }

    public String getCifNum() {
        return cifNum;
    }

    public void setCifNum(String cifNum) {
        this.cifNum = cifNum;
    }

    public String getSidNum() {
        return sidNum;
    }

    public void setSidNum(String sidNum) {
        this.sidNum = sidNum;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAcctNumber() {
        return acctNumber;
    }

    public void setAcctNumber(String acctNumber) {
        this.acctNumber = acctNumber;
    }

    public String getLevelRisk() {
        return levelRisk;
    }

    public void setLevelRisk(String levelRisk) {
        this.levelRisk = levelRisk;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getInvestorType() {
        return investorType;
    }

    public void setInvestorType(String investorType) {
        this.investorType = investorType;
    }

    public String getInvestorClientType() {
        return investorClientType;
    }

    public void setInvestorClientType(String investorClientType) {
        this.investorClientType = investorClientType;
    }

    public String getAccountLocalCode() {
        return accountLocalCode;
    }

    public void setAccountLocalCode(String accountLocalCode) {
        this.accountLocalCode = accountLocalCode;
    }

    public String getAccountClientCode() {
        return accountClientCode;
    }

    public void setAccountClientCode(String accountClientCode) {
        this.accountClientCode = accountClientCode;
    }

    public String getAccountTaxCode() {
        return accountTaxCode;
    }

    public void setAccountTaxCode(String accountTaxCode) {
        this.accountTaxCode = accountTaxCode;
    }

    public String getDirectSid() {
        return directSid;
    }

    public void setDirectSid(String directSid) {
        this.directSid = directSid;
    }

    public Integer getAssetOwner() {
        return assetOwner;
    }

    public void setAssetOwner(Integer assetOwner) {
        this.assetOwner = assetOwner;
    }

    public InvestorProfile getInvestorProfile() {
        return investorProfile;
    }

    public void setInvestorProfile(InvestorProfile investorProfile) {
        this.investorProfile = investorProfile;
    }

    public InvestorBank getInvestorBank() {
        return investorBank;
    }

    public void setInvestorBank(InvestorBank investorBank) {
        this.investorBank = investorBank;
    }

    public InvestorSbn getInvestorSbn() {
        return investorSbn;
    }

    public void setInvestorSbn(InvestorSbn investorSbn) {
        this.investorSbn = investorSbn;
    }

    public List<InvestorExchange> getExchange() {
        return exchange;
    }

    public void setExchange(List<InvestorExchange> exchange) {
        this.exchange = exchange;
    }

    public List<InvestorSbnBank> getSbndana() {
        return sbndana;
    }

    public void setSbndana(List<InvestorSbnBank> sbndana) {
        this.sbndana = sbndana;
    }

    public String getInvestorStatus() {
        return investorStatus;
    }

    public void setInvestorStatus(String investorStatus) {
        this.investorStatus = investorStatus;
    }

    public List<InvestorOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<InvestorOrder> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Investor investor = (Investor) o;
        return !(investor.getSeqNum() == null || getSeqNum() == null) && Objects.equals(getSeqNum(), investor.getSeqNum());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getSeqNum());
    }
}
