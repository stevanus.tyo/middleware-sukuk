package id.co.bsm.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.co.bsm.enumeration.Constants;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "InvestorExchange")
@XmlAccessorType(value = XmlAccessType.FIELD)
@Table(name = "T_INVESTOR_EXCHANGE")
@Entity
public class InvestorExchange implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SRE_NUMBER", unique = true, length = 50)
    private String sreNum;

    @Column(name = "SRE_NAME", length = 50)
    private String sreName;

    @Column(name = "SUB_REGISTRY")
    private Long subRegistry = Constants.SBN_SUBREG_ID;

    @Column(name = "PARTICIPANT")
    private Long participant = Constants.SBN_PARTICIPANT_ID;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE}, optional = false)
    @JoinColumn(name = "SEQ_NUMBER", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Investor investors;

    @Column(name = "SBN_ID")
    private Long sbnId;

    public String getSreNum() {
        return sreNum;
    }

    public void setSreNum(String sreNum) {
        this.sreNum = sreNum;
    }

    public String getSreName() {
        return sreName;
    }

    public void setSreName(String sreName) {
        this.sreName = sreName;
    }

    public Long getSubRegistry() {
        return subRegistry;
    }

    public void setSubRegistry(Long subRegistry) {
        this.subRegistry = subRegistry;
    }

    public Long getParticipant() {
        return participant;
    }

    public void setParticipant(Long participant) {
        this.participant = participant;
    }

    public Investor getInvestors() {
        return investors;
    }

    public void setInvestors(Investor investors) {
        this.investors = investors;
    }

    public Long getSbnId() {
        return sbnId;
    }

    public void setSbnId(Long sbnId) {
        this.sbnId = sbnId;
    }
}
