package id.co.bsm.domain;

import id.co.bsm.domain.generator.BaseSeqGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Objects;

@XmlRootElement(name = "InvestorBank")
@XmlAccessorType(value = XmlAccessType.FIELD)
@Entity
@Table(name = "M_INVESTOR_BANK")
public class InvestorBank implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "inv_bank_seq")
    @GenericGenerator(name = "inv_bank_seq", strategy = "id.co.bsm.domain.generator.BaseSeqGenerator", parameters = {
            @org.hibernate.annotations.Parameter(name = BaseSeqGenerator.VALUE_PREFIX_PARAMETER, value = "INVB"),
            @org.hibernate.annotations.Parameter(name = BaseSeqGenerator.SEQUENCE_PARAM, value = "seq_inv_bank")
    })
    @Column(name = "ID", length = 20)
    private String id;

    @Column(name = "INV_BANK_ACCT_NAME_1", length = 100)
    private String investorBankAccountName1;

    @Column(name = "INV_BANK_ACCT_NUMBER_1", length = 100)
    private String investorBankAccountNumber1;

    @Column(name = "INV_BANK_ACCT_BI_CODE_1", length = 100)
    private String investorBankAccountBICCode1;

    @Column(name = "INV_BANK_ACCT_HOLDER_NAME_1", length = 100)
    private String investorBankAccountHolderName1;

    @Column(name = "INV_BANK_ACCT_CCY_1", length = 5)
    private String investorBankAccountCurrency1;

    @Column(name = "INV_BANK_ACCT_NAME_2", length = 100)
    private String investorBankAccountName2;

    @Column(name = "INV_BANK_ACCT_NUMBER_2", length = 100)
    private String investorBankAccountNumber2;

    @Column(name = "INV_BANK_ACCT_BI_CODE_2", length = 100)
    private String investorBankAccountBICCode2;

    @Column(name = "INV_BANK_ACCT_HOLDER_NAME_2", length = 100)
    private String investorBankAccountHolderName2;

    @Column(name = "INV_BANK_ACCT_CCY_2", length = 5)
    private String investorBankAccountCurrency2;

    @Column(name = "INV_BANK_ACCT_NAME_3", length = 100)
    private String investorBankAccountName3;

    @Column(name = "INV_BANK_ACCT_NUMBER_3", length = 100)
    private String investorBankAccountNumber3;

    @Column(name = "INV_BANK_ACCT_BI_CODE_3", length = 100)
    private String investorBankAccountBICCode3;

    @Column(name = "INV_BANK_ACCT_HOLDER_NAME_3", length = 100)
    private String investorBankAccountHolderName3;

    @Column(name = "INV_BANK_ACCT_CCY_3", length = 5)
    private String investorBankAccountCurrency3;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="SEQ_NUMBER", unique = true, nullable = false)
    private Investor investor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvestorBankAccountName1() {
        return investorBankAccountName1;
    }

    public void setInvestorBankAccountName1(String investorBankAccountName1) {
        this.investorBankAccountName1 = investorBankAccountName1;
    }

    public String getInvestorBankAccountNumber1() {
        return investorBankAccountNumber1;
    }

    public void setInvestorBankAccountNumber1(String investorBankAccountNumber1) {
        this.investorBankAccountNumber1 = investorBankAccountNumber1;
    }

    public String getInvestorBankAccountBICCode1() {
        return investorBankAccountBICCode1;
    }

    public void setInvestorBankAccountBICCode1(String investorBankAccountBICCode1) {
        this.investorBankAccountBICCode1 = investorBankAccountBICCode1;
    }

    public String getInvestorBankAccountHolderName1() {
        return investorBankAccountHolderName1;
    }

    public void setInvestorBankAccountHolderName1(String investorBankAccountHolderName1) {
        this.investorBankAccountHolderName1 = investorBankAccountHolderName1;
    }

    public String getInvestorBankAccountCurrency1() {
        return investorBankAccountCurrency1;
    }

    public void setInvestorBankAccountCurrency1(String investorBankAccountCurrency1) {
        this.investorBankAccountCurrency1 = investorBankAccountCurrency1;
    }

    public String getInvestorBankAccountName2() {
        return investorBankAccountName2;
    }

    public void setInvestorBankAccountName2(String investorBankAccountName2) {
        this.investorBankAccountName2 = investorBankAccountName2;
    }

    public String getInvestorBankAccountNumber2() {
        return investorBankAccountNumber2;
    }

    public void setInvestorBankAccountNumber2(String investorBankAccountNumber2) {
        this.investorBankAccountNumber2 = investorBankAccountNumber2;
    }

    public String getInvestorBankAccountBICCode2() {
        return investorBankAccountBICCode2;
    }

    public void setInvestorBankAccountBICCode2(String investorBankAccountBICCode2) {
        this.investorBankAccountBICCode2 = investorBankAccountBICCode2;
    }

    public String getInvestorBankAccountHolderName2() {
        return investorBankAccountHolderName2;
    }

    public void setInvestorBankAccountHolderName2(String investorBankAccountHolderName2) {
        this.investorBankAccountHolderName2 = investorBankAccountHolderName2;
    }

    public String getInvestorBankAccountCurrency2() {
        return investorBankAccountCurrency2;
    }

    public void setInvestorBankAccountCurrency2(String investorBankAccountCurrency2) {
        this.investorBankAccountCurrency2 = investorBankAccountCurrency2;
    }

    public String getInvestorBankAccountName3() {
        return investorBankAccountName3;
    }

    public void setInvestorBankAccountName3(String investorBankAccountName3) {
        this.investorBankAccountName3 = investorBankAccountName3;
    }

    public String getInvestorBankAccountNumber3() {
        return investorBankAccountNumber3;
    }

    public void setInvestorBankAccountNumber3(String investorBankAccountNumber3) {
        this.investorBankAccountNumber3 = investorBankAccountNumber3;
    }

    public String getInvestorBankAccountBICCode3() {
        return investorBankAccountBICCode3;
    }

    public void setInvestorBankAccountBICCode3(String investorBankAccountBICCode3) {
        this.investorBankAccountBICCode3 = investorBankAccountBICCode3;
    }

    public String getInvestorBankAccountHolderName3() {
        return investorBankAccountHolderName3;
    }

    public void setInvestorBankAccountHolderName3(String investorBankAccountHolderName3) {
        this.investorBankAccountHolderName3 = investorBankAccountHolderName3;
    }

    public String getInvestorBankAccountCurrency3() {
        return investorBankAccountCurrency3;
    }

    public void setInvestorBankAccountCurrency3(String investorBankAccountCurrency3) {
        this.investorBankAccountCurrency3 = investorBankAccountCurrency3;
    }

    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }
}

