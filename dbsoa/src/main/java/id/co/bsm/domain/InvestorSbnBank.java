package id.co.bsm.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "InvestorSbnDana")
@XmlAccessorType(value = XmlAccessType.FIELD)
@Table(name = "T_INVESTOR_SBN_DANA")
@Entity
public class InvestorSbnBank implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ACCOUNT_NO", unique = true, length = 50)
    private String accountNo;

    @Column(name = "ACCOUNT_NAME", length = 150)
    private String accountName;

    @Column(name = "BANK_ID", length = 50)
    private Long bankId;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE}, optional = false)
    @JoinColumn(name = "SEQ_NUMBER", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Investor investors;

    @Column(name = "SBN_ID")
    private Long sbnId;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public Investor getInvestors() {
        return investors;
    }

    public void setInvestors(Investor investors) {
        this.investors = investors;
    }

    public Long getSbnId() {
        return sbnId;
    }

    public void setSbnId(Long sbnId) {
        this.sbnId = sbnId;
    }
}
