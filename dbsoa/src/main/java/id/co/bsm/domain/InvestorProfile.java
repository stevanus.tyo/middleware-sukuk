package id.co.bsm.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

import id.co.bsm.domain.generator.BaseSeqGenerator;
import id.co.bsm.enumeration.Constants;
import id.co.bsm.util.DateUtil;
import org.hibernate.annotations.GenericGenerator;

@XmlRootElement(name = "InvestorProfile")
@XmlAccessorType(value = XmlAccessType.FIELD)
@Entity
@Table(name = "M_INVESTOR_PROFILE")
public class InvestorProfile implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "inv_profie_seq")
    @GenericGenerator(name = "inv_profie_seq", strategy = "id.co.bsm.domain.generator.BaseSeqGenerator", parameters = {
            @org.hibernate.annotations.Parameter(name = BaseSeqGenerator.VALUE_PREFIX_PARAMETER, value = "INVP"),
            @org.hibernate.annotations.Parameter(name = BaseSeqGenerator.SEQUENCE_PARAM, value = "seq_inv_profie")
    })
    @Column(name = "ID", length = 20)
    private String id;

    @Column(name = "INV_FST_NAME", length = 100, nullable = false)
    private String investorFirstName;

    @Column(name = "INV_MID_NAME", length = 100)
    private String investorMiddleName;

    @Column(name = "INV_LAST_NAME", length = 100)
    private String investorLastName;

    @Column(name = "INV_NATIONALITY", length = 100)
    private String investorNationality = Constants.Nationality.INDONESIA.name();

    @Column(name = "INV_KTP_NUMBER", length = 50, nullable = false)
    private String investorKTPNumber;

    @Column(name = "INV_KTP_EXP_DATE", nullable = false)
    private Date investorKtpExpiredDate = DateUtil.getInstance().get(Constants.NEW_DATE);

    @Column(name = "INV_NPWP_NUMBER", length = 50)
    private String investorNpwpNumber;

    @Column(name = "INV_KTP_REG_DATE", nullable = false)
    private Date investorNpwpRegistrationDate = DateUtil.getInstance().get(Constants.NEW_DATE);

    @Column(name = "INV_PASS_NUMBER", length = 50)
    private String investorPassportNumber;

    @Column(name = "INV_PASS_EXP_DATE")
    private Date investorPassportExpiredDate;

    @Column(name = "INV_KITAS_SKD_NUMBER", length = 50)
    private String investorKitasSkdNumber;

    @Column(name = "INV_KITAS_SKD_EXP_DATE")
    private Date investorKitasSkdExpiredDate;

    @Column(name = "INV_BIRTH_PLACE", nullable = false, length = 50)
    private String investorBirthPlace;

    @Column(name = "INV_BIRTH_DATE", nullable = false)
    private Date investorBirthDate;

    @Column(name = "INV_ADDR_1", nullable = false, length = 60)
    private String investorAddress1;

    @Column(name = "INV_ADDR_2", nullable = false, length = 60)
    private String investorAddress2;

    @Column(name = "INV_ADDR_3", nullable = false, length = 60)
    private String investorAddress3;

    @Column(name = "INV_CITY", nullable = false, length = 5)
    private String investorCity;

    @Column(name = "INV_PROVICE", nullable = false, length = 5)
    private String investorProvince;

    @Column(name = "INV_POSTAL_CODE", length = 15)
    private String investorPostalCode;

    @Column(name = "INV_COUNTRY", length = 5)
    private String investorCountry = Constants.StaticValue.ID.name();

    @Column(name = "INV_HOME_PHONE", nullable = false, length = 20)
    private String investorHomePhone;

    @Column(name = "INV_MOBILE_PHONE", length = 20)
    private String investorMobilePhone;

    @Column(name = "INV_EMAIL", nullable = false, length = 50)
    private String investorEmail;

    @Column(name = "INV_FAX", length = 20)
    private String investorFax;

    @Column(name = "INV_OTHER_ADDR_1", length = 60)
    private String investorOtherAddress1;

    @Column(name = "INV_OTHER_ADDR_2", length = 60)
    private String investorOtherAddress2;

    @Column(name = "INV_OTHER_ADDR_3", length = 60)
    private String investorOtherAddress3;

    @Column(name = "INV_OTHER_CITY", length = 5)
    private String investorOtherCity;

    @Column(name = "INV_OTHER_PROVINCE", length = 5)
    private String investorOtherProvince;

    @Column(name = "INV_OTHER_POSTAL_CODE", length = 15)
    private String investorOtherPostalCode;

    @Column(name = "INV_OTHER_COUNTRY", length = 5)
    private String investorOtherCountry;

    @Column(name = "INV_OTHER_HOME_PHONE", length = 20)
    private String investorOtherHomePhone;

    @Column(name = "INV_OTHER_MOBILE_PHONE", length = 20)
    private String investorOtherMobilePhone;

    @Column(name = "INV_OTHER_EMAIL", length = 50)
    private String investorOtherEmail;

    @Column(name = "INV_OTHER_FAX", length = 20)
    private String investorOtherFax;

    @Column(name = "INV_SEX", nullable = false, length = 5)
    private String investorSex;

    @Column(name = "INV_MARITAL_STATUS", nullable = false, length = 5)
    private String investorMaritalStatus;

    @Column(name = "INV_SPOUSE_NAME", length = 100)
    private String investorSpouseName;

    @Column(name = "INV_HEIR_NAME", length = 100)
    private String investorHeirName;

    @Column(name = "INV_HEIR_RELATION", length = 100)
    private String investorHeirRelation;

    @Column(name = "INV_EDU_BACKGROUND", nullable = false,length = 5)
    private String investorEducationalBackground;

    @Column(name = "INV_OCCUPATION", nullable = false,length = 5)
    private String investorOccupation;

    @Column(name = "INV_OCCUPATION_TEXT", length = 100)
    private String investorOccupationText;

    @Column(name = "INV_NAT_BUSINESS", length = 100)
    private String investorNatureofBusiness;

    @Column(name = "INV_INCOME", nullable = false,length = 50)
    private String investorIncomePerAnnum;

    @Column(name = "INV_FUND_SOURCE", nullable = false, length = 5)
    private String investorFundSource;

    @Column(name = "INV_FUND_SOURCE_TEXT", length = 100)
    private String investorFundSourceText;

    @Column(name = "INV_ACCT_DESC", length = 100)
    private String accountDescription;

    @Column(name = "INV_INVEST_OBJECTIVE", length = 5)
    private String investorInvestmentObjective = "3";

    @Column(name = "INV_MOTHERS_MAIDEN_NAME", nullable = false, length = 100)
    private String investorMothersMaidenName;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="SEQ_NUMBER", unique = true, nullable = false)
    private Investor investor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvestorFirstName() {
        return investorFirstName;
    }

    public void setInvestorFirstName(String investorFirstName) {
        this.investorFirstName = investorFirstName;
    }

    public String getInvestorMiddleName() {
        return investorMiddleName;
    }

    public void setInvestorMiddleName(String investorMiddleName) {
        this.investorMiddleName = investorMiddleName;
    }

    public String getInvestorLastName() {
        return investorLastName;
    }

    public void setInvestorLastName(String investorLastName) {
        this.investorLastName = investorLastName;
    }

    public String getInvestorNationality() {
        return investorNationality;
    }

    public void setInvestorNationality(String investorNationality) {
        this.investorNationality = investorNationality;
    }

    public String getInvestorKTPNumber() {
        return investorKTPNumber;
    }

    public void setInvestorKTPNumber(String investorKTPNumber) {
        this.investorKTPNumber = investorKTPNumber;
    }

    public Date getInvestorKtpExpiredDate() {
        return investorKtpExpiredDate;
    }

    public void setInvestorKtpExpiredDate(Date investorKtpExpiredDate) {
        this.investorKtpExpiredDate = investorKtpExpiredDate;
    }

    public String getInvestorNpwpNumber() {
        return investorNpwpNumber;
    }

    public void setInvestorNpwpNumber(String investorNpwpNumber) {
        this.investorNpwpNumber = investorNpwpNumber;
    }

    public Date getInvestorNpwpRegistrationDate() {
        return investorNpwpRegistrationDate;
    }

    public void setInvestorNpwpRegistrationDate(Date investorNpwpRegistrationDate) {
        this.investorNpwpRegistrationDate = investorNpwpRegistrationDate;
    }

    public String getInvestorPassportNumber() {
        return investorPassportNumber;
    }

    public void setInvestorPassportNumber(String investorPassportNumber) {
        this.investorPassportNumber = investorPassportNumber;
    }

    public Date getInvestorPassportExpiredDate() {
        return investorPassportExpiredDate;
    }

    public void setInvestorPassportExpiredDate(Date investorPassportExpiredDate) {
        this.investorPassportExpiredDate = investorPassportExpiredDate;
    }

    public String getInvestorKitasSkdNumber() {
        return investorKitasSkdNumber;
    }

    public void setInvestorKitasSkdNumber(String investorKitasSkdNumber) {
        this.investorKitasSkdNumber = investorKitasSkdNumber;
    }

    public Date getInvestorKitasSkdExpiredDate() {
        return investorKitasSkdExpiredDate;
    }

    public void setInvestorKitasSkdExpiredDate(Date investorKitasSkdExpiredDate) {
        this.investorKitasSkdExpiredDate = investorKitasSkdExpiredDate;
    }

    public String getInvestorBirthPlace() {
        return investorBirthPlace;
    }

    public void setInvestorBirthPlace(String investorBirthPlace) {
        this.investorBirthPlace = investorBirthPlace;
    }

    public Date getInvestorBirthDate() {
        return investorBirthDate;
    }

    public void setInvestorBirthDate(Date investorBirthDate) {
        this.investorBirthDate = investorBirthDate;
    }

    public String getInvestorAddress1() {
        return investorAddress1;
    }

    public void setInvestorAddress1(String investorAddress1) {
        this.investorAddress1 = investorAddress1;
    }

    public String getInvestorAddress2() {
        return investorAddress2;
    }

    public void setInvestorAddress2(String investorAddress2) {
        this.investorAddress2 = investorAddress2;
    }

    public String getInvestorAddress3() {
        return investorAddress3;
    }

    public void setInvestorAddress3(String investorAddress3) {
        this.investorAddress3 = investorAddress3;
    }

    public String getInvestorCity() {
        return investorCity;
    }

    public void setInvestorCity(String investorCity) {
        this.investorCity = investorCity;
    }

    public String getInvestorProvince() {
        return investorProvince;
    }

    public void setInvestorProvince(String investorProvince) {
        this.investorProvince = investorProvince;
    }

    public String getInvestorPostalCode() {
        return investorPostalCode;
    }

    public void setInvestorPostalCode(String investorPostalCode) {
        this.investorPostalCode = investorPostalCode;
    }

    public String getInvestorCountry() {
        return investorCountry;
    }

    public void setInvestorCountry(String investorCountry) {
        this.investorCountry = investorCountry;
    }

    public String getInvestorHomePhone() {
        return investorHomePhone;
    }

    public void setInvestorHomePhone(String investorHomePhone) {
        this.investorHomePhone = investorHomePhone;
    }

    public String getInvestorMobilePhone() {
        return investorMobilePhone;
    }

    public void setInvestorMobilePhone(String investorMobilePhone) {
        this.investorMobilePhone = investorMobilePhone;
    }

    public String getInvestorEmail() {
        return investorEmail;
    }

    public void setInvestorEmail(String investorEmail) {
        this.investorEmail = investorEmail;
    }

    public String getInvestorFax() {
        return investorFax;
    }

    public void setInvestorFax(String investorFax) {
        this.investorFax = investorFax;
    }

    public String getInvestorOtherAddress1() {
        return investorOtherAddress1;
    }

    public void setInvestorOtherAddress1(String investorOtherAddress1) {
        this.investorOtherAddress1 = investorOtherAddress1;
    }

    public String getInvestorOtherAddress2() {
        return investorOtherAddress2;
    }

    public void setInvestorOtherAddress2(String investorOtherAddress2) {
        this.investorOtherAddress2 = investorOtherAddress2;
    }

    public String getInvestorOtherAddress3() {
        return investorOtherAddress3;
    }

    public void setInvestorOtherAddress3(String investorOtherAddress3) {
        this.investorOtherAddress3 = investorOtherAddress3;
    }

    public String getInvestorOtherCity() {
        return investorOtherCity;
    }

    public void setInvestorOtherCity(String investorOtherCity) {
        this.investorOtherCity = investorOtherCity;
    }

    public String getInvestorOtherProvince() {
        return investorOtherProvince;
    }

    public void setInvestorOtherProvince(String investorOtherProvince) {
        this.investorOtherProvince = investorOtherProvince;
    }

    public String getInvestorOtherPostalCode() {
        return investorOtherPostalCode;
    }

    public void setInvestorOtherPostalCode(String investorOtherPostalCode) {
        this.investorOtherPostalCode = investorOtherPostalCode;
    }

    public String getInvestorOtherCountry() {
        return investorOtherCountry;
    }

    public void setInvestorOtherCountry(String investorOtherCountry) {
        this.investorOtherCountry = investorOtherCountry;
    }

    public String getInvestorOtherHomePhone() {
        return investorOtherHomePhone;
    }

    public void setInvestorOtherHomePhone(String investorOtherHomePhone) {
        this.investorOtherHomePhone = investorOtherHomePhone;
    }

    public String getInvestorOtherMobilePhone() {
        return investorOtherMobilePhone;
    }

    public void setInvestorOtherMobilePhone(String investorOtherMobilePhone) {
        this.investorOtherMobilePhone = investorOtherMobilePhone;
    }

    public String getInvestorOtherEmail() {
        return investorOtherEmail;
    }

    public void setInvestorOtherEmail(String investorOtherEmail) {
        this.investorOtherEmail = investorOtherEmail;
    }

    public String getInvestorOtherFax() {
        return investorOtherFax;
    }

    public void setInvestorOtherFax(String investorOtherFax) {
        this.investorOtherFax = investorOtherFax;
    }

    public String getInvestorSex() {
        return investorSex;
    }

    public void setInvestorSex(String investorSex) {
        this.investorSex = investorSex;
    }

    public String getInvestorMaritalStatus() {
        return investorMaritalStatus;
    }

    public void setInvestorMaritalStatus(String investorMaritalStatus) {
        this.investorMaritalStatus = investorMaritalStatus;
    }

    public String getInvestorSpouseName() {
        return investorSpouseName;
    }

    public void setInvestorSpouseName(String investorSpouseName) {
        this.investorSpouseName = investorSpouseName;
    }

    public String getInvestorHeirName() {
        return investorHeirName;
    }

    public void setInvestorHeirName(String investorHeirName) {
        this.investorHeirName = investorHeirName;
    }

    public String getInvestorHeirRelation() {
        return investorHeirRelation;
    }

    public void setInvestorHeirRelation(String investorHeirRelation) {
        this.investorHeirRelation = investorHeirRelation;
    }

    public String getInvestorEducationalBackground() {
        return investorEducationalBackground;
    }

    public void setInvestorEducationalBackground(String investorEducationalBackground) {
        this.investorEducationalBackground = investorEducationalBackground;
    }

    public String getInvestorOccupation() {
        return investorOccupation;
    }

    public void setInvestorOccupation(String investorOccupation) {
        this.investorOccupation = investorOccupation;
    }

    public String getInvestorOccupationText() {
        return investorOccupationText;
    }

    public void setInvestorOccupationText(String investorOccupationText) {
        this.investorOccupationText = investorOccupationText;
    }

    public String getInvestorNatureofBusiness() {
        return investorNatureofBusiness;
    }

    public void setInvestorNatureofBusiness(String investorNatureofBusiness) {
        this.investorNatureofBusiness = investorNatureofBusiness;
    }

    public String getInvestorIncomePerAnnum() {
        return investorIncomePerAnnum;
    }

    public void setInvestorIncomePerAnnum(String investorIncomePerAnnum) {
        this.investorIncomePerAnnum = investorIncomePerAnnum;
    }

    public String getInvestorFundSource() {
        return investorFundSource;
    }

    public void setInvestorFundSource(String investorFundSource) {
        this.investorFundSource = investorFundSource;
    }

    public String getInvestorFundSourceText() {
        return investorFundSourceText;
    }

    public void setInvestorFundSourceText(String investorFundSourceText) {
        this.investorFundSourceText = investorFundSourceText;
    }

    public String getAccountDescription() {
        return accountDescription;
    }

    public void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }

    public String getInvestorInvestmentObjective() {
        return investorInvestmentObjective;
    }

    public void setInvestorInvestmentObjective(String investorInvestmentObjective) {
        this.investorInvestmentObjective = investorInvestmentObjective;
    }

    public String getInvestorMothersMaidenName() {
        return investorMothersMaidenName;
    }

    public void setInvestorMothersMaidenName(String investorMothersMaidenName) {
        this.investorMothersMaidenName = investorMothersMaidenName;
    }

    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }
}
