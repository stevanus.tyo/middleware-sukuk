package id.co.bsm.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "M_SYS_PROPERTIES")
@Entity
public class SystemProperties {

    @Id
    @Column(name = "PROP_KEY", nullable = false, length = 100)
    private String propKey;

    @Column(name = "PROP_VALUE", length = 100)
    private String propValue;

    @Column(name = "DESCRIPTION", length = 100)
    private String description;

    public String getPropKey() {
        return propKey;
    }

    public void setPropKey(String propKey) {
        this.propKey = propKey;
    }

    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
