package id.co.bsm.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.DateTimeException;
import java.util.Date;

@XmlRootElement(name = "InvestorOrder")
@XmlAccessorType(value = XmlAccessType.FIELD)
@Table(name = "T_INVESTOR_ORDER")
@Entity
public class InvestorOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "BOOKING_CODE", unique = true, length = 50)
    private String bookingCode;

    @Column(name = "BILLING_CODE", unique = true, length = 50)
    private String billingCode;

    @Column(name = "NTPN", length = 50)
    private String ntpn;

    @Column(name = "NTB", length = 20)
    private String ntp;

    @Column(name = "SID", length = 50)
    private String sid;

    @Column(name = "NAME", length = 150)
    private String name;

    @Column(name = "SERIES_ID")
    private Long seriesId;

    @Column(name = "SERIES_NAME", length = 100)
    private String series;

    @Column(name = "SETTELMENT_DATE")
    private Date settelmentDate;

    @Column(name = "MATURITY_DATE")
    private Date maturityDate;

    @Column(name = "ORDER_DATE")
    private Date orderDate;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "OWNER_OVER")
    private BigDecimal ownerOver;

    @Column(name = "PAYMENT_DATE")
    private Date paymentDate;

    @Column(name = "CHANNEL")
    private String channel;

    @Column(name = "BRANCH")
    private String branch;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE}, optional = false)
    @JoinColumn(name = "SEQ_NUMBER", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Investor investors;

    public InvestorOrder() { }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public String getNtpn() {
        return ntpn;
    }

    public void setNtpn(String ntpn) {
        this.ntpn = ntpn;
    }

    public String getNtp() {
        return ntp;
    }

    public void setNtp(String ntp) {
        this.ntp = ntp;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(Long seriesId) {
        this.seriesId = seriesId;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public Date getSettelmentDate() {
        return settelmentDate;
    }

    public void setSettelmentDate(Date settelmentDate) {
        this.settelmentDate = settelmentDate;
    }

    public Date getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(Date maturityDate) {
        this.maturityDate = maturityDate;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getOwnerOver() {
        return ownerOver;
    }

    public void setOwnerOver(BigDecimal ownerOver) {
        this.ownerOver = ownerOver;
    }

    public Investor getInvestors() {
        return investors;
    }

    public void setInvestors(Investor investors) {
        this.investors = investors;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}

