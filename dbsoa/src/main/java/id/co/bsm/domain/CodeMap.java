package id.co.bsm.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "CodeMap")
@XmlAccessorType(value = XmlAccessType.FIELD)
@Entity
@Table(name = "M_CODE_MAP")
public class CodeMap implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private CodeKey codeKey;

    @Column(name = "NAME", length = 100)
    private String name;

    @Column(name = "DESCRIPTION", length = 150)
    private String description;

    @Column(name = "IS_DEFAULT", length = 1)
    private char isDefault;

    public CodeMap() {  }

    public CodeMap(CodeKey codeHashMap, String description) {
        this.codeKey = codeHashMap;
        this.description = description;
    }

    public CodeKey getCodeKey() {
        return codeKey;
    }

    public void setCodeKey(CodeKey codeKey) {
        this.codeKey = codeKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public char getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(char isDefault) {
        this.isDefault = isDefault;
    }
}
