package id.co.bsm.service;

import id.co.bsm.domain.CodeKey;
import id.co.bsm.domain.CodeMap;
import id.co.bsm.repositories.CodeMapRepository;
import id.co.bsm.service.dto.CodeMapExcelDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CodeMapDbService {
    private final Logger log = LoggerFactory.getLogger(CodeMapDbService.class);

    @Autowired
    private CodeMapRepository codeMapRepository;

    public void saveCodeMap(List<CodeMapExcelDTO> codeMapExcelDTOS) {
        codeMapExcelDTOS.forEach(codeMapExcelDTO -> {
            CodeKey codeKey = new CodeKey();
            codeKey.setCodeName(codeMapExcelDTO.getCodeName());
            codeKey.setSysCode(codeMapExcelDTO.getSysCode());
            codeKey.setUserCode(codeMapExcelDTO.getUserCode());

            CodeMap codeMap = new CodeMap();
            codeMap.setName(codeMapExcelDTO.getName());
            codeMap.setIsDefault(codeMapExcelDTO.getIsDefault());
            codeMap.setCodeKey(codeKey);

            codeMapRepository.save(codeMap);
        });
    }
}
