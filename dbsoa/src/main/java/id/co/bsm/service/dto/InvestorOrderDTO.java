package id.co.bsm.service.dto;

import id.co.bsm.domain.InvestorOrder;

import java.math.BigDecimal;
import java.util.Date;

public class InvestorOrderDTO {
    private String bookingCode;

    private String billingCode;

    private String ntpn;

    private String ntp;

    private String sid;

    private String name;

    private Long seriesId;

    private String series;

    private Date settelmentDate;

    private Date maturityDate;

    private Date orderDate;

    private BigDecimal amount;

    private BigDecimal ownerOver;

    private Date paymentDate;

    private String channel;

    public InvestorOrderDTO() { }

    public InvestorOrderDTO(InvestorOrder investorOrder) {
        this.bookingCode = investorOrder.getBookingCode();
        this.billingCode = investorOrder.getBillingCode();
        this.ntpn = investorOrder.getNtp();
        this.sid = investorOrder.getSid();
        this.name = investorOrder.getName();
        this.seriesId = investorOrder.getSeriesId();
        this.series = investorOrder.getSeries();
        this.settelmentDate = investorOrder.getSettelmentDate();
        this.maturityDate = investorOrder.getMaturityDate();
        this.orderDate = investorOrder.getOrderDate();
        this.amount = investorOrder.getAmount();
        this.ownerOver = investorOrder.getOwnerOver();
        this.paymentDate = investorOrder.getPaymentDate();
        this.channel = investorOrder.getChannel();
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public String getNtpn() {
        return ntpn;
    }

    public void setNtpn(String ntpn) {
        this.ntpn = ntpn;
    }

    public String getNtp() {
        return ntp;
    }

    public void setNtp(String ntp) {
        this.ntp = ntp;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(Long seriesId) {
        this.seriesId = seriesId;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public Date getSettelmentDate() {
        return settelmentDate;
    }

    public void setSettelmentDate(Date settelmentDate) {
        this.settelmentDate = settelmentDate;
    }

    public Date getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(Date maturityDate) {
        this.maturityDate = maturityDate;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getOwnerOver() {
        return ownerOver;
    }

    public void setOwnerOver(BigDecimal ownerOver) {
        this.ownerOver = ownerOver;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
