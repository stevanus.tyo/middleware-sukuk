package id.co.bsm.service;

import id.co.bsm.domain.*;
import id.co.bsm.enumeration.Constants;
import id.co.bsm.repositories.*;
import id.co.bsm.service.dto.InvestorExcelDTO;
import id.co.bsm.service.dto.InvestorProfileDTO;
import id.co.bsm.service.dto.InvestorRegisterDTO;
import id.co.bsm.util.DateUtil;
import id.co.bsm.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class InvestorDbService {
    private final Logger log = LoggerFactory.getLogger(InvestorDbService.class);

    @Autowired
    private InvestorRepository investorRepository;

    @Autowired
    private InvestorSbnRepository investorSbnRepository;

    @Autowired
    private InvestorSbnBankRepository investorSbnBankRepository;

    @Autowired
    private InvestorExchangeRepository investorExchangeRepository;

    @Autowired
    private CodeMapRepository codeMapRepository;

    @Autowired
    private InvestorOrderRepository investorOrderRepository;

    @Autowired
    private BranchRepository branchRepository;

    /* Investor */
    public String findInvestorStatus(String cif) {
        Investor investor = this.investorRepository.findInvestorByCifNum(cif);
        return (investor==null)? Constants.InvestorStatus.NEW.name():investor.getInvestorStatus();
    }

    public InvestorProfileDTO findInvestorProfile(String cif) {
        return new InvestorProfileDTO(investorRepository.findInvestorByCifNum(cif));
    }

    public List<InvestorOrder> findInvestorOrderHistory(String cif) {
        Investor investor = investorRepository.findInvestorByCifNum(cif);
        return investor.getOrders();
    }

    public String findInvestorSidByCif(String cif) {
        Investor investor = investorRepository.findInvestorByCifNum(cif);
        return investor.getSidNum();
    }

    public Investor findActiveInvestorByCif(String cif) {
        return investorRepository.findInvestorByCifNumAndInvestorStatus(cif, Constants.InvestorStatus.ACTIVE.name());
    }

    public Investor findInvestorByCifNum(String cif) {
        return investorRepository.findInvestorByCifNum(cif);
    }

    public Investor findInvestorBySidNum(String sid) {
        return investorRepository.findInvestorBySidNum(sid);
    }

    public void saveInvestor(Investor investor) {
        investorRepository.save(investor);
    }

    public InvestorRegisterDTO investorRegisterComplate(String sid) {
        Investor investor = investorRepository.findInvestorBySidNum(sid);

        InvestorRegisterDTO investorRegisterDTO = new InvestorRegisterDTO();
        investorRegisterDTO.setName(investor.getCustName());
        investorRegisterDTO.setCif(investor.getCifNum());
        investorRegisterDTO.setSid(investor.getSidNum());
        investorRegisterDTO.setSre(investor.getExchange().get(0).getSreNum());

        return investorRegisterDTO;
    }

    public void saveInvestor(List<InvestorExcelDTO> investorExcelDTOS) {
        investorExcelDTOS.forEach(investorExcelDTO -> {
            /* Investor */
            Investor investor = investorRepository.findInvestorByCifNum(investorExcelDTO.getCif());
            if(investor == null) investor = new Investor();

            investor.setCifNum(investorExcelDTO.getCif());
            investor.setSidNum(investorExcelDTO.getSid());
            investor.setCustName(investorExcelDTO.getName());
            investor.setInvestorStatus(Constants.InvestorStatus.REGISTER.name());

            /* Investor Profile */
            InvestorProfile investorProfile = new InvestorProfile();
            investorProfile.setInvestor(investor);
            investorProfile.setInvestorFirstName(investorExcelDTO.getName());
            investorProfile.setInvestorKTPNumber(investorExcelDTO.getKtp());
            investorProfile.setInvestorBirthPlace(investorExcelDTO.getBirthPlace());
            investorProfile.setInvestorBirthDate(DateUtil.getInstance().get(investorExcelDTO.getBirthDate()));

            investorProfile.setInvestorAddress1(StringUtil.getInstance()
                    .forLengthEmpty(investorExcelDTO.getAddress(), 60, 1));
            investorProfile.setInvestorAddress2(StringUtil.getInstance()
                    .forLengthEmpty(investorExcelDTO.getAddress(), 60, 2));
            investorProfile.setInvestorAddress3(StringUtil.getInstance()
                    .forLengthEmpty(investorExcelDTO.getAddress(), 60, 3));

            investorProfile.setInvestorCity(investorExcelDTO.getCity());
            investorProfile.setInvestorProvince(investorExcelDTO.getProvince());
            investorProfile.setInvestorHomePhone(investorExcelDTO.getPhoneNo());
            investorProfile.setInvestorEmail(investorExcelDTO.getEmail());
            investorProfile.setInvestorSex(investorExcelDTO.getSex());
            investorProfile.setInvestorMaritalStatus(investorExcelDTO.getMaritalStatus());
            investorProfile.setInvestorEducationalBackground(investorExcelDTO.getEducation());
            investorProfile.setInvestorOccupation(investorExcelDTO.getOccupation());
            investorProfile.setInvestorIncomePerAnnum(investorExcelDTO.getIncome());
            investorProfile.setInvestorFundSource(investorExcelDTO.getFundSource());
            investorProfile.setInvestorMothersMaidenName(investorExcelDTO.getMotherMaidenName());

            /* investor exchange */
            List<InvestorExchange> investorExchanges = investor.getExchange();
            if(investorExchanges == null) investorExchanges = new ArrayList<InvestorExchange>();

            InvestorExchange investorExchange = new InvestorExchange();
            investorExchange.setInvestors(investor);
            investorExchange.setSreNum(investorExcelDTO.getSreNo());
            investorExchange.setSreName(investorExcelDTO.getSreName());
            investorExchanges.add(investorExchange);

            /* Save investor Detail */
            investor.setInvestorProfile(investorProfile);
            investor.setExchange(investorExchanges);
            investorRepository.save(investor);
        });
    }

    /* Code Map */
    public String findUserCodeMapBySys(String name, String key) {
        CodeMap codeMap = codeMapRepository.findCodeMapByCodeNameAndSysCode(name, key);
        if(codeMap==null)
            codeMap = codeMapRepository.findCodeMapByCodeNameAndIsDefault(name, Constants.IS_DEFAULT);

        return codeMap.getCodeKey().getUserCode();
    }

    /* Investor Sbn */
    public void saveInvestorSbn(InvestorSbn investorSbn) {
        investorSbnRepository.save(investorSbn);
    }

    public InvestorSbn findInvestorSbnByInvestorSidNum(String sid) {
        return investorSbnRepository.findInvestorSbnByInvestorSidNum(sid);
    }

    /* Investor Sbn Bank */
    public InvestorSbnBank findInvestorSbnBankByAcctNo(String acctno) {
        return investorSbnBankRepository.findByAccountNo(acctno);
    }

    public void saveInvestorSbnBank(InvestorSbnBank investorSbnBank) {
        investorSbnBankRepository.save(investorSbnBank);
    }

    /* Investor Surat Berharga */
    public void saveInvestorExchange(InvestorExchange investorExchange) {
        investorExchangeRepository.save(investorExchange);
    }

    /* Order */
    public void saveOrder(InvestorOrder investorOrder) {
        investorOrderRepository.save(investorOrder);
    }

    public List<Branch> getAllBranch() {
        return branchRepository.findAll();
    }
}
