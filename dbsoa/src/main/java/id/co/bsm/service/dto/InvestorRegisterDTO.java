package id.co.bsm.service.dto;

public class InvestorRegisterDTO {
    private String name;

    private String cif;

    private String sid;

    private String sre;

    private String status;

    public InvestorRegisterDTO () { }

    public InvestorRegisterDTO(String name, String cif
            , String sid, String sre, String status) {
        this.cif = cif;
        this.sid = sid;
        this.sre = sre;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSre() {
        return sre;
    }

    public void setSre(String sre) {
        this.sre = sre;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
