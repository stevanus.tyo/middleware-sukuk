package id.co.bsm.service.dto;

import id.co.bsm.domain.Investor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InvestorProfileDTO {
    private String seqNum;

    private String cifNum;

    private String sidNum;

    private String name;

    private List<String> sreNum;

    public InvestorProfileDTO() { }

    public InvestorProfileDTO(Investor investor) {
        this.seqNum = investor.getSeqNum();
        this.cifNum = investor.getCifNum();
        this.sidNum = investor.getSidNum();
        this.name = investor.getCustName();
        this.sreNum = investor.getExchange().stream().map(sre -> sre.getSreNum()).collect(Collectors.toList());
    }

    public String getSeqNum() {
        return seqNum;
    }

    public void setSeqNum(String seqNum) {
        this.seqNum = seqNum;
    }

    public String getCifNum() {
        return cifNum;
    }

    public void setCifNum(String cifNum) {
        this.cifNum = cifNum;
    }

    public String getSidNum() {
        return sidNum;
    }

    public void setSidNum(String sidNum) {
        this.sidNum = sidNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSreNum() {
        return sreNum;
    }

    public void setSreNum(List<String> sreNum) {
        this.sreNum = sreNum;
    }
}
