package id.co.bsm.service;

import id.co.bsm.domain.Branch;
import id.co.bsm.repositories.BranchRepository;
import id.co.bsm.service.dto.BranchExcelDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BranchDbService {
    private final Logger log = LoggerFactory.getLogger(BranchDbService.class);

    @Autowired
    private BranchRepository branchRepository;

    public void saveBranch(List<BranchExcelDTO> branchExcelDTOS) {
        branchExcelDTOS.forEach(codeMapExcelDTO -> {
            Branch branch = new Branch();
            branch.setCode(codeMapExcelDTO.getCode());
            branch.setName(codeMapExcelDTO.getName());
            branchRepository.save(branch);
        });
    }

    public List<Branch> getAllBranch() {
        return branchRepository.findAll();
    }
}
