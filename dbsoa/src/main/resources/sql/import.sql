INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.upload.investor.path','/Users/hermanto/Workspace/bsmsoa/upload/investor');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.upload.codemap.path','/Users/hermanto/Workspace/bsmsoa/upload/codemap');
-- Sbn Key
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.server.api.url','v1');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.server.api.base','https://test-apisbn.kemenkeu.go.id/');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.server.api.id','8faf1b7c13404581985cea9ebb6b3dcd');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.server.api.key','WgJkun1iGnxtZ0Yb4rJVR5hCcT0Ha6bD/H7nD3C4rX8=');
-- Sbn Property Path Map
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.bank','Bank');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.bankperception','bankpersepsi');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.channel','channelpembayaran');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.statustransaksi','statustransaksi');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.investor','investor');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.investorbank','rekeningdana');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.investorsec','rekeningsb');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.occupation','pekerjaan');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.sex','jeniskelamin');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.province','provinsi');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.subregistry','SubRegistry');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.series','Seri');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.order','pemesanan');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.redeem','redemption');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.partisipan','Partisipan');
INSERT INTO M_SYS_PROPERTIES (PROP_KEY,PROP_VALUE) VALUE ('sbn.map.api.quota','Kuota');
