package id.co.bsm.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class LoadFileRoute extends RouteBuilder {

    @Value("${sbn.upload.path.investor}")
    private String investorPath;

    @Value("${sbn.upload.path.codemap}")
    private String codeMapPath;

    @Value("${sbn.upload.path.branch}")
    private String branchPath;

    @Override
    public void configure() throws Exception {
        String investorFile = investorPath + "?move=./processed/"
                + String.valueOf((System.currentTimeMillis() / 1000L))
                + ".${file:name}&moveFailed=./error/"
                + String.valueOf((System.currentTimeMillis() / 1000L))
                +".${file:name}&initialDelay=10000";

        from("file:"+ investorFile ).to("log:input")
                .bean("excelFileService", "readInvestorFile");


        String codeMapFile = codeMapPath + "?move=./processed/"
                + String.valueOf((System.currentTimeMillis() / 1000L))
                + ".${file:name}&moveFailed=./error/"
                + String.valueOf((System.currentTimeMillis() / 1000L))
                +".${file:name}&initialDelay=10000";

        from("file:"+ codeMapFile ).to("log:input")
                .bean("excelFileService", "readCodeMapFile");

        String branchFile = branchPath + "?move=./processed/"
                + String.valueOf((System.currentTimeMillis() / 1000L))
                + ".${file:name}&moveFailed=./error/"
                + String.valueOf((System.currentTimeMillis() / 1000L))
                +".${file:name}&initialDelay=10000";

        from("file:"+ branchFile ).to("log:input")
                .bean("excelFileService", "branchFile");
    }
}
