package id.co.bsm.service;

import id.co.bsm.service.dto.BranchExcelDTO;
import id.co.bsm.service.dto.CodeMapExcelDTO;
import id.co.bsm.service.dto.InvestorExcelDTO;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.util.StringUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExcelFileService implements Serializable {
    private static final long serialVersionUID = 1L;

    @Autowired
    private InvestorDbService investorDbService;

    @Autowired
    private CodeMapDbService codeMapDbService;

    @Autowired
    private BranchDbService branchDbService;

    @Transactional
    public void readInvestorFile(File _file) throws BsmSoaException {
        List<InvestorExcelDTO> investorExcelDTOS = new ArrayList<InvestorExcelDTO>();
        try {
            XSSFSheet sheet = (new XSSFWorkbook(new FileInputStream(_file))).getSheetAt(0);
            sheet.forEach(row -> {
                if (row.getRowNum() == 0) return;

                InvestorExcelDTO investorExcelDTO = new InvestorExcelDTO();
                investorExcelDTO.setSid(StringUtil.getInstance().getCellString(row, 0));
                investorExcelDTO.setCif(StringUtil.getInstance().getCellString(row, 1));
                investorExcelDTO.setName(StringUtil.getInstance().getCellString(row, 2));
                investorExcelDTO.setKtp(StringUtil.getInstance().getCellString(row, 3));
                investorExcelDTO.setBirthPlace(StringUtil.getInstance().getCellString(row, 4));
                investorExcelDTO.setBirthDate(StringUtil.getInstance().getCellString(row, 5));
                investorExcelDTO.setSex(StringUtil.getInstance().getCellString(row, 6));
                investorExcelDTO.setMaritalStatus(StringUtil.getInstance().getCellString(row, 7));
                investorExcelDTO.setEducation(StringUtil.getInstance().getCellString(row, 8));
                investorExcelDTO.setOccupation(StringUtil.getInstance().getCellString(row, 9));
                investorExcelDTO.setIncome(StringUtil.getInstance().getCellString(row, 10));
                investorExcelDTO.setFundSource(StringUtil.getInstance().getCellString(row, 11));
                investorExcelDTO.setProvince(StringUtil.getInstance().getCellString(row, 12));
                investorExcelDTO.setCity(StringUtil.getInstance().getCellString(row, 13));
                investorExcelDTO.setAddress(StringUtil.getInstance().getCellString(row, 14));
                investorExcelDTO.setPhoneNo(StringUtil.getInstance().getCellString(row, 15));
                investorExcelDTO.setEmail(StringUtil.getInstance().getCellString(row, 16));
                investorExcelDTO.setMotherMaidenName(StringUtil.getInstance().getCellString(row, 17));
                investorExcelDTO.setSreNo(StringUtil.getInstance().getCellString(row, 18));
                investorExcelDTO.setSreName(StringUtil.getInstance().getCellString(row, 19));

                investorExcelDTOS.add(investorExcelDTO);
            });
        } catch (Exception e) {
            throw new BsmSoaException(e);
        }

        investorDbService.saveInvestor(investorExcelDTOS);
    }

    @Transactional
    public void readCodeMapFile(File _file) throws BsmSoaException {
        List<CodeMapExcelDTO> codeMapExcelDTOS = new ArrayList<CodeMapExcelDTO>();

        try {
            XSSFSheet sheet = (new XSSFWorkbook(new FileInputStream(_file))).getSheetAt(0);
            sheet.forEach(row -> {
                if (row.getRowNum() == 0) return;

                CodeMapExcelDTO codeMapExcelDTO = new CodeMapExcelDTO();
                codeMapExcelDTO.setCodeName(StringUtil.getInstance().getCellString(row, 0));
                codeMapExcelDTO.setSysCode(StringUtil.getInstance().getCellString(row, 1));
                codeMapExcelDTO.setUserCode(StringUtil.getInstance().getCellString(row, 2));
                codeMapExcelDTO.setName(StringUtil.getInstance().getCellString(row, 3));

                if(StringUtil.getInstance().getCellString(row, 4)!=null)
                    codeMapExcelDTO.setIsDefault('Y');

                codeMapExcelDTOS.add(codeMapExcelDTO);
            });
        } catch (Exception e) {
            throw new BsmSoaException(e);
        }

        codeMapDbService.saveCodeMap(codeMapExcelDTOS);
    }

    @Transactional
    public void branchFile(File _file) throws BsmSoaException {
        List<BranchExcelDTO> branchExcelDTOS = new ArrayList<BranchExcelDTO>();

        try {
            XSSFSheet sheet = (new XSSFWorkbook(new FileInputStream(_file))).getSheetAt(0);
            sheet.forEach(row -> {
                if (row.getRowNum() == 0) return;

                BranchExcelDTO branchExcelDTO = new BranchExcelDTO();
                branchExcelDTO.setCode(StringUtil.getInstance().getCellString(row, 0));
                branchExcelDTO.setName(StringUtil.getInstance().getCellString(row, 1));
                branchExcelDTOS.add(branchExcelDTO);
            });
        } catch (Exception e) {
            throw new BsmSoaException(e);
        }

        branchDbService.saveBranch(branchExcelDTOS);

    }
}
