package id.co.bsm.client.sbn;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import id.co.bsm.enumeration.Constants;
import id.co.bsm.service.dto.SbnRequestDTO;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.util.StringUtil;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SbnRequestHandler {
    private static final String UTF8 = "UTF-8";
    private static final String ALGORITH = "HmacSHA256";
    private static final String MD5 = "MD5";

    private Client wsclient = ClientBuilder.newClient();
    private SbnRequestDTO sbnRequestDTO;
    private Map<String, String> requestData = new HashMap<String, String>();

    private String version;

    private ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();

    public SbnRequestHandler() { }

    public SbnRequestHandler(SbnRequestDTO sbnRequestDTO) {
        this.sbnRequestDTO = sbnRequestDTO;
    }

    public Response get(String _path, String _query, String _mssg) throws BsmSoaException {
        try {
            this.requestData.put(Constants.HendlerEnum.METHOD.name(), Constants.RequestMethod.GET.name().toUpperCase());

            _mssg = StringUtil.getInstance().replaceSbnChar(_mssg);

            String fullUrl = this.sbnRequestDTO.getSbnApiRef()
                    + ((version!=null)?version:this.sbnRequestDTO.getSbnUrl())
                    + "/" + _path;

            if (!_query.equals(""))
                fullUrl = fullUrl + "?" + _query;

            this.genAuthString(fullUrl, _mssg);

            Invocation.Builder builder = wsclient.target(this.sbnRequestDTO.getSbnApiRef())
                    .path(((version!=null)?version:this.sbnRequestDTO.getSbnUrl()) + "/" + _path)
                    .queryParam(_query)
                    .request(MediaType.APPLICATION_JSON)
                    .header("Content-type", MediaType.APPLICATION_JSON)
                    .header("Authorization", "amx " + this.requestData.get(Constants.HendlerEnum.AUTH.name()));

            return builder.get();
        }catch (Exception e) {
            throw new BsmSoaException(e);
        }
    }

    public Response put(String _path, Object _omssg) throws BsmSoaException {
        try {
            this.requestData.put(Constants.HendlerEnum.METHOD.name(), Constants.RequestMethod.PUT.name().toUpperCase());

            String _mssg = objectWriter.writeValueAsString(_omssg);
            _mssg = StringUtil.getInstance().replaceSbnChar(_mssg);

            String fullUrl = this.sbnRequestDTO.getSbnApiRef()
                    + ((version!=null)?version:this.sbnRequestDTO.getSbnUrl())
                    + "/" + _path;

            this.genAuthString(fullUrl, _mssg);

            Invocation.Builder builder = wsclient.target(this.sbnRequestDTO.getSbnApiRef())
                    .path(((version!=null)?version:this.sbnRequestDTO.getSbnUrl()) + "/" + _path)
                    .request(MediaType.APPLICATION_JSON)
                    .header("Content-type", MediaType.APPLICATION_JSON)
                    .header("Authorization", "amx " + this.requestData.get(Constants.HendlerEnum.AUTH.name()));
            return builder.put(Entity.entity(_mssg, MediaType.APPLICATION_JSON));
        } catch (Exception e) {
            throw new BsmSoaException(e);
        }
    }

    public Response delete(String _path, String _omssg) throws BsmSoaException {
        try {
            this.requestData.put(Constants.HendlerEnum.METHOD.name(), Constants.RequestMethod.DELETE.name().toUpperCase());

            String _mssg = objectWriter.writeValueAsString(_omssg);
            _mssg = StringUtil.getInstance().replaceSbnChar(_mssg);

            String fullUrl = this.sbnRequestDTO.getSbnApiRef()
                    + ((version!=null)?version:this.sbnRequestDTO.getSbnUrl())
                    + "/" + _path;

            this.genAuthString(fullUrl, _mssg);

            Invocation.Builder builder = wsclient.target(this.sbnRequestDTO.getSbnApiRef())
                    .path(((version!=null)?version:this.sbnRequestDTO.getSbnUrl()) + "/" + _path)
                    .request(MediaType.APPLICATION_JSON)
                    .header("Content-type", MediaType.APPLICATION_JSON)
                    .header("Authorization", "amx " + this.requestData.get(Constants.HendlerEnum.AUTH.name()));

            return builder.delete();
        } catch (Exception e) {
            throw new BsmSoaException(e);
        }
    }

    public Response post(String _path, Object _omssg) throws BsmSoaException {
        try {
            requestData.put(Constants.HendlerEnum.METHOD.name(), Constants.RequestMethod.POST.name().toUpperCase());

            String _mssg = objectWriter.writeValueAsString(_omssg);
            _mssg = StringUtil.getInstance().replaceSbnChar(_mssg);

            String fullUrl = this.sbnRequestDTO.getSbnApiRef()
                    + ((version!=null)?version:this.sbnRequestDTO.getSbnUrl())
                    + "/" + _path;

            this.genAuthString(fullUrl, _mssg);

            Invocation.Builder builder = wsclient.target(this.sbnRequestDTO.getSbnApiRef())
                    .path(((version!=null)?version:this.sbnRequestDTO.getSbnUrl()) + "/" + _path)
                    .request(MediaType.APPLICATION_JSON)
                    .header("Content-type", MediaType.APPLICATION_JSON)
                    .header("Authorization", "amx " + this.requestData.get(Constants.HendlerEnum.AUTH.name()));

            return builder.post(Entity.entity(_mssg, MediaType.APPLICATION_JSON));
        } catch (Exception e) {
            throw new BsmSoaException(e);
        }
    }

    private void genAuthString(String url, String _mssg) throws UnsupportedEncodingException
            , NoSuchAlgorithmException, InvalidKeyException {
        /* Url Encode */
        String urlEncode = URLEncoder.encode((url).toLowerCase(), UTF8).toLowerCase();

        /* Url Path */
        requestData.put(Constants.HendlerEnum.URL.name(), urlEncode);

        this.requestData.put(Constants.HendlerEnum.TIME.name(), String.valueOf((System.currentTimeMillis() / 1000L)));
        this.requestData.put(Constants.HendlerEnum.NONCE.name(), UUID.randomUUID().toString().replaceAll("[^A-Za-z0-9 ]", ""));
        this.requestData.put(Constants.HendlerEnum.BODY.name(), this.generateBase64(_mssg));

        String reqBuf = this.sbnRequestDTO.getSbnApiId()
                + this.requestData.get(Constants.HendlerEnum.METHOD.name())
                + this.requestData.get(Constants.HendlerEnum.URL.name())
                + this.requestData.get(Constants.HendlerEnum.TIME.name())
                + this.requestData.get(Constants.HendlerEnum.NONCE.name())
                + this.requestData.get(Constants.HendlerEnum.BODY.name());

        this.requestData.put(Constants.HendlerEnum.SIGN64.name(), this.generateSignature(sbnRequestDTO.getSbnApiKey(), reqBuf));

        String authString = this.sbnRequestDTO.getSbnApiId() + ":"
                + this.requestData.get(Constants.HendlerEnum.SIGN64.name()) + ":"
                + this.requestData.get(Constants.HendlerEnum.NONCE.name()) + ":"
                + this.requestData.get(Constants.HendlerEnum.TIME.name());

        this.requestData.put(Constants.HendlerEnum.AUTH.name(), Base64.encodeBase64String(authString.getBytes(UTF8)));
    }

    private String generateSignature(String key, String mssg) throws NoSuchAlgorithmException
            , UnsupportedEncodingException, InvalidKeyException {
        Mac sha256_HMAC = Mac.getInstance(ALGORITH);
        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(UTF8), ALGORITH);
        sha256_HMAC.init(secret_key);

        byte[] bytes = sha256_HMAC.doFinal(mssg.getBytes(UTF8));
        return Base64.encodeBase64String(bytes);
    }

    private String generateBase64(String mssg) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if(mssg.equals("")) mssg = "\"\"";

        MessageDigest md = MessageDigest.getInstance(MD5);
        md.update(mssg.getBytes(UTF8));
        byte[] digest = md.digest();

        return Base64.encodeBase64String(digest);
    }

    public SbnRequestDTO getSbnRequestDTO() {
        return sbnRequestDTO;
    }

    public void setSbnRequestDTO(SbnRequestDTO sbnRequestDTO) {
        this.sbnRequestDTO = sbnRequestDTO;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
