package id.co.bsm.predicate;

import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;

public class RegisterStatePredicate implements Predicate {
    private String state;

    public RegisterStatePredicate(String state) {
        this.state = state;
    }

    @Override
    public boolean matches(Exchange exchange) {
        exchange.getIn().getHeader("STATE");
        return false;
    }
}
