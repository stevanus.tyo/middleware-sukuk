package id.co.bsm.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.util.ArrayList;
import java.util.List;

public class ListObjectWsdlProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        if (exchange.getIn().getBody() instanceof ArrayList) {
            Object object = exchange.getIn().getBody(Object.class);

            List<Object> objectList = new ArrayList<Object>();
            objectList.add(object);
            exchange.getOut().setBody(objectList);
        }
    }
}
