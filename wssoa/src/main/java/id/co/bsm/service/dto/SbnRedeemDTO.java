package id.co.bsm.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnRedeemDTO {
    @JsonProperty("TrxId")
    private String trnId;

    @JsonProperty("NamaInvestor")
    private String name;

    @JsonProperty("KodeRedeem")
    private String redeemCode;

    @JsonProperty("Seri")
    private String series;

    @JsonProperty("TglSetelmen")
    private String settelmentDate;

    @JsonProperty("SisaKepemilikan")
    private Number ownerOver;

    @JsonProperty("Status")
    private String status;

    @JsonProperty("CreatedBy")
    private String createBy;

    @JsonProperty("TglRedeem")
    private String redeemDate;

    @JsonProperty("KelipatanRedeem")
    private BigDecimal redeemMulti;

    @JsonProperty("MinimalRedeem")
    private BigDecimal minRedeem;

    @JsonProperty("MaksimalRedeem")
    private BigDecimal maxRedeem;

    @JsonProperty("Redeemable")
    private BigDecimal redeemable;

    @JsonProperty("KodePemesanan")
    private String orderCode;

    @JsonProperty("Sid")
    private String sid;

    @JsonProperty("Nominal")
    private BigDecimal amount;

    public SbnRedeemDTO() { }

    public String getTrnId() {
        return trnId;
    }

    public void setTrnId(String trnId) {
        this.trnId = trnId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRedeemCode() {
        return redeemCode;
    }

    public void setRedeemCode(String redeemCode) {
        this.redeemCode = redeemCode;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getSettelmentDate() {
        return settelmentDate;
    }

    public void setSettelmentDate(String settelmentDate) {
        this.settelmentDate = settelmentDate;
    }

    public Number getOwnerOver() {
        return ownerOver;
    }

    public void setOwnerOver(Number ownerOver) {
        this.ownerOver = ownerOver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getRedeemDate() {
        return redeemDate;
    }

    public void setRedeemDate(String redeemDate) {
        this.redeemDate = redeemDate;
    }

    public BigDecimal getRedeemMulti() {
        return redeemMulti;
    }

    public void setRedeemMulti(BigDecimal redeemMulti) {
        this.redeemMulti = redeemMulti;
    }

    public BigDecimal getMinRedeem() {
        return minRedeem;
    }

    public void setMinRedeem(BigDecimal minRedeem) {
        this.minRedeem = minRedeem;
    }

    public BigDecimal getMaxRedeem() {
        return maxRedeem;
    }

    public void setMaxRedeem(BigDecimal maxRedeem) {
        this.maxRedeem = maxRedeem;
    }

    public BigDecimal getRedeemable() {
        return redeemable;
    }

    public void setRedeemable(BigDecimal redeemable) {
        this.redeemable = redeemable;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
