package id.co.bsm.service.ws;

import id.co.bsm.service.dto.*;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.service.param.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.*;
import java.util.List;

@WebService(serviceName = "wsdlapi", targetNamespace = "http://bsm.co.id/ws/sbn/wsdl/api/")
public interface SukukWsdlService {
    @WebMethod
    public List<SbnOccupationDTO> getOccupationWsdl()  throws BsmSoaException;

    @WebMethod
    public List<SbnSexDTO> getSexWsdl()  throws BsmSoaException;

    @WebMethod
    public List<SbnProvinceDTO> getProvinceWsdl()  throws BsmSoaException;

    @WebMethod
    public SbnProvinceDTO getCityWsdl(@WebParam(name = "id") Long id)  throws BsmSoaException;

    @WebMethod
    public List<SbnBankDTO> getBankWsdl()  throws BsmSoaException;

    @WebMethod
    public List<SbnBankPerceptionDTO> getBankPerceptionWsdl()  throws BsmSoaException;

    @WebMethod
    public List<SbnChannelDTO> getChannelWsdl()  throws BsmSoaException;

    @WebMethod
    public List<SbnSubRegistryDTO> getSubRegistryWsdl()  throws BsmSoaException;

    @WebMethod
    public List<SbnParticipantDTO> getParticipantWsdl() throws BsmSoaException;

    @WebMethod
    public List<SbnTrnCodeDTO> getTrnCodeWsdl() throws BsmSoaException;

    @WebMethod
    public SbnQuotaDTO getQuotaAllWsdl(@BeanParam QuotaParam quotaParam) throws BsmSoaException;

    @WebMethod
    public SbnQuotaDTO getQuotaSidWsdl(@BeanParam QuotaParam quotaParam) throws BsmSoaException;

    /*Series*/
    @WebMethod
    public List<SbnSeriesDTO> getSeriesWsdl() throws BsmSoaException;

    @WebMethod
    public SbnSeriesDTO getSeriesByIdWsdl(@WebParam(name = "id") Long id) throws BsmSoaException;

    @WebMethod
    public List<SbnSeriesDTO> getSeriesOfferWsdl() throws BsmSoaException;

    @WebMethod
    public List<SbnSeriesDTO> getSeriesRedeemWsdl() throws BsmSoaException;

    @WebMethod
    public SbnSeriesDTO getSeriesRedeemByIdWsdl(@WebParam(name = "id") Long id) throws BsmSoaException;

    @WebMethod
    public List<SbnSeriesDTO> getSeriesRedeemOfferWsdl() throws BsmSoaException;

    /* Investor */

    @WebMethod
    public SbnInvestorDTO getInvestorSelectionWsdl(@BeanParam InvestorParam investorParam) throws BsmSoaException;

    @WebMethod
    public SbnInvestorDTO getInvestorBySidWsdl(@WebParam(name = "sid") String sid) throws BsmSoaException;

    @WebMethod
    public SbnInvestorDTO postInvestorWsdl(SbnInvestorDTO sbnInvestorDTO) throws BsmSoaException;

    @WebMethod
    public SbnInvestorDTO putInvestorEditWsdl(SbnInvestorDTO sbnInvestorDTO) throws BsmSoaException;

    @WebMethod
    public SbnInvestorDTO putInvestorActivateWsdl(@WebParam(name = "sid") String sid) throws BsmSoaException;

    @WebMethod
    public SbnInvestorDTO putInvestorDeactivateWsdl(@WebParam(name = "sid") String sid) throws BsmSoaException;

    /*Rekening Sumber Dana*/
    @WebMethod
    public List<SbnInvestorBankDTO> getInvestorBankSelectionWsdl(@BeanParam InvestorBankParam investorBankParam) throws BsmSoaException;

    @WebMethod
    public SbnInvestorBankDTO getInvestorBankByIdWsdl(@WebParam(name = "id") Long id) throws BsmSoaException;

    @WebMethod
    public SbnInvestorBankDTO postInvestorBankWsdl(SbnInvestorBankDTO sbnInvestorBankDTO) throws BsmSoaException;

    @WebMethod
    public SbnInvestorBankDTO putInvestorBankEditWsdl(SbnInvestorBankDTO sbnInvestorBankDTO) throws BsmSoaException;

    @WebMethod
    public SbnMessageDTO deleteInvestorBankWsdl(@WebParam(name = "id") Long id) throws BsmSoaException;

    /*Rekening Surat Berharga*/
    @WebMethod
    public SbnInvestorSecDTO getInvestorSecSelectionWsdl(@BeanParam InvestorSecParam investorSecParam) throws BsmSoaException;

    @WebMethod
    public SbnInvestorSecDTO getInvestorSecByIdWsdl(@WebParam(name = "id") Long id) throws BsmSoaException;

    @WebMethod
    public SbnInvestorSecDTO postInvestorSecWsdl(SbnInvestorSecDTO sbnInvestorSecDTO) throws BsmSoaException;

    @WebMethod
    public SbnInvestorSecDTO putInvestorSecEditWsdl(SbnInvestorSecDTO sbnInvestorSecDTO) throws BsmSoaException;

    @WebMethod
    public SbnMessageDTO deleteInvestorSecWsdl(@WebParam(name = "id") Long id) throws BsmSoaException;

    /* Pemesanan */
    @WebMethod
    public SbnOrderingDTO postOrderWsdl(SbnOrderingDTO sbnOrderingDTO) throws BsmSoaException;

    @WebMethod
    public SbnOrderingDTO getOrderByIdSeriesAndTrnIdWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;


    @WebMethod
    public List<SbnOrderingDTO> getOrderRedeemBySidWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public SbnOrderingDTO getOrderHistoryBySeriesAndSidWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public List<SbnOrderingDTO> getOrderSeriesBySeriesAndSidWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public SbnOrderingDTO getOrderSeriesBySeriesAndSidAndTrnCodeWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public SbnOrderingDTO getOrderSeriesAllBySidWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public SbnOrderingDTO getOrderSeriesOfferBySidWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public SbnOrderingDTO getOrderByOrderCodeWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public SbnQueryOrderDTO getOrderSeriesBySeriesWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public SbnOrderingDTO getOrderSeriesBySeriesRangeWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public SbnOrderingDTO getOrderByPaymentStatusWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public SbnOrderingDTO getOrderSeriesAllWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public SbnOrderingDTO getOrderSeriesAllRangeWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public SbnOrderingDTO getOrderBillingWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;


    /* Redeem */
    @WebMethod
    public SbnRedeemDTO postRedeemWsdl(SbnRedeemDTO sbnOrderingDTO) throws BsmSoaException;

    @WebMethod
    public List<SbnRedeemDTO> geRedeemBySidWsdl(@BeanParam RedeemParam redeemParam) throws BsmSoaException;

    @WebMethod
    public List<SbnRedeemDTO> geRedeemBySeriesAndSidWsdl(@BeanParam RedeemParam redeemParam) throws BsmSoaException;

    @WebMethod
    public SbnRedeemDTO geRedeemByRedeemCodeWsdl(@BeanParam RedeemParam redeemParam) throws BsmSoaException;

    @WebMethod
    public SbnQueryRedeemDTO geRedeemBySeriesAndDateWsdl(@BeanParam RedeemParam redeemParam) throws BsmSoaException;

}
