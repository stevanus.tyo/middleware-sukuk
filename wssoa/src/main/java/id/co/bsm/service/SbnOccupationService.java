package id.co.bsm.service;

import com.fasterxml.jackson.core.type.TypeReference;
import id.co.bsm.service.dto.SbnOccupationDTO;
import id.co.bsm.config.SbnPropertyService;
import id.co.bsm.service.error.BsmSoaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Component
public class SbnOccupationService {
    @Value("${sbn.map.api.occupation}")
    private String PATH_URL;

    @Autowired
    private SbnPropertyService propertyService = SbnPropertyService.getInstance();

    private ResponseService responseUtil;

    public SbnOccupationService() {
        responseUtil = new ResponseService(propertyService);
    }

    public List<SbnOccupationDTO> getOccupation() throws BsmSoaException {
        List<SbnOccupationDTO> occupationDTO = new ArrayList<SbnOccupationDTO>();
        Response res = propertyService.getSbnRequestHandler().get(PATH_URL, "", "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnOccupationDTO>>(){});
    }
}
