package id.co.bsm.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnInvestorQueryDTO {
    private String name;

    private String ktpno;

    private String birthdate;

    public SbnInvestorQueryDTO() { }

    public SbnInvestorQueryDTO(String name, String ktpno, String birthdate) {
        this.name = name;
        this.ktpno = ktpno;
        this.birthdate = birthdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKtpno() {
        return ktpno;
    }

    public void setKtpno(String ktpno) {
        this.ktpno = ktpno;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
}
