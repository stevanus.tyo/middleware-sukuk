package id.co.bsm.service;

import com.fasterxml.jackson.core.type.TypeReference;
import id.co.bsm.service.dto.SbnOrderingDTO;
import id.co.bsm.service.dto.SbnQueryOrderDTO;
import id.co.bsm.config.SbnPropertyService;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.util.DateUtil;
import id.co.bsm.util.QueryAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.List;

@Component
public class SbnOrderService {
    @Value("${sbn.map.api.order}")
    private String PATH_URL;

    private static String V1_1 = "v1.1";

    @Autowired
    private SbnPropertyService propertyService = SbnPropertyService.getInstance();

    private ResponseService responseUtil;

    public SbnOrderService() {
        responseUtil = new ResponseService(propertyService);
    }

    public SbnOrderingDTO postOrder(SbnOrderingDTO sbnOrderingDTO) throws BsmSoaException {
        Response res = propertyService.getSbnRequestHandler().post(PATH_URL, sbnOrderingDTO);
        return this.responseUtil.getResponseDTO(res, 201, SbnOrderingDTO.class);
    }

    public SbnOrderingDTO getOrderByIdSeriesAndTrnId(String idSeries, String idTrn) throws BsmSoaException {
        String path = PATH_URL + "/seri/" + idSeries + "/trx/" + idTrn;
        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, SbnOrderingDTO.class);
    }

    public List<SbnOrderingDTO> getOrderRedeemBySid(String sid) throws BsmSoaException {
        String path = PATH_URL + "/redeemable/" + sid;
        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnOrderingDTO>>(){});
    }

    public List<SbnOrderingDTO> getOrderSeriesBySeriesAndSid(String idSeries, String sid) throws BsmSoaException {
        String path = PATH_URL + "/seri/" + idSeries + "/" + sid;
        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnOrderingDTO>>(){});
    }

    public SbnQueryOrderDTO getOrderSeriesBySeries(String idSeries
            , String search, String orderDate, String trncode, String pageNum) throws BsmSoaException {
        String path = PATH_URL + "/seri/" + idSeries;

        QueryAppend qparam = new QueryAppend();
        qparam.add("Search", search);
        qparam.add("TglPemesanan", DateUtil.getInstance().parse(orderDate));
        qparam.add("IdStatus", trncode);
        qparam.add("PageNumber", ((pageNum!=null)?pageNum:1));
        qparam.add("PageSize", "100");
        qparam.add("Sort", "SID");

        Response res = propertyService.getSbnRequestHandler().get(path, qparam.getRes(), "");
        return this.responseUtil.getResponseDTO(res, 200, SbnQueryOrderDTO.class);
    }

    public List<SbnOrderingDTO> getOrderSeriesAllBySid(String sid) throws BsmSoaException {
        String path = PATH_URL + "/seri/all/" + sid;
        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnOrderingDTO>>(){});
    }

    public SbnOrderingDTO getOrderByOrderCode(String ordercode) throws BsmSoaException {
        String path = PATH_URL + "/" + ordercode;
        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, SbnOrderingDTO.class);
    }

    public SbnOrderingDTO getOrderBilling(String billcode) throws BsmSoaException {
        String path = PATH_URL + "/billing/" + billcode;
        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, SbnOrderingDTO.class);
    }
}
