package id.co.bsm.service;


import com.fasterxml.jackson.core.type.TypeReference;
import id.co.bsm.service.dto.SbnInvestorSecDTO;
import id.co.bsm.service.dto.SbnMessageDTO;
import id.co.bsm.config.SbnPropertyService;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.util.DataDTOUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.List;

@Component
public class SbnInvestorSecService {
    @Value("${sbn.map.api.investorsec}")
    private String PATH_URL;

    @Autowired
    private SbnPropertyService propertyService = SbnPropertyService.getInstance();

    private ResponseService responseUtil;

    public SbnInvestorSecService() {
        responseUtil = new ResponseService(propertyService);
    }

    public SbnInvestorSecDTO getInvestorSecById(Long id) throws BsmSoaException {
        String path = PATH_URL + "/" + id;
        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, SbnInvestorSecDTO.class);
    }

    public List<SbnInvestorSecDTO> getInvestorSecBySid(String sid) throws BsmSoaException {
        String qparam = "sid=" + sid;
        Response res = propertyService.getSbnRequestHandler().get(PATH_URL, qparam, "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnInvestorSecDTO>>(){});
    }

    public SbnInvestorSecDTO postInvestorSec(SbnInvestorSecDTO sbnInvestorSecDTO) throws BsmSoaException {
        Response res = propertyService.getSbnRequestHandler().post(PATH_URL, sbnInvestorSecDTO);
        return this.responseUtil.getResponseDTO(res, 201, SbnInvestorSecDTO.class);
    }

    public SbnInvestorSecDTO putInvestorSecEdit(SbnInvestorSecDTO sbnInvestorSecDTO) throws BsmSoaException {
        SbnInvestorSecDTO sbninvOld = this.getInvestorSecById(sbnInvestorSecDTO.getId());

        String path = PATH_URL + "/" + sbnInvestorSecDTO.getId();

        Response res = propertyService.getSbnRequestHandler().put(path, (SbnInvestorSecDTO) DataDTOUtil
                .getInstance().update(sbninvOld, sbnInvestorSecDTO));

        return this.responseUtil.getResponseDTO(res, 200, SbnInvestorSecDTO.class);
    }

    public SbnMessageDTO deleteInvestorSec(String id) throws BsmSoaException {
        String path = PATH_URL + "/" + id;
        Response res = propertyService.getSbnRequestHandler().delete(path, "");
        return this.responseUtil.getResponseDTO(res, 200, SbnMessageDTO.class);
    }

}
