package id.co.bsm.service.param;

import javax.ws.rs.PathParam;;

public class QuotaParam {
    @PathParam("series")
    private Long series;

    @PathParam("sid")
    private String sid;

    @PathParam("cif")
    private String cif;

    public QuotaParam() { }

    public Long getSeries() {
        return series;
    }

    public void setSeries(Long series) {
        this.series = series;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }
}
