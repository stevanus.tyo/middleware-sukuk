package id.co.bsm.service.ws;

import id.co.bsm.domain.Branch;
import id.co.bsm.service.dto.*;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.service.param.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.BeanParam;
import java.util.List;

@WebService(serviceName = "inet", targetNamespace = "http://bsm.co.id/ws/sbn/wsdl/inet")
public interface InetWsdlService {
    /* Cek status investor */
    @WebMethod
    public String netInvestorStatusWsdl(@WebParam(name = "cif") String cif) throws BsmSoaException;

    @WebMethod
    public InvestorProfileDTO netInvestorProfileWsdl(@WebParam(name = "cif") String cif) throws BsmSoaException;

    /* Get product list offer */
    @WebMethod
    public List<SbnSeriesDTO> netSeriesOfferWsdl() throws BsmSoaException;

    /* get all product list */
    @WebMethod
    public List<SbnSeriesDTO> netSeriesAllWsdl() throws BsmSoaException;

    /* get quota investor berdasarkan series */
    @WebMethod
    public SbnQuotaDTO netQuotaWsdl(@BeanParam QuotaParam quotaParam) throws BsmSoaException;

    /* new investor */
    @WebMethod
    public String netInvestorNewWsdl(@BeanParam InvestorInetParam investorRegisterParam) throws BsmSoaException;

    /* register investor */
    @WebMethod
    public InvestorRegisterDTO netInvestorRegisterWsdl(@BeanParam InvestorInetParam investorRegisterParam) throws BsmSoaException;

    /* Order sbn */
    @WebMethod
    public SbnOrderingDTO netOrderWsdl(InvestorOrderParam investorOrderParam) throws BsmSoaException;

    /* Order History by API  */
    @WebMethod
    public List<SbnOrderingDTO> netOrderHistoryFromApiWsdl(@WebParam(name = "cif") String cif) throws BsmSoaException;

    @WebMethod
    public List<SbnOrderingDTO> netOrderHistoryBySeriesFromApiWsdl(@BeanParam HistoryInetParam orderHistoryInetParam) throws BsmSoaException;

    /* Order History By Db */
    @WebMethod
    public List<InvestorOrderDTO> netOrderHistoryFromDbWsdl(@WebParam(name = "cif") String cif) throws BsmSoaException;

    /* Order redeemable */
    @WebMethod
    public List<SbnOrderingDTO> netOrderRedeemableWsdl(@WebParam(name = "cif") String cif) throws BsmSoaException;

    /* Redeem sbn */
    @WebMethod
    public SbnRedeemDTO netRedeemWsdl(@BeanParam InvestorRedeemParam investorRedeemParam) throws BsmSoaException;

    /* Redeem history by api */
    @WebMethod
    public List<SbnRedeemDTO> netOrderRedemtionWsdl(@WebParam(name = "cif") String cif) throws BsmSoaException;

    @WebMethod
    public List<SbnRedeemDTO> netOrderRedemtionBySeriesWsdl(HistoryInetParam inetParam) throws BsmSoaException;

    @WebMethod
    public List<Branch> netBranchAll() throws BsmSoaException;
}
