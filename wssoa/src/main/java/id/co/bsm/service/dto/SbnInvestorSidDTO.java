package id.co.bsm.service.dto;


import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnInvestorSidDTO {
    String sid;

    public SbnInvestorSidDTO() { }

    public SbnInvestorSidDTO(String sid) {
        this.sid = sid;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
