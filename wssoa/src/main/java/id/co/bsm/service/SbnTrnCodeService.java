package id.co.bsm.service;

import com.fasterxml.jackson.core.type.TypeReference;
import id.co.bsm.service.dto.SbnTrnCodeDTO;
import id.co.bsm.config.SbnPropertyService;
import id.co.bsm.service.error.BsmSoaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.List;

@Component
public class SbnTrnCodeService {
    @Value("${sbn.map.api.statustransaksi}")
    private String PATH_URL;

    @Autowired
    private SbnPropertyService propertyService = SbnPropertyService.getInstance();

    private ResponseService responseUtil;

    public SbnTrnCodeService() {
        responseUtil = new ResponseService(propertyService);
    }

    public List<SbnTrnCodeDTO> getTrnCode() throws BsmSoaException {
        Response res = propertyService.getSbnRequestHandler().get(PATH_URL, "", "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnTrnCodeDTO>>(){});
    }
}
