package id.co.bsm.service.ws;

import id.co.bsm.service.*;
import id.co.bsm.service.dto.*;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.service.param.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Service
@Transactional
public class SbnApiServiceImpl implements Serializable {
    private static final long serialVersionUID = 1L;

    private final Logger log = LoggerFactory.getLogger(SbnApiServiceImpl.class);

    @Autowired
    private SbnInvestorService sbnInvestorService = new SbnInvestorService();

    @Autowired
    private SbnInvestorBankService sbnInvestorBankService = new SbnInvestorBankService();

    @Autowired
    private SbnInvestorSecService sbnInvestorSecService = new SbnInvestorSecService();

    @Autowired
    private SbnOccupationService sbnOccupationService = new SbnOccupationService();

    @Autowired
    private SbnSexService sbnSexService = new SbnSexService();

    @Autowired
    private SbnProvinceService sbnProvinceService = new SbnProvinceService();

    @Autowired
    private SbnBankService sbnBankService = new SbnBankService();

    @Autowired
    private SbnBankPerceptionService sbnBankPerceptionService = new SbnBankPerceptionService();

    @Autowired
    private SbnChannelService sbnChannelService = new SbnChannelService();

    @Autowired
    private SbnSubRegistryService sbnSubRegistryService = new SbnSubRegistryService();

    @Autowired
    private SbnSeriesService seriesService = new SbnSeriesService();

    @Autowired
    private SbnOrderService sbnOrderService = new SbnOrderService();

    @Autowired
    private SbnRedeemService sbnRedeemService = new SbnRedeemService();

    @Autowired
    private SbnParticipantService sbnParticipantService = new SbnParticipantService();

    @Autowired
    private SbnTrnCodeService sbnTrnCodeService = new SbnTrnCodeService();

    @Autowired
    private SbnQuotaService sbnQuotaService = new SbnQuotaService();

    private static String ACTIVATE = "activate";
    private static String DEACTIVATE = "deactivate";
    private static String BLANK = "";

    public List<SbnOccupationDTO> getOccupation() throws BsmSoaException {
        return this.sbnOccupationService.getOccupation();
    }

    public List<SbnSexDTO> getSex() throws BsmSoaException {
        return this.sbnSexService.getSex();
    }

    public List<SbnProvinceDTO> getProvince() throws BsmSoaException {
        return this.sbnProvinceService.getProvince();
    }

    public SbnProvinceDTO getCity(String id) throws BsmSoaException {
        return this.sbnProvinceService.getCity(id);
    }

    public List<SbnBankDTO> getBank() throws BsmSoaException {
        return this.sbnBankService.getBank();
    }

    public List<SbnBankPerceptionDTO> getBankPerception() throws BsmSoaException {
        return this.sbnBankPerceptionService.getBankPerception();
    }

    public List<SbnChannelDTO> getChannel() throws BsmSoaException {
        return this.sbnChannelService.getChannel();
    }

    public List<SbnSubRegistryDTO> getSubRegistry() throws BsmSoaException {
        return this.sbnSubRegistryService.getSubRegistry();
    }

    public List<SbnParticipantDTO> getParticipant() throws BsmSoaException {
        return this.sbnParticipantService.getParticipant();
    }

    public List<SbnTrnCodeDTO> getTrnCode() throws BsmSoaException {
        return this.sbnTrnCodeService.getTrnCode();
    }

    public SbnQuotaDTO getQuotaAll(QuotaParam quotaParam) throws BsmSoaException {
        return this.sbnQuotaService.getQuotaAll(quotaParam.getSeries());
    }

    public SbnQuotaDTO getQuotaSid(QuotaParam quotaParam) throws BsmSoaException {
        return this.sbnQuotaService.getQuotaSid(quotaParam.getSeries(), quotaParam.getSid());
    }

    /*Series*/
    public List<SbnSeriesDTO> getSeries() throws BsmSoaException {
        return this.seriesService.getSeries();
    }

    public SbnSeriesDTO getSeriesById(Long id) throws BsmSoaException {
        return this.seriesService.getSeriesById(id);
    }

    public List<SbnSeriesDTO> getSeriesOffer() throws BsmSoaException {
        return this.seriesService.getSeriesTypes("offer");
    }

    public List<SbnSeriesDTO> getSeriesRedeem() throws BsmSoaException {
        return this.seriesService.getSeriesTypes("redeem");
    }

    public SbnSeriesDTO getSeriesRedeemById(Long id) throws BsmSoaException {
        return this.seriesService.getSeriesType("redeem", id);
    }

    public List<SbnSeriesDTO> getSeriesRedeemOffer() throws BsmSoaException {
        return this.seriesService.getSeriesTypes("redeem/offer");
    }

    /*Investor*/
    public SbnInvestorDTO getInvestorBySid(String sid) throws BsmSoaException {
        return this.sbnInvestorService.getInvestorBySid(sid);
    }

    public SbnInvestorDTO getInvestorSelection(InvestorParam investorParam) throws BsmSoaException {
        String name = (investorParam.getName()!=null)?investorParam.getName():BLANK;
        String ktpno = (investorParam.getKtpno()!=null)?investorParam.getKtpno():BLANK;
        String birthdate = (investorParam.getBirthdate()!=null)?investorParam.getBirthdate():BLANK;

        name = name.replaceAll(" ", "");

        return this.sbnInvestorService.getInvestorSelection(name, ktpno, birthdate);
    }

    public SbnInvestorDTO postInvestor(SbnInvestorDTO sbnInvestorDTO) throws BsmSoaException {
        return this.sbnInvestorService.postInvestor(sbnInvestorDTO);
    }

    public SbnInvestorDTO putInvestorEdit(SbnInvestorDTO sbnInvestorDTO) throws BsmSoaException {
        return this.sbnInvestorService.putInvestorEdit(sbnInvestorDTO);
    }

    public SbnInvestorDTO putInvestorActivate(String sid) throws BsmSoaException {
        return this.sbnInvestorService.putInvestorAction(sid, ACTIVATE);
    }

    public SbnInvestorDTO putInvestorDeactivate(String sid) throws BsmSoaException {
        return this.sbnInvestorService.putInvestorAction(sid, DEACTIVATE);
    }

    /*Rekening Sumber Dana*/
    public SbnInvestorBankDTO postInvestorBank(SbnInvestorBankDTO sbnInvestorBankDTO) throws BsmSoaException {
        return this.sbnInvestorBankService.postInvestorBank(sbnInvestorBankDTO);
    }

    public SbnInvestorBankDTO putInvestorBankEdit(SbnInvestorBankDTO sbnInvestorBankDTO) throws BsmSoaException {
        return this.sbnInvestorBankService.putInvestorBankEdit(sbnInvestorBankDTO);
    }

    public SbnMessageDTO deleteInvestorBank(String id) throws BsmSoaException {
        return this.sbnInvestorBankService.deleteInvestorBank(id);
    }

    public SbnInvestorBankDTO getInvestorBankById(Long id) throws BsmSoaException {
        return this.sbnInvestorBankService.getInvestorBankById(id);
    }

    public List<SbnInvestorBankDTO> getInvestorBankSelection(InvestorBankParam investorBankParam) throws BsmSoaException {
        String sid = (investorBankParam.getSid()!=null)?investorBankParam.getSid():BLANK;
        return this.sbnInvestorBankService.getInvestorBankBySid(sid);
    }

    /*Rekening Surat Berharga*/
    public SbnInvestorSecDTO postInvestorSec(SbnInvestorSecDTO sbnInvestorSecDTO) throws BsmSoaException {
        return this.sbnInvestorSecService.postInvestorSec(sbnInvestorSecDTO);
    }

    public SbnInvestorSecDTO putInvestorSecEdit(SbnInvestorSecDTO sbnInvestorSecDTO) throws BsmSoaException {
        return this.sbnInvestorSecService.putInvestorSecEdit(sbnInvestorSecDTO);
    }

    public SbnMessageDTO deleteInvestorSec(String id) throws BsmSoaException {
        return this.sbnInvestorSecService.deleteInvestorSec(id);
    }

    public SbnInvestorSecDTO getInvestorSecById(Long id) throws BsmSoaException {
        return this.sbnInvestorSecService.getInvestorSecById(id);
    }

    public List<SbnInvestorSecDTO> getInvestorSecSelection(InvestorSecParam investorSecParam) throws BsmSoaException {
        String sid = (investorSecParam.getSid()!=null)?investorSecParam.getSid():BLANK;
        return this.sbnInvestorSecService.getInvestorSecBySid(sid);
    }

    /* Pemesanan */
    public SbnOrderingDTO postOrder(SbnOrderingDTO sbnOrderingDTO) throws BsmSoaException {
        return this.sbnOrderService.postOrder(sbnOrderingDTO);
    }

    public SbnOrderingDTO getOrderByIdSeriesAndTrnId(OrderParam orderParam) throws BsmSoaException {
        return this.sbnOrderService.getOrderByIdSeriesAndTrnId(orderParam.getSeries(), orderParam.getTrnid());
    }

    public List<SbnOrderingDTO> getOrderRedeemBySid(OrderParam orderParam) throws BsmSoaException {
        return this.sbnOrderService.getOrderRedeemBySid(orderParam.getSid());
    }

    public List<SbnOrderingDTO> getOrderSeriesBySeriesAndSid(OrderParam orderParam) throws BsmSoaException {
        return this.sbnOrderService.getOrderSeriesBySeriesAndSid(orderParam.getSeries(), orderParam.getSid());
    }

    public SbnQueryOrderDTO getOrderSeriesBySeries(OrderParam orderParam) throws BsmSoaException {
        return this.sbnOrderService.getOrderSeriesBySeries(orderParam.getSeries()
                , orderParam.getSearch(), orderParam.getOrderdate(), orderParam.getQtrncode()
                , orderParam.getPageNumber());
    }

    public List<SbnOrderingDTO> getOrderSeriesAllBySid(OrderParam orderParam) throws BsmSoaException {
        return this.sbnOrderService.getOrderSeriesAllBySid(orderParam.getSid());
    }

    public SbnOrderingDTO getOrderByOrderCode(OrderParam orderParam) throws BsmSoaException {
        return this.sbnOrderService.getOrderByOrderCode(orderParam.getOrdercode());
    }

    public SbnOrderingDTO getOrderBilling(OrderParam orderParam) throws BsmSoaException {
        return this.sbnOrderService.getOrderBilling(orderParam.getBillcode());
    }

    public SbnRedeemDTO postRedeem(SbnRedeemDTO sbnRedeemDTO) throws BsmSoaException {
        return this.sbnRedeemService.postRedeem(sbnRedeemDTO);
    }

    public List<SbnRedeemDTO> geRedeemBySid(RedeemParam redeemParam) throws BsmSoaException {
        return this.sbnRedeemService.geRedeemBySid(redeemParam.getSid());
    }

    public List<SbnRedeemDTO> geRedeemBySeriesAndSid(RedeemParam redeemParam) throws BsmSoaException {
        return this.sbnRedeemService.geRedeemBySeriesAndSid(redeemParam.getSeries(), redeemParam.getSid());
    }

    public SbnRedeemDTO geRedeemByRedeemCode(RedeemParam redeemParam) throws BsmSoaException {
        return this.sbnRedeemService.geRedeemByRedeemCode(redeemParam.getRedeemCode());
    }

    public SbnQueryRedeemDTO geRedeemBySeriesAndDate(RedeemParam redeemParam) throws BsmSoaException {
        return this.sbnRedeemService.geRedeemBySeriesAndDate(redeemParam.getSeries()
                , redeemParam.getSearch(), redeemParam.getRedeemDate(), null);
    }
}
