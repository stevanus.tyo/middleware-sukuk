package id.co.bsm.service.param;

import javax.ws.rs.PathParam;

public class InvestorInetParam {
    @PathParam("CifNo")
    private String cif;

    @PathParam("AcctNo")
    private String acctNo;

    @PathParam("PhoneNo")
    private String phoneNo;

    @PathParam("MobileNo")
    private String mobileNo;

    @PathParam("Email")
    private String email;

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getAcctNo() {
        return acctNo;
    }

    public void setAcctNo(String acctNo) {
        this.acctNo = acctNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
