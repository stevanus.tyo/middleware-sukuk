package id.co.bsm.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnOrderingDTO {
    @JsonProperty("TrxId")
    private String trnId;

    @JsonProperty("NamaInvestor")
    private String name;

    @JsonProperty("Seri")
    private String series;

    @JsonProperty("KodePemesanan")
    private String bookingCode;

    @JsonProperty("TingkatKupon")
    private BigDecimal couponLeven;

    @JsonProperty("TglBayarKupon")
    private String couponPayDate;

    @JsonProperty("TglSetelmen")
    private String settelmentDate;

    @JsonProperty("TglJatuhTempo")
    private String maturityDate;

    @JsonProperty("KodeBilling")
    private String billingCode;

    @JsonProperty("NominalKupon")
    private BigDecimal couponAmount;

    @JsonProperty("NominalKuponPerUnit")
    private BigDecimal couponAmountUnit;

    @JsonProperty("NominalPerUnit")
    private BigDecimal unitAmount;

    @JsonProperty("NominalKuponPertama")
    private BigDecimal firstCouponAmount;

    @JsonProperty("Redeem")
    private BigDecimal redeeml;

    @JsonProperty("MinimalRedeem")
    private BigDecimal minRedeem;

    @JsonProperty("MaksimalRedeem")
    private BigDecimal maxRedeem;

    @JsonProperty("Redeemable")
    private BigDecimal redeemable;

    @JsonProperty("KelipatanRedeem")
    private BigDecimal multipleRedeem;

    @JsonProperty("SisaKepemilikan")
    private BigDecimal ownerOver;

    @JsonProperty("IdStatus")
    private String statusId;

    @JsonProperty("Status")
    private String status;

    @JsonProperty("BatasWaktuBayar")
    private String billDeadline;

    @JsonProperty("NTPN")
    private String ntpn;

    @JsonProperty("TglPemesanan")
    private String orderDate;

    @JsonProperty("CreatedBy")
    private String createBy;

    @JsonProperty("Sid")
    private String sid;

    @JsonProperty("IdSeri")
    private Long seriesId;

    @JsonProperty("Nominal")
    private BigDecimal amount;

    @JsonProperty("IdRekDana")
    private Long idRekeningDana;

    @JsonProperty("RekeningDana")
    private SbnInvestorBankDTO rekeningDana;

    @JsonProperty("IdRekSb")
    private Long idRekeningSb;

    @JsonProperty("RekeningSB")
    private SbnInvestorSecDTO rekeningSb;

    public SbnOrderingDTO() { }

    public String getTrnId() {
        return trnId;
    }

    public void setTrnId(String trnId) {
        this.trnId = trnId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public BigDecimal getCouponLeven() {
        return couponLeven;
    }

    public void setCouponLeven(BigDecimal couponLeven) {
        this.couponLeven = couponLeven;
    }

    public String getCouponPayDate() {
        return couponPayDate;
    }

    public void setCouponPayDate(String couponPayDate) {
        this.couponPayDate = couponPayDate;
    }

    public String getSettelmentDate() {
        return settelmentDate;
    }

    public void setSettelmentDate(String settelmentDate) {
        this.settelmentDate = settelmentDate;
    }

    public String getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(String maturityDate) {
        this.maturityDate = maturityDate;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public BigDecimal getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(BigDecimal couponAmount) {
        this.couponAmount = couponAmount;
    }

    public BigDecimal getCouponAmountUnit() {
        return couponAmountUnit;
    }

    public void setCouponAmountUnit(BigDecimal couponAmountUnit) {
        this.couponAmountUnit = couponAmountUnit;
    }

    public BigDecimal getUnitAmount() {
        return unitAmount;
    }

    public void setUnitAmount(BigDecimal unitAmount) {
        this.unitAmount = unitAmount;
    }

    public BigDecimal getFirstCouponAmount() {
        return firstCouponAmount;
    }

    public void setFirstCouponAmount(BigDecimal firstCouponAmount) {
        this.firstCouponAmount = firstCouponAmount;
    }

    public BigDecimal getRedeeml() {
        return redeeml;
    }

    public void setRedeeml(BigDecimal redeeml) {
        this.redeeml = redeeml;
    }

    public BigDecimal getMinRedeem() {
        return minRedeem;
    }

    public void setMinRedeem(BigDecimal minRedeem) {
        this.minRedeem = minRedeem;
    }

    public BigDecimal getMaxRedeem() {
        return maxRedeem;
    }

    public void setMaxRedeem(BigDecimal maxRedeem) {
        this.maxRedeem = maxRedeem;
    }

    public BigDecimal getRedeemable() {
        return redeemable;
    }

    public void setRedeemable(BigDecimal redeemable) {
        this.redeemable = redeemable;
    }

    public BigDecimal getMultipleRedeem() {
        return multipleRedeem;
    }

    public void setMultipleRedeem(BigDecimal multipleRedeem) {
        this.multipleRedeem = multipleRedeem;
    }

    public BigDecimal getOwnerOver() {
        return ownerOver;
    }

    public void setOwnerOver(BigDecimal ownerOver) {
        this.ownerOver = ownerOver;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBillDeadline() {
        return billDeadline;
    }

    public void setBillDeadline(String billDeadline) {
        this.billDeadline = billDeadline;
    }

    public String getNtpn() {
        return ntpn;
    }

    public void setNtpn(String ntpn) {
        this.ntpn = ntpn;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public Long getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(Long seriesId) {
        this.seriesId = seriesId;
    }

    //@JsonSerialize(using = AmountUtilSerializer.class)
    public BigDecimal getAmount() {
        return amount;
    }

    // @JsonSerialize(using = AmountUtilSerializer.class)
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getIdRekeningDana() {
        return idRekeningDana;
    }

    public void setIdRekeningDana(Long idRekeningDana) {
        this.idRekeningDana = idRekeningDana;
    }

    public SbnInvestorBankDTO getRekeningDana() {
        return rekeningDana;
    }

    public void setRekeningDana(SbnInvestorBankDTO rekeningDana) {
        this.rekeningDana = rekeningDana;
    }

    public Long getIdRekeningSb() {
        return idRekeningSb;
    }

    public void setIdRekeningSb(Long idRekeningSb) {
        this.idRekeningSb = idRekeningSb;
    }

    public SbnInvestorSecDTO getRekeningSb() {
        return rekeningSb;
    }

    public void setRekeningSb(SbnInvestorSecDTO rekeningSb) {
        this.rekeningSb = rekeningSb;
    }
}
