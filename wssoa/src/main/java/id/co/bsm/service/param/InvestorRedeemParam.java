package id.co.bsm.service.param;

import javax.ws.rs.PathParam;
import java.math.BigDecimal;

public class InvestorRedeemParam {
    @PathParam("CifNo")
    private String cif;

    @PathParam("BookingCode")
    private String bookingCode;

    @PathParam("Amount")
    private BigDecimal amount;

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
