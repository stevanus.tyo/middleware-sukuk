package id.co.bsm.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnInvestorSecDTO {
    @JsonProperty("Id")
    private Long id;

    @JsonProperty("Sid")
    private String sid;

    @JsonProperty("NoRek")
    private String acctNo;

    @JsonProperty("Nama")
    private String acctName;

    @JsonProperty("IdSubregistry")
    private Long subRegId;

    @JsonProperty("NamaSubregistry")
    private String subRegName;

    @JsonProperty("IdPartisipan")
    private Long participantId;

    @JsonProperty("NamaPartisipan")
    private String participantName;

    @JsonProperty("CreatedAt")
    private String createAt;

    @JsonProperty("CreatedBy")
    private String createBy;

    @JsonProperty("ModifiedAt")
    private String modifiedAt;

    @JsonProperty("ModifiedBy")
    private String modifiedBy;

    public SbnInvestorSecDTO() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getAcctNo() {
        return acctNo;
    }

    public void setAcctNo(String acctNo) {
        this.acctNo = acctNo;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public Long getSubRegId() {
        return subRegId;
    }

    public void setSubRegId(Long subRegId) {
        this.subRegId = subRegId;
    }

    public String getSubRegName() {
        return subRegName;
    }

    public void setSubRegName(String subRegName) {
        this.subRegName = subRegName;
    }

    public Long getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Long participantId) {
        this.participantId = participantId;
    }

    public String getParticipantName() {
        return participantName;
    }

    public void setParticipantName(String participantName) {
        this.participantName = participantName;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
