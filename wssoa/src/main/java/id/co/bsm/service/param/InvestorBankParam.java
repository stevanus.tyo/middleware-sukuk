package id.co.bsm.service.param;

import javax.ws.rs.QueryParam;

public class InvestorBankParam {
    @QueryParam("sid")
    private String sid;

    public InvestorBankParam() { }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
