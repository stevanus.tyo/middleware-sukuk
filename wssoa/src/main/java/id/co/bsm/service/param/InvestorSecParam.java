package id.co.bsm.service.param;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

public class InvestorSecParam {
    @PathParam("id")
    private Long id;

    @QueryParam("sid")
    private String sid;

    public InvestorSecParam() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
