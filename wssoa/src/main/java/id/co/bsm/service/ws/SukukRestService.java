package id.co.bsm.service.ws;

import id.co.bsm.service.dto.*;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.service.param.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/sbn/api/")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public interface SukukRestService {
    @GET
    @Path("/occupation/")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnOccupationDTO> getOccupationRest()  throws BsmSoaException;

    @GET
    @Path("/sex/")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnSexDTO> getSexRest()  throws BsmSoaException;

    @GET
    @Path("/province/")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnProvinceDTO> getProvinceRest()  throws BsmSoaException;

    @GET
    @Path("/province/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnProvinceDTO getCityRest(@PathParam(value = "id") Long id)  throws BsmSoaException;

    @GET
    @Path("/bank/")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnBankDTO> getBankRest()  throws BsmSoaException;

    @GET
    @Path("/bankperception/")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnBankPerceptionDTO> getBankPerceptionRest()  throws BsmSoaException;

    @GET
    @Path("/channel/")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnChannelDTO> getChannelRest()  throws BsmSoaException;

    @GET
    @Path("/subregistry/")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnBankDTO> getSubRegistryRest()  throws BsmSoaException;

    @GET
    @Path("/participant/")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnParticipantDTO> getParticipantRest() throws BsmSoaException;

    @GET
    @Path("/trncode/")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnTrnCodeDTO> getTrnCodeRest() throws BsmSoaException;

    @GET
    @Path("/quota/{series}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnQuotaDTO getQuotaAllRest(@BeanParam QuotaParam quotaParam) throws BsmSoaException;

    @GET
    @Path("/quota/{series}/{sid}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnQuotaDTO getQuotaSidRest(@BeanParam QuotaParam quotaParam) throws BsmSoaException;

    /*Series*/
    @GET
    @Path("/series/")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnSeriesDTO> getSeriesRest() throws BsmSoaException;

    @GET
    @Path("/series/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnSeriesDTO getSeriesByIdRest(@PathParam(value = "id") Long id) throws BsmSoaException;

    @GET
    @Path("/series/offer")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnSeriesDTO> getSeriesOfferRest() throws BsmSoaException;

    @GET
    @Path("/series/redeem")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnSeriesDTO> getSeriesRedeemRest() throws BsmSoaException;

    @GET
    @Path("/series/redeem/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnSeriesDTO getSeriesRedeemByIdRest(@PathParam(value = "id") Long id) throws BsmSoaException;

    @GET
    @Path("/series/redeem/offer")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnSeriesDTO> getSeriesRedeemOfferRest() throws BsmSoaException;

    /* Investor */

    @GET
    @Path("/investor")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorDTO getInvestorSelectionRest(@BeanParam InvestorParam investorParam) throws BsmSoaException;

    @GET
    @Path("/investor/{sid}/")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorDTO getInvestorBySidRest(@PathParam(value = "sid") String sid) throws BsmSoaException;

    @POST
    @Path("/investor/")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorDTO postInvestorRest(SbnInvestorDTO sbnInvestorDTO) throws BsmSoaException;

    @PUT
    @Path("/investor/{sid}/")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorDTO putInvestorEditRest(SbnInvestorDTO sbnInvestorDTO) throws BsmSoaException;

    @PUT
    @Path("/investor/{sid}/activate")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorDTO putInvestorActivateRest(@PathParam(value = "sid") String sid) throws BsmSoaException;

    @PUT
    @Path("/investor/{sid}/deactivate")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorDTO putInvestorDeactivateRest(@PathParam(value = "sid") String sid) throws BsmSoaException;

    /*Rekening Sumber Dana*/

    @GET
    @Path("/investorbank")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorBankDTO getInvestorBankSelectionRest(@BeanParam InvestorBankParam investorBankParam) throws BsmSoaException;

    @GET
    @Path("/investorbank/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorBankDTO getInvestorBankByIdRest(@PathParam(value = "id") Long id) throws BsmSoaException;

    @POST
    @Path("/investorbank/")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorBankDTO postInvestorBankRest(SbnInvestorBankDTO sbnInvestorBankDTO) throws BsmSoaException;

    @PUT
    @Path("/investorbank/{id}/")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorBankDTO putInvestorBankEditRest(SbnInvestorBankDTO sbnInvestorBankDTO) throws BsmSoaException;

    @DELETE
    @Path("/investorbank/{id}/")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnMessageDTO deleteInvestorBankRest(@PathParam(value = "id") Long id) throws BsmSoaException;

    /*Rekening Surat Berharga*/
    @GET
    @Path("/investorsec")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorSecDTO getInvestorSecSelectionRest(@BeanParam InvestorSecParam investorSecParam) throws BsmSoaException;

    @GET
    @Path("/investorsec/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorSecDTO getInvestorSecByIdRest(@PathParam(value = "id") Long id) throws BsmSoaException;

    @POST
    @Path("/investorsec/")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorSecDTO postInvestorSecRest(SbnInvestorSecDTO sbnInvestorSecDTO) throws BsmSoaException;

    @PUT
    @Path("/investorsec/{id}/")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnInvestorSecDTO putInvestorSecEditRest(SbnInvestorSecDTO sbnInvestorSecDTO) throws BsmSoaException;

    @DELETE
    @Path("/investorsec/{id}/")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnMessageDTO deleteInvestorSecRest(@PathParam(value = "id") Long id) throws BsmSoaException;

    /* Pemesanan */
    @POST
    @Path("/order/")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnOrderingDTO postOrderRest(SbnOrderingDTO sbnOrderingDTO) throws BsmSoaException;

    @GET
    @Path("/order/series/{series}/trx/{trnid}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnOrderingDTO getOrderByIdSeriesAndTrnIdRest(@BeanParam OrderParam orderParam) throws BsmSoaException;


    @GET
    @Path("/order/redeemable/{sid}")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnOrderingDTO> getOrderRedeemBySidRest(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @GET
    @Path("/order/history/series/{series}/{sid}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnOrderingDTO getOrderHistoryBySeriesAndSidRest(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @GET
    @Path("/order/series/{series}/{sid}")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnOrderingDTO> getOrderSeriesBySeriesAndSidRest(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @GET
    @Path("/order/series/{series}/{sid}/{trncode}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnOrderingDTO getOrderSeriesBySeriesAndSidAndTrnCodeRest(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @GET
    @Path("/order/series/all/{sid}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnOrderingDTO getOrderSeriesAllBySidRest(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @GET
    @Path("/order/series/offer/{sid}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnOrderingDTO getOrderSeriesOfferBySidRest(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @GET
    @Path("/order/{ordercode}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnOrderingDTO getOrderByOrderCodeRest(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @GET
    @Path("/order/series/{series}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnQueryOrderDTO getOrderSeriesBySeriesRest(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @GET
    @Path("/order/series/{series}/range")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnOrderingDTO getOrderSeriesBySeriesRangeRest(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @GET
    @Path("/order/seri/paymentstatus")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnOrderingDTO getOrderByPaymentStatusRest(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @GET
    @Path("/order/seri/all")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnOrderingDTO getOrderSeriesAllRest(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @GET
    @Path("/order/seri/all/Range")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnOrderingDTO getOrderSeriesAllRangeRest(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @GET
    @Path("/order/billing/{billcode}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnOrderingDTO getOrderBillingRest(@BeanParam OrderParam orderParam) throws BsmSoaException;


    /* Redeem */
    @POST
    @Path("/redeem/")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnRedeemDTO postRedeemRest(SbnRedeemDTO sbnOrderingDTO) throws BsmSoaException;

    @GET
    @Path("/redeem/series/all/{sid}")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnRedeemDTO> geRedeemBySidRest(@BeanParam RedeemParam redeemParam) throws BsmSoaException;

    @GET
    @Path("/redeem/series/{series}/{sid}")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<SbnRedeemDTO> geRedeemBySeriesAndSidRest(@BeanParam RedeemParam redeemParam) throws BsmSoaException;

    @GET
    @Path("/redeem/{redeemcode}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnRedeemDTO geRedeemByRedeemCodeRest(@BeanParam RedeemParam redeemParam) throws BsmSoaException;

    @GET
    @Path("/redeem/series/{series}")
    @Consumes({MediaType.APPLICATION_JSON})
    public SbnQueryRedeemDTO geRedeemBySeriesAndDateRest(@BeanParam RedeemParam redeemParam) throws BsmSoaException;
}
