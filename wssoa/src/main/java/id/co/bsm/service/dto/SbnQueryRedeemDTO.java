package id.co.bsm.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnQueryRedeemDTO {
    @JsonProperty("PageNumber")
    private Long pageNum;

    @JsonProperty("PageSize")
    private Long pageSize;

    @JsonProperty("TotalPage")
    private Long totalPage;

    @JsonProperty("TotalRecord")
    private Long totalRecord;

    @JsonProperty("Records")
    private List<SbnRedeemDTO> sbnRedeemDTOS;

    public SbnQueryRedeemDTO() { }

    public Long getPageNum() {
        return pageNum;
    }

    public void setPageNum(Long pageNum) {
        this.pageNum = pageNum;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    public Long getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(Long totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<SbnRedeemDTO> getSbnRedeemDTOS() {
        return sbnRedeemDTOS;
    }

    public void setSbnRedeemDTOS(List<SbnRedeemDTO> sbnRedeemDTOS) {
        this.sbnRedeemDTOS = sbnRedeemDTOS;
    }
}
