package id.co.bsm.service;

import com.fasterxml.jackson.core.type.TypeReference;
import id.co.bsm.service.dto.SbnProvinceDTO;
import id.co.bsm.config.SbnPropertyService;
import id.co.bsm.service.error.BsmSoaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Component
public class SbnProvinceService {
    @Value("${sbn.map.api.province}")
    private String PATH_URL;

    @Autowired
    private SbnPropertyService propertyService = SbnPropertyService.getInstance();

    private ResponseService responseUtil;

    public SbnProvinceService() {
        responseUtil = new ResponseService(propertyService);
    }

    public List<SbnProvinceDTO> getProvince() throws BsmSoaException {
        List<SbnProvinceDTO> provinceDTOS = new ArrayList<SbnProvinceDTO>();
        Response res = propertyService.getSbnRequestHandler().get(PATH_URL, "", "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnProvinceDTO>>(){});
    }

    public SbnProvinceDTO getCity(String id) throws BsmSoaException {
        String urlPath = PATH_URL + "/" + id;
        Response res = propertyService.getSbnRequestHandler().get(urlPath, "", "");
        return this.responseUtil.getResponseDTO(res, 200, SbnProvinceDTO.class);
    }
}
