package id.co.bsm.service;

import com.fasterxml.jackson.core.type.TypeReference;
import id.co.bsm.service.dto.SbnQueryRedeemDTO;
import id.co.bsm.service.dto.SbnRedeemDTO;
import id.co.bsm.config.SbnPropertyService;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.util.DateUtil;
import id.co.bsm.util.QueryAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.List;

@Component
public class SbnRedeemService {
    @Value("${sbn.map.api.redeem}")
    private String PATH_URL;

    private static String V1_1 = "v1.1";

    @Autowired
    private SbnPropertyService propertyService = SbnPropertyService.getInstance();

    private ResponseService responseUtil;

    public SbnRedeemService() {
        responseUtil = new ResponseService(propertyService);
    }

    public SbnRedeemDTO postRedeem(SbnRedeemDTO sbnRedeemDTO) throws BsmSoaException {
        Response res = propertyService.getSbnRequestHandler().post(PATH_URL, sbnRedeemDTO);
        return this.responseUtil.getResponseDTO(res, 201, SbnRedeemDTO.class);
    }

    public List<SbnRedeemDTO> geRedeemBySid(String sid) throws BsmSoaException {
        String path = PATH_URL + "/seri/all/" + sid;
        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnRedeemDTO>>(){});
    }

    public List<SbnRedeemDTO> geRedeemBySeriesAndSid(Long series, String sid) throws BsmSoaException {
        String path = PATH_URL + "/seri/" + series + "/" + sid;
        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnRedeemDTO>>(){});
    }

    public SbnRedeemDTO geRedeemByRedeemCode(String redeemCode) throws BsmSoaException {
        String path = PATH_URL + "/" + redeemCode;
        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, SbnRedeemDTO.class);
    }

    public SbnQueryRedeemDTO geRedeemBySeriesAndDate(Long series, String search
            , String redeemDate, String pageNum) throws BsmSoaException {
        String path = PATH_URL + "/seri/" +  series;

        QueryAppend qparam = new QueryAppend();
        qparam.add("Search", search);
        qparam.add("TglRedeem", DateUtil.getInstance().parse(redeemDate));
        qparam.add("PageNumber", ((pageNum!=null)?pageNum:1));
        qparam.add("PageSize", "100");
        qparam.add("Sort", "SID");

        Response res = propertyService.getSbnRequestHandler().get(path, qparam.getRes(), "");
        return this.responseUtil.getResponseDTO(res, 200, SbnQueryRedeemDTO.class);
    }
}
