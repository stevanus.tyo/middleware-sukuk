package id.co.bsm.service;

import com.fasterxml.jackson.core.type.TypeReference;
import id.co.bsm.service.dto.SbnErrorResponseDTO;
import id.co.bsm.config.SbnPropertyService;
import id.co.bsm.service.error.BsmSoaException;

import javax.ws.rs.core.Response;

public class ResponseService {
    private SbnPropertyService propertyService;

    public ResponseService(SbnPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    public  <T> T getResponseDTO(Response res, long code, TypeReference oclass) throws BsmSoaException {
        if(propertyService.getErrorResponseDTO() != null)
            propertyService.getErrorResponseDTO().setErrors(null);

        try {
            if (res.getStatus() != code) {
                propertyService.setErrorResponseDTO(propertyService.getMapper()
                        .readValue(res.readEntity(String.class), SbnErrorResponseDTO.class));

                throw new BsmSoaException(propertyService.getErrorResponseDTO());
            }

            return propertyService.getMapper().readValue(res.readEntity(String.class), oclass);
        }catch (Exception e) {
            if(propertyService.getErrorResponseDTO() == null) {
                propertyService.getErrorResponseDTO().setCode(500);
                propertyService.getErrorResponseDTO().setMessage(e.getMessage());
            }

            throw new BsmSoaException(propertyService.getErrorResponseDTO());
        }
    }

    public  <T> T getResponseDTO(Response res, long code, Class<T> oclass) throws BsmSoaException {
        if(propertyService.getErrorResponseDTO() != null)
            propertyService.getErrorResponseDTO().setErrors(null);

        try {
            if (res.getStatus() != code) {
                propertyService.setErrorResponseDTO(propertyService.getMapper()
                        .readValue(res.readEntity(String.class), SbnErrorResponseDTO.class));

                throw new BsmSoaException(propertyService.getErrorResponseDTO());
            }

            return propertyService.getMapper().readValue(res.readEntity(String.class), oclass);
        }catch (Exception e) {
            if(propertyService.getErrorResponseDTO() == null) {
                propertyService.getErrorResponseDTO().setCode(500);
                propertyService.getErrorResponseDTO().setMessage(e.getMessage());
            }

            throw new BsmSoaException(propertyService.getErrorResponseDTO());
        }
    }
}
