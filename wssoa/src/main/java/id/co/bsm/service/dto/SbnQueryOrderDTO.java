package id.co.bsm.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnQueryOrderDTO {
    @JsonProperty("PageNumber")
    private Long pageNum;

    @JsonProperty("PageSize")
    private Long pageSize;

    @JsonProperty("TotalPage")
    private Long totalPage;

    @JsonProperty("TotalRecord")
    private Long totalRecord;

    @JsonProperty("Records")
    private List<SbnOrderingDTO> sbnOrderingDTO;

    public SbnQueryOrderDTO() { }

    public Long getPageNum() {
        return pageNum;
    }

    public void setPageNum(Long pageNum) {
        this.pageNum = pageNum;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    public Long getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(Long totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<SbnOrderingDTO> getSbnOrderingDTO() {
        return sbnOrderingDTO;
    }

    public void setSbnOrderingDTO(List<SbnOrderingDTO> sbnOrderingDTO) {
        this.sbnOrderingDTO = sbnOrderingDTO;
    }
}
