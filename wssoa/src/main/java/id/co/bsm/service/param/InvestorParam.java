package id.co.bsm.service.param;

import javax.ws.rs.QueryParam;

public class InvestorParam {
    @QueryParam("name")
    private String name;

    @QueryParam("ktpno")
    private String ktpno;

    @QueryParam("birthdate")
    private String birthdate;

    public InvestorParam() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKtpno() {
        return ktpno;
    }

    public void setKtpno(String ktpno) {
        this.ktpno = ktpno;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
}
