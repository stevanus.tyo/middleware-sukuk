package id.co.bsm.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnQuotaDTO {
    @JsonProperty("KuotaInvestor")
    private Number investorQuota;

    @JsonProperty("IdSeri")
    private Long seriesId;

    @JsonProperty("Seri")
    private String seriesName;

    @JsonProperty("TglSetelmen")
    private String settelmentDate;

    @JsonProperty("TglJatuhTempo")
    private String maturityDate;

    @JsonProperty("TglMulaiPemesanan")
    private String startDate;

    @JsonProperty("TglAkhirPemesanan")
    private String endDate;

    @JsonProperty("KuotaSeri")
    private Number seriesQuota;

    @JsonProperty("Tradability")
    private String tradability;

    public SbnQuotaDTO() { }

    public Number getInvestorQuota() {
        return investorQuota;
    }

    public void setInvestorQuota(Number investorQuota) {
        this.investorQuota = investorQuota;
    }

    public Long getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(Long seriesId) {
        this.seriesId = seriesId;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public String getSettelmentDate() {
        return settelmentDate;
    }

    public void setSettelmentDate(String settelmentDate) {
        this.settelmentDate = settelmentDate;
    }

    public String getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(String maturityDate) {
        this.maturityDate = maturityDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Number getSeriesQuota() {
        return seriesQuota;
    }

    public void setSeriesQuota(Number seriesQuota) {
        this.seriesQuota = seriesQuota;
    }

    public String getTradability() {
        return tradability;
    }

    public void setTradability(String tradability) {
        this.tradability = tradability;
    }
}
