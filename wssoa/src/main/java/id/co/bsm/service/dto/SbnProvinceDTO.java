package id.co.bsm.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"Id", "Nama", "Kota"})
public class SbnProvinceDTO {
    @JsonProperty("Id")
    private String id;

    @JsonProperty("Nama")
    private String name;

    @JsonProperty("Kota")
    private List<SbnCityDTO> city;

    public SbnProvinceDTO() { }

    public SbnProvinceDTO(String id, String name, List<SbnCityDTO> city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SbnCityDTO> getCity() {
        return city;
    }

    public void setCity(List<SbnCityDTO> city) {
        this.city = city;
    }
}
