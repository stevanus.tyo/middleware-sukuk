package id.co.bsm.service;

import id.co.bsm.service.dto.SbnQuotaDTO;
import id.co.bsm.config.SbnPropertyService;
import id.co.bsm.service.error.BsmSoaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;

@Component
public class SbnQuotaService {
    @Value("${sbn.map.api.quota}")
    private String PATH_URL;

    @Autowired
    private SbnPropertyService propertyService = SbnPropertyService.getInstance();

    private ResponseService responseUtil;

    public SbnQuotaService() {
        responseUtil = new ResponseService(propertyService);
    }

    public SbnQuotaDTO getQuotaAll(Long series) throws BsmSoaException {
        String fullurl = PATH_URL + "/" + series;
        Response res = propertyService.getSbnRequestHandler().get(fullurl, "", "");
        return this.responseUtil.getResponseDTO(res, 200, SbnQuotaDTO.class);
    }

    public SbnQuotaDTO getQuotaSid(Long series, String sid) throws BsmSoaException {
        String fullurl = PATH_URL + "/" + series + "/" + sid;

        Response res = propertyService.getSbnRequestHandler().get(fullurl, "", "");
        return this.responseUtil.getResponseDTO(res, 200, SbnQuotaDTO.class);
    }
}
