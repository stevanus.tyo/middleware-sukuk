package id.co.bsm.service.param;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

public class OrderParam {
    @PathParam("series")
    private String series;

    @PathParam("trnid")
    private String trnid;

    @PathParam("sid")
    private String sid;

    @PathParam("trncode")
    private String trncode;

    @PathParam("ordercode")
    private String ordercode;

    @PathParam("billcode")
    private String billcode;

    @QueryParam("Search")
    private String search;

    @QueryParam("OrderDate")
    private String orderdate;

    @QueryParam("StartDate")
    private String startDate;

    @QueryParam("EndDate")
    private String endDate;

    @QueryParam("TrnCode")
    private String qtrncode;

    @QueryParam("PageNumber")
    private String pageNumber;

    @QueryParam("PageSize")
    private String pageSize;

    @QueryParam("Sort")
    private String sort;


    public OrderParam() { }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getTrnid() {
        return trnid;
    }

    public void setTrnid(String trnid) {
        this.trnid = trnid;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getTrncode() {
        return trncode;
    }

    public void setTrncode(String trncode) {
        this.trncode = trncode;
    }

    public String getOrdercode() {
        return ordercode;
    }

    public void setOrdercode(String ordercode) {
        this.ordercode = ordercode;
    }

    public String getBillcode() {
        return billcode;
    }

    public void setBillcode(String billcode) {
        this.billcode = billcode;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(String orderdate) {
        this.orderdate = orderdate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getQtrncode() {
        return qtrncode;
    }

    public void setQtrncode(String qtrncode) {
        this.qtrncode = qtrncode;
    }
}
