package id.co.bsm.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnRequestDTO {
    private String sbnUrl;
    private String sbnApiId;
    private String sbnApiKey;
    private String sbnApiRef;

    public SbnRequestDTO() { }

    public SbnRequestDTO(String sbnUrl, String sbnApiId, String sbnApiKey, String sbnApiRef) {
        this.sbnUrl = sbnUrl;
        this.sbnApiId = sbnApiId;
        this.sbnApiKey = sbnApiKey;
        this.sbnApiRef = sbnApiRef;
    }

    public String getSbnUrl() {
        return sbnUrl;
    }

    public void setSbnUrl(String sbnUrl) {
        this.sbnUrl = sbnUrl;
    }

    public String getSbnApiId() {
        return sbnApiId;
    }

    public void setSbnApiId(String sbnApiId) {
        this.sbnApiId = sbnApiId;
    }

    public String getSbnApiKey() {
        return sbnApiKey;
    }

    public void setSbnApiKey(String sbnApiKey) {
        this.sbnApiKey = sbnApiKey;
    }

    public String getSbnApiRef() {
        return sbnApiRef;
    }

    public void setSbnApiRef(String sbnApiRef) {
        this.sbnApiRef = sbnApiRef;
    }
}
