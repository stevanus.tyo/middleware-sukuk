package id.co.bsm.service.param;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

public class RedeemParam {
    @PathParam("sid")
    private String sid;

    @PathParam("series")
    private Long series;

    @PathParam("redeemcode")
    private String redeemCode;

    @QueryParam("Search")
    private String search;

    @QueryParam("redeemdate")
    private String redeemDate;

    public RedeemParam() { }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public Long getSeries() {
        return series;
    }

    public void setSeries(Long series) {
        this.series = series;
    }

    public String getRedeemCode() {
        return redeemCode;
    }

    public void setRedeemCode(String redeemCode) {
        this.redeemCode = redeemCode;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getRedeemDate() {
        return redeemDate;
    }

    public void setRedeemDate(String redeemDate) {
        this.redeemDate = redeemDate;
    }
}
