package id.co.bsm.service.param;

import javax.ws.rs.PathParam;

public class HistoryInetParam {
    @PathParam("CifNo")
    private String cif;

    @PathParam("Series")
    private String series;

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }
}
