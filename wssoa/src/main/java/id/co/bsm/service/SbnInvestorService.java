package id.co.bsm.service;

import id.co.bsm.service.dto.SbnInvestorDTO;
import id.co.bsm.service.dto.SbnInvestorSecDTO;
import id.co.bsm.config.SbnPropertyService;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.util.DataDTOUtil;
import id.co.bsm.util.DateUtil;
import id.co.bsm.util.QueryAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.text.SimpleDateFormat;

@Component
public class SbnInvestorService implements Serializable {

    @Value("${sbn.map.api.investor}")
    private String PATH_URL;

    @Value("${sbn.map.api.investorsec}")
    private String PATH_ACCT_SCT;

    SimpleDateFormat fullDate = new SimpleDateFormat("yyyyMMdd");

    @Autowired
    private SbnPropertyService propertyService = SbnPropertyService.getInstance();

    private ResponseService responseUtil;

    public SbnInvestorService() {
        responseUtil = new ResponseService(propertyService);
    }

    public SbnInvestorDTO getInvestorBySid(String sid) throws BsmSoaException {
        String path = PATH_URL + "/" + sid;

        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, SbnInvestorDTO.class);
    }

    public SbnInvestorDTO getInvestorSelection(String name, String noidentitas, String tgllahir) throws BsmSoaException {
        QueryAppend qparam = new QueryAppend();
        qparam.add("nama", name);
        qparam.add("noidentitas", noidentitas);
        qparam.add("tgllahir", DateUtil.getInstance().parse(tgllahir));

        Response res = propertyService.getSbnRequestHandler().get(PATH_URL, qparam.getRes(), "");
        return this.responseUtil.getResponseDTO(res, 200, SbnInvestorDTO.class);
    }

    public SbnInvestorDTO postInvestor(SbnInvestorDTO sbnInvestorDTO) throws BsmSoaException {
        if (sbnInvestorDTO.getBirthDate() != null)
            sbnInvestorDTO.setBirthDate((new DateUtil()).parse(sbnInvestorDTO.getBirthDate()));

        Response res = propertyService.getSbnRequestHandler().post(PATH_URL, sbnInvestorDTO);
        return this.responseUtil.getResponseDTO(res, 201, SbnInvestorDTO.class);
    }

    public SbnInvestorDTO putInvestorEdit(SbnInvestorDTO sbnInvestorDTO) throws BsmSoaException {
        SbnInvestorDTO sbninvOld = this.getInvestorBySid(sbnInvestorDTO.getSid());

        if(sbnInvestorDTO.getBirthDate() != null)
            sbnInvestorDTO.setBirthDate(DateUtil.getInstance().parse(sbnInvestorDTO.getBirthDate()));


        String path = PATH_URL + "/" + sbnInvestorDTO.getSid();
        Response res = propertyService.getSbnRequestHandler().put(path, (SbnInvestorDTO) DataDTOUtil
                .getInstance().update(sbninvOld, sbnInvestorDTO));
        return this.responseUtil.getResponseDTO(res, 200, SbnInvestorDTO.class);
    }

    public SbnInvestorDTO putInvestorAction(String sid, String action) throws BsmSoaException {
        String path = PATH_URL + "/" + sid + "/" + action;
        Response res = propertyService.getSbnRequestHandler().put(path, "");
        return this.responseUtil.getResponseDTO(res, 200, SbnInvestorDTO.class);
    }

    public SbnInvestorSecDTO postInvestorSec(SbnInvestorSecDTO sbnInvestorSecDTO) throws BsmSoaException {
        Response res = propertyService.getSbnRequestHandler().post(PATH_ACCT_SCT, sbnInvestorSecDTO);
        return this.responseUtil.getResponseDTO(res, 201, SbnInvestorSecDTO.class);
    }
}
