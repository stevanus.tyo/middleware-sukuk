package id.co.bsm.service.ws;

import id.co.bsm.domain.*;
import id.co.bsm.enumeration.ErrorMessage;
import id.co.bsm.service.*;
import id.co.bsm.service.dto.*;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.service.param.*;
import id.co.bsm.util.DateUtil;
import id.co.bsm.enumeration.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SbnInetServiceImpl implements Serializable {
    private static final long serialVersionUID = 1L;

    private final Logger log = LoggerFactory.getLogger(SbnInetServiceImpl.class);

    @Autowired
    private InvestorDbService investorDbService;

    @Autowired
    private SbnInvestorService sbnInvestorService;

    @Autowired
    private SbnInvestorBankService sbnInvestorBankService;

    @Autowired
    private SbnInvestorSecService sbnInvestorSecService;

    @Autowired
    private SbnQuotaService sbnQuotaService;

    @Autowired
    private SbnOrderService sbnOrderService;

    @Autowired
    private SbnRedeemService sbnRedeemService;


    public List<Branch> getAllBranch() throws Exception {
        return investorDbService.getAllBranch();
    }

    public String getInvestorStatus(String cif) throws BsmSoaException {
        return investorDbService.findInvestorStatus(cif);
    }

    public InvestorProfileDTO getInvestorProfile(String cif) throws BsmSoaException {
        return investorDbService.findInvestorProfile(cif);
    }

    public SbnQuotaDTO getQuota(QuotaParam quotaParam) throws BsmSoaException {
        String sid = quotaParam.getSid();
        if(sid==null)
            sid = investorDbService.findInvestorSidByCif(quotaParam.getCif());

        if(sid==null) {
            throw new BsmSoaException(new SbnErrorResponseDTO(500, ErrorMessage.NO_INVESTOR, null));
        }

        return this.sbnQuotaService.getQuotaSid(quotaParam.getSeries(), sid);
    }

    public void postInvestorNew(InvestorInetParam investorRegisterParam)
            throws BsmSoaException {

        SbnErrorResponseDTO sbnErrorResponseDTO = new SbnErrorResponseDTO();
        sbnErrorResponseDTO.setCode(500);
        sbnErrorResponseDTO.setMessage("Not Implemented Yet");
        throw new BsmSoaException(sbnErrorResponseDTO);
    }

    /* save all data to domain db */
    @Transactional
    public String startInvestorRegister(InvestorInetParam investorInetParam)
            throws BsmSoaException {

        /* get data from repository */
        Investor investor = investorDbService.findInvestorByCifNum(investorInetParam.getCif());

        if(investor == null) {
            throw new BsmSoaException(new SbnErrorResponseDTO(500, ErrorMessage.NO_INVESTOR, null));
        }

        /* Add investor Sbn */
        InvestorSbn investorSbn =  investor.getInvestorSbn();
        if(investorSbn == null)
            investorSbn = new InvestorSbn();

        if(investorSbn.getStatus().equalsIgnoreCase(Constants.SbnRegister.FINISH.name())) {
            SbnErrorResponseDTO sbnErrorResponseDTO = new SbnErrorResponseDTO();
            sbnErrorResponseDTO.setCode(500);
            sbnErrorResponseDTO.setMessage(ErrorMessage.REG_COMPLETE);
            throw new BsmSoaException(sbnErrorResponseDTO);
        }

        investorSbn.setName(investor.getCustName());
        investorSbn.setKtpNo(investor.getInvestorProfile().getInvestorKTPNumber());
        investorSbn.setBirthPlace(investor.getInvestorProfile().getInvestorBirthPlace());
        investorSbn.setBirthDate(investor.getInvestorProfile().getInvestorBirthDate());
        /* Map to data parameter */
        investorSbn.setSex(investor.getInvestorProfile().getInvestorSex());
        investorSbn.setOccupation(investor.getInvestorProfile().getInvestorOccupation());
        investorSbn.setCity(investor.getInvestorProfile().getInvestorCity());
        /* end map */
        investorSbn.setAddress(investor.getInvestorProfile().getInvestorAddress1());
        investorSbn.setHomePhone(investor.getInvestorProfile().getInvestorHomePhone());
        investorSbn.setMobilePhone(investorInetParam.getMobileNo());
        investorSbn.setEmail(investorInetParam.getEmail());

        if(investorSbn.getStatus()==null)
            investorSbn.setStatus(Constants.SbnRegister.INVESTOR.name());

        investorSbn.setInvestor(investor);
        investorDbService.saveInvestorSbn(investorSbn);

        /* Add investor Sbn sumber dana */
        InvestorSbnBank investorSbnBank = investorDbService
                .findInvestorSbnBankByAcctNo(investorInetParam.getAcctNo());
        if(investorSbnBank==null) {
            investorSbnBank = new InvestorSbnBank();
            investorSbnBank.setAccountNo(investorInetParam.getAcctNo());
            investorSbnBank.setAccountName(investor.getCustName());
            investorSbnBank.setBankId(Constants.SBN_BANK_CODE);

            investorSbnBank.setInvestors(investor);
            investorDbService.saveInvestorSbnBank(investorSbnBank);
        }

        return investor.getSidNum();
    }

    public String postSbnInvestorRegister(String sid)
            throws BsmSoaException {
        InvestorSbn investorSbn = investorDbService.findInvestorBySidNum(sid).getInvestorSbn();

        /* Send profile investor to sbn onlie api */
        SbnInvestorDTO sbnInvestorDTO =  new SbnInvestorDTO();
        sbnInvestorDTO.setSid(sid);
        sbnInvestorDTO.setName(investorSbn.getName());
        sbnInvestorDTO.setKtpNo(investorSbn.getKtpNo());
        sbnInvestorDTO.setBirthPlace(investorSbn.getBirthPlace());
        sbnInvestorDTO.setBirthDate(DateUtil.getInstance().parse(investorSbn.getBirthDate()));

        sbnInvestorDTO.setSexCode(investorDbService.findUserCodeMapBySys(Constants.SBN_SEX, investorSbn.getSex()));
        sbnInvestorDTO.setOccupationCode(investorDbService.findUserCodeMapBySys(Constants.SBN_OCCUPATION, investorSbn.getOccupation()));
        sbnInvestorDTO.setCityCode(investorDbService.findUserCodeMapBySys(Constants.SBN_CITY,investorSbn.getCity()));

        sbnInvestorDTO.setAddress(investorSbn.getAddress());
        sbnInvestorDTO.setHomePhone(investorSbn.getHomePhone());
        sbnInvestorDTO.setMobilePhone(investorSbn.getMobilePhone());
        sbnInvestorDTO.setEmail(investorSbn.getEmail());

        if(investorSbn.getStatus().equalsIgnoreCase(Constants.SbnRegister.INVESTOR.name())) {
            try {
                this.sbnInvestorService.postInvestor(sbnInvestorDTO);
            } catch (BsmSoaException  e) {
                if(e.getErrorDetail().getCode()!=101)
                    throw new BsmSoaException(e);
            }

            this.updateStatus(Constants.SbnRegister.SUMBER_DANA.name(), sid);
        }

        return sid;
    }

    public String postSbnInvestorBankRegister(String sid) throws BsmSoaException {
        /* get data from repository */
        Investor investor = investorDbService.findInvestorBySidNum(sid);
        InvestorSbnBank investorSbnBank = investor.getSbndana().get(0);

        SbnInvestorBankDTO sbnInvestorBankDTO = new SbnInvestorBankDTO();
        sbnInvestorBankDTO.setSid(sid);
        sbnInvestorBankDTO.setAcctNo(investorSbnBank.getAccountNo());
        sbnInvestorBankDTO.setAcctName(investorSbnBank.getAccountName());
        sbnInvestorBankDTO.setIdBank(investorSbnBank.getBankId());

        if(investor.getInvestorSbn().getStatus().equalsIgnoreCase(Constants.SbnRegister.SUMBER_DANA.name())) {
            sbnInvestorBankDTO = this.sbnInvestorBankService.postInvestorBank(sbnInvestorBankDTO);
            investorSbnBank.setSbnId(sbnInvestorBankDTO.getId());
            investorDbService.saveInvestorSbnBank(investorSbnBank);
            this.updateStatus(Constants.SbnRegister.SURAT_BERHARGA.name(), sid);
        }

        return sid;
    }

    public String postSbnInvestorSecRegister(String sid) throws BsmSoaException {
        /* get data from repository */
        Investor investor = investorDbService.findInvestorBySidNum(sid);

        if(investor.getExchange()==null)
            throw new BsmSoaException(new SbnErrorResponseDTO(500, ErrorMessage.NO_EXCHANGE_INVESTOR, null));

        InvestorExchange investorExchange = investor.getExchange().get(0);

        SbnInvestorSecDTO sbnInvestorSecDTO = new SbnInvestorSecDTO();
        sbnInvestorSecDTO.setSid(sid);
        sbnInvestorSecDTO.setAcctNo(investorExchange.getSreNum());
        sbnInvestorSecDTO.setAcctName(investorExchange.getSreName());
        sbnInvestorSecDTO.setParticipantId(investorExchange.getParticipant());
        sbnInvestorSecDTO.setSubRegId(investorExchange.getSubRegistry());

        if(investor.getInvestorSbn().getStatus().equalsIgnoreCase(Constants.SbnRegister.SURAT_BERHARGA.name())) {
            sbnInvestorSecDTO = this.sbnInvestorSecService.postInvestorSec(sbnInvestorSecDTO);
            investorExchange.setSbnId(sbnInvestorSecDTO.getId());
            investorDbService.saveInvestorExchange(investorExchange);
            this.updateStatus(Constants.SbnRegister.ACTIVATE.name(), sid);
        }

        return sid;
    }

    public String postSbnActivate(String sid) throws BsmSoaException {
        Investor investor = investorDbService.findInvestorBySidNum(sid);

        if(investor.getInvestorSbn().getStatus().equalsIgnoreCase(Constants.SbnRegister.SURAT_BERHARGA.name())) {
            this.sbnInvestorService.putInvestorAction(sid, Constants.SBN_ACTIVATE);
            this.updateStatus(Constants.SbnRegister.COMPLETE.name(), sid);
        }

        return sid;
    }

    @Transactional
    public InvestorRegisterDTO endInvestorRegister(String sid) throws BsmSoaException {
        Investor investor = investorDbService.findInvestorBySidNum(sid);
        investor.setInvestorStatus(Constants.InvestorStatus.ACTIVE.name());

        InvestorSbn investorSbn = investor.getInvestorSbn();
        investorSbn.setStatus(Constants.SbnRegister.FINISH.name());
        investor.setInvestorSbn(investorSbn);
        investorDbService.saveInvestor(investor);

        return investorDbService.investorRegisterComplate(sid);
    }

    @Transactional
    public String updateStatus(String _update) throws BsmSoaException {
        String[] update = _update.split("-");
        InvestorSbn investorSbn = investorDbService.findInvestorSbnByInvestorSidNum(update[1]);
        investorSbn.setStatus(update[0]);
        investorDbService.saveInvestorSbn(investorSbn);

        return update[1];
    }

    @Transactional
    public void updateStatus(String State, String sid) throws BsmSoaException {
        InvestorSbn investorSbn = investorDbService.findInvestorSbnByInvestorSidNum(sid);
        investorSbn.setStatus(State);
        investorDbService.saveInvestorSbn(investorSbn);
    }

    public SbnOrderingDTO postOrder(InvestorOrderParam investorOrderParam) throws BsmSoaException {
        Investor investor = getInvestor(investorOrderParam.getCif());

        /* Check Condition */
        if(investor.getSidNum()==null)
            throw new BsmSoaException(new SbnErrorResponseDTO(500, ErrorMessage.NO_SID_INVESTOR, null));

        if(investorOrderParam.getSeries()==null)
            throw new BsmSoaException(new SbnErrorResponseDTO(500, ErrorMessage.ORDER_SERIES_MANDATORY, null));

        /* Send Order to SBN API */
        SbnOrderingDTO sbnOrderingDTO = new SbnOrderingDTO();
        sbnOrderingDTO.setSid(investor.getSidNum());
        sbnOrderingDTO.setSeriesId(Long.valueOf(investorOrderParam.getSeries()));
        sbnOrderingDTO.setAmount(investorOrderParam.getAmount());
        sbnOrderingDTO.setIdRekeningDana(investor.getSbndana().get(0).getSbnId());
        sbnOrderingDTO.setIdRekeningSb(investor.getExchange().get(0).getSbnId());
        sbnOrderingDTO = sbnOrderService.postOrder(sbnOrderingDTO);

        /* Insert new Order */
        InvestorOrder investorOrder = new InvestorOrder();
        investorOrder.setBookingCode(sbnOrderingDTO.getBookingCode());
        investorOrder.setBillingCode(sbnOrderingDTO.getBillingCode());
        investorOrder.setNtpn(sbnOrderingDTO.getNtpn());
        investorOrder.setSid(sbnOrderingDTO.getSid());
        investorOrder.setName(sbnOrderingDTO.getName());
        investorOrder.setSeriesId(sbnOrderingDTO.getSeriesId());
        investorOrder.setSeries(sbnOrderingDTO.getSeries());
        investorOrder.setSettelmentDate(DateUtil.getInstance().getFull(sbnOrderingDTO.getSettelmentDate()));
        investorOrder.setMaturityDate(DateUtil.getInstance().getFull(sbnOrderingDTO.getMaturityDate()));
        investorOrder.setOrderDate(DateUtil.getInstance().getFull(sbnOrderingDTO.getOrderDate()));
        investorOrder.setAmount(sbnOrderingDTO.getAmount());
        investorOrder.setInvestors(investor);
        investorOrder.setBranch(investorOrderParam.getBranch());
        investorDbService.saveOrder(investorOrder);

        return sbnOrderingDTO;
    }

    public List<SbnOrderingDTO> getHitoryFromApi(String cif) throws BsmSoaException {
        Investor investor = getInvestor(cif);

        return this.sbnOrderService.getOrderSeriesAllBySid(investor.getSidNum());
    }

    public List<SbnOrderingDTO> getHitoryBySeriesFromApi(HistoryInetParam historyInetParam)
            throws BsmSoaException {
        Investor investor = getInvestor(historyInetParam.getCif());

        return this.sbnOrderService.getOrderSeriesBySeriesAndSid(historyInetParam.getSeries()
                , investor.getSidNum());
    }

    public List<InvestorOrderDTO> getHistoryFromDb(String cif) throws BsmSoaException {
        Investor investor = getInvestor(cif);

        List<InvestorOrder> investorOrder = investor.getOrders();
        if(investorOrder==null)
            throw new BsmSoaException(new SbnErrorResponseDTO(500, ErrorMessage.NO_ORDER, null));

        return investorOrder.stream().map(InvestorOrderDTO::new).collect(Collectors.toList());
    }

    public List<SbnOrderingDTO> getOrderRedeemable(String cif) throws BsmSoaException {
        Investor investor = getInvestor(cif);
        return this.sbnOrderService.getOrderRedeemBySid(investor.getSidNum());
    }

    public SbnRedeemDTO readeem(InvestorRedeemParam investorRedeemParam) throws BsmSoaException {
        Investor investor = getInvestor(investorRedeemParam.getCif());
        SbnRedeemDTO sbnRedeemDTO = new SbnRedeemDTO();
        sbnRedeemDTO.setSid(investor.getSidNum());
        sbnRedeemDTO.setOrderCode(investorRedeemParam.getBookingCode());
        sbnRedeemDTO.setAmount(investorRedeemParam.getAmount());

        return this.sbnRedeemService.postRedeem(sbnRedeemDTO);
    }

    public List<SbnRedeemDTO> getRedeemByCif(String cif) throws BsmSoaException {
        Investor investor = getInvestor(cif);

        return this.sbnRedeemService.geRedeemBySid(investor.getSidNum());
    }

    public List<SbnRedeemDTO> getRedeemByCifAndSeries(HistoryInetParam inetParam) throws BsmSoaException {
        Investor investor = getInvestor(inetParam.getCif());

        return this.sbnRedeemService.geRedeemBySeriesAndSid(Long.valueOf(inetParam.getSeries()), investor.getSidNum());
    }

    private Investor getInvestor(String cif) throws BsmSoaException {
        Investor investor = investorDbService.findActiveInvestorByCif(cif);
        if(investor==null)
            throw new BsmSoaException(new SbnErrorResponseDTO(500, ErrorMessage.NO_ACTIVE_INVESTOR, null));

        return investor;
    }
}
