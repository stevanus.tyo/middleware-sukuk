package id.co.bsm.service;

import com.fasterxml.jackson.core.type.TypeReference;
import id.co.bsm.service.dto.SbnBankDTO;
import id.co.bsm.config.SbnPropertyService;
import id.co.bsm.service.error.BsmSoaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.List;

@Component
public class SbnBankService {
    @Value("${sbn.map.api.bank}")
    private String PATH_URL;

    @Autowired
    private SbnPropertyService propertyService = SbnPropertyService.getInstance();

    private ResponseService responseUtil;

    public SbnBankService() {
        responseUtil = new ResponseService(propertyService);
    }

    public List<SbnBankDTO> getBank() throws BsmSoaException {
        Response res = propertyService.getSbnRequestHandler().get(PATH_URL, "", "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnBankDTO>>(){});
    }
}
