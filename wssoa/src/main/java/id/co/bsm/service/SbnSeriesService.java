package id.co.bsm.service;

import com.fasterxml.jackson.core.type.TypeReference;
import id.co.bsm.service.dto.SbnSeriesDTO;
import id.co.bsm.config.SbnPropertyService;
import id.co.bsm.service.error.BsmSoaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Component
public class SbnSeriesService {
    @Value("${sbn.map.api.series}")
    private String PATH_URL;

    @Autowired
    private SbnPropertyService propertyService = SbnPropertyService.getInstance();

    private ResponseService responseUtil;

    public SbnSeriesService() {
        responseUtil = new ResponseService(propertyService);
    }

    public List<SbnSeriesDTO> getSeries() throws BsmSoaException {
        List<SbnSeriesDTO> seriesDTOS = new ArrayList<SbnSeriesDTO>();
        Response res = propertyService.getSbnRequestHandler().get(PATH_URL, "", "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnSeriesDTO>>(){});
    }

    public SbnSeriesDTO getSeriesById(Long id) throws BsmSoaException {
        String path = PATH_URL + "/" + id;

        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, SbnSeriesDTO.class);
    }

    public List<SbnSeriesDTO> getSeriesTypes(String type) throws BsmSoaException {
        String path = PATH_URL + "/" + type;

        List<SbnSeriesDTO> seriesDTOS = new ArrayList<SbnSeriesDTO>();
        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnSeriesDTO>>(){});
    }

    public SbnSeriesDTO getSeriesType(String type, Long id) throws BsmSoaException {
        String path = PATH_URL + "/" + type + "/" + id;

        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, SbnSeriesDTO.class);
    }
}
