package id.co.bsm.service.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnInvestorDTO {
    @JsonProperty("Sid")
    private String sid;

    @JsonProperty("Nama")
    private String name;

    @JsonProperty("KdJenisKelamin")
    private String sexCode;

    @JsonProperty("JenisKelamin")
    private String sex;

    @JsonProperty("KdPekerjaan")
    private String occupationCode;

    @JsonProperty("Pekerjaan")
    private String occupation;

    @JsonProperty("KdKota")
    private String cityCode;

    @JsonProperty("Kota")
    private String city;

    @JsonProperty("Provinsi")
    private String province;

    @JsonProperty("Status")
    private String status;

    @JsonProperty("NoIdentitas")
    private String ktpNo;

    @JsonProperty("TempatLahir")
    private String birthPlace;

    @JsonProperty("TglLahir")
    private String birthDate;

    @JsonProperty("Alamat")
    private String address;

    @JsonProperty("NoTelp")
    private String homePhone;

    @JsonProperty("NoHp")
    private String mobilePhone;

    @JsonProperty("Email")
    private String email;

    @JsonProperty("CreatedAt")
    private String createAt;

    @JsonProperty("CreatedBy")
    private String createBy;

    @JsonProperty("ModifiedAt")
    private String modifiedAt;

    @JsonProperty("ModifiedBy")
    private String modifiedBy;

    @JsonProperty("RekeningDana")
    private List<SbnInvestorBankDTO> rekeningDana;

    @JsonProperty("RekeningSB")
    private List<SbnInvestorSecDTO> rekeningSb;

    public SbnInvestorDTO() { }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSexCode() {
        return sexCode;
    }

    public void setSexCode(String sexCode) {
        this.sexCode = sexCode;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getOccupationCode() {
        return occupationCode;
    }

    public void setOccupationCode(String occupationCode) {
        this.occupationCode = occupationCode;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKtpNo() {
        return ktpNo;
    }

    public void setKtpNo(String ktpNo) {
        this.ktpNo = ktpNo;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public List<SbnInvestorBankDTO> getRekeningDana() {
        return rekeningDana;
    }

    public void setRekeningDana(List<SbnInvestorBankDTO> rekeningDana) {
        this.rekeningDana = rekeningDana;
    }

    public List<SbnInvestorSecDTO> getRekeningSb() {
        return rekeningSb;
    }

    public void setRekeningSb(List<SbnInvestorSecDTO> rekeningSb) {
        this.rekeningSb = rekeningSb;
    }
}
