package id.co.bsm.service;


import com.fasterxml.jackson.core.type.TypeReference;
import id.co.bsm.service.dto.SbnInvestorBankDTO;
import id.co.bsm.service.dto.SbnMessageDTO;
import id.co.bsm.config.SbnPropertyService;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.util.DataDTOUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.List;

@Component
public class SbnInvestorBankService {
    @Value("${sbn.map.api.investorbank}")
    private String PATH_URL;

    @Autowired
    private SbnPropertyService propertyService = SbnPropertyService.getInstance();

    private ResponseService responseUtil;

    public SbnInvestorBankService() {
        responseUtil = new ResponseService(propertyService);
    }

    public SbnInvestorBankDTO getInvestorBankById(Long id) throws BsmSoaException {
        String path = PATH_URL + "/" + id;
        Response res = propertyService.getSbnRequestHandler().get(path, "", "");
        return this.responseUtil.getResponseDTO(res, 200, SbnInvestorBankDTO.class);
    }

    public List<SbnInvestorBankDTO> getInvestorBankBySid(String sid) throws BsmSoaException {
        String qparam = "sid=" + sid;
        Response res = propertyService.getSbnRequestHandler().get(PATH_URL, qparam, "");
        return this.responseUtil.getResponseDTO(res, 200, new TypeReference<List<SbnInvestorBankDTO>>(){});
    }

    public SbnInvestorBankDTO postInvestorBank(SbnInvestorBankDTO sbnInvestorBankDTO) throws BsmSoaException {
        Response res = propertyService.getSbnRequestHandler().post(PATH_URL, sbnInvestorBankDTO);
        return this.responseUtil.getResponseDTO(res, 201, SbnInvestorBankDTO.class);
    }

    public SbnInvestorBankDTO putInvestorBankEdit(SbnInvestorBankDTO sbnInvestorBankDTO) throws BsmSoaException {
        SbnInvestorBankDTO sbninvOld = this.getInvestorBankById(sbnInvestorBankDTO.getId());

        String path = PATH_URL + "/" + sbnInvestorBankDTO.getId();

        Response res = propertyService.getSbnRequestHandler().put(path, (SbnInvestorBankDTO) DataDTOUtil
                .getInstance().update(sbninvOld, sbnInvestorBankDTO));

        return this.responseUtil.getResponseDTO(res, 200, SbnInvestorBankDTO.class);
    }

    public SbnMessageDTO deleteInvestorBank(String id) throws BsmSoaException {
        String path = PATH_URL + "/" + id;
        Response res = propertyService.getSbnRequestHandler().delete(path, "");
        return this.responseUtil.getResponseDTO(res, 200, SbnMessageDTO.class);
    }

}
