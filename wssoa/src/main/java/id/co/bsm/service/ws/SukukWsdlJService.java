package id.co.bsm.service.ws;

import id.co.bsm.service.dto.*;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.service.param.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.BeanParam;

@WebService(serviceName = "wsdljapi", targetNamespace = "http://bsm.co.id/ws/sbn/wsdlj/api")
public interface SukukWsdlJService {
    @WebMethod
    public String getOccupationWsdl()  throws BsmSoaException;

    @WebMethod
    public String getSexWsdl()  throws BsmSoaException;

    @WebMethod
    public String getProvinceWsdl()  throws BsmSoaException;

    @WebMethod
    public String getCityWsdl(@WebParam(name = "id") Long id)  throws BsmSoaException;

    @WebMethod
    public String getBankWsdl()  throws BsmSoaException;

    @WebMethod
    public String getBankPerceptionWsdl()  throws BsmSoaException;

    @WebMethod
    public String getChannelWsdl()  throws BsmSoaException;

    @WebMethod
    public String getSubRegistryWsdl()  throws BsmSoaException;

    @WebMethod
    public String getParticipantWsdl() throws BsmSoaException;

    @WebMethod
    public String getTrnCodeWsdl() throws BsmSoaException;

    @WebMethod
    public String getQuotaAllWsdl(@BeanParam QuotaParam quotaParam) throws BsmSoaException;

    @WebMethod
    public String getQuotaSidWsdl(@BeanParam QuotaParam quotaParam) throws BsmSoaException;

    /*Series*/
    @WebMethod
    public String getSeriesWsdl() throws BsmSoaException;

    @WebMethod
    public String getSeriesByIdWsdl(@WebParam(name = "id") Long id) throws BsmSoaException;

    @WebMethod
    public String getSeriesOfferWsdl() throws BsmSoaException;

    @WebMethod
    public String getSeriesRedeemWsdl() throws BsmSoaException;

    @WebMethod
    public String getSeriesRedeemByIdWsdl(@WebParam(name = "id") Long id) throws BsmSoaException;

    @WebMethod
    public String getSeriesRedeemOfferWsdl() throws BsmSoaException;

    /* Investor */

    @WebMethod
    public String getInvestorSelectionWsdl(@BeanParam InvestorParam investorParam) throws BsmSoaException;

    @WebMethod
    public String getInvestorBySidWsdl(@WebParam(name = "sid") String sid) throws BsmSoaException;

    @WebMethod
    public String postInvestorWsdl(SbnInvestorDTO sbnInvestorDTO) throws BsmSoaException;

    @WebMethod
    public String putInvestorEditWsdl(SbnInvestorDTO sbnInvestorDTO) throws BsmSoaException;

    @WebMethod
    public String putInvestorActivateWsdl(@WebParam(name = "sid") String sid) throws BsmSoaException;

    @WebMethod
    public String putInvestorDeactivateWsdl(@WebParam(name = "sid") String sid) throws BsmSoaException;

    /*Rekening Sumber Dana*/
    @WebMethod
    public String getInvestorBankSelectionWsdl(@BeanParam InvestorBankParam investorBankParam) throws BsmSoaException;

    @WebMethod
    public String getInvestorBankByIdWsdl(@WebParam(name = "id") Long id) throws BsmSoaException;

    @WebMethod
    public String postInvestorBankWsdl(SbnInvestorBankDTO sbnInvestorBankDTO) throws BsmSoaException;

    @WebMethod
    public String putInvestorBankEditWsdl(SbnInvestorBankDTO sbnInvestorBankDTO) throws BsmSoaException;

    @WebMethod
    public String deleteInvestorBankWsdl(@WebParam(name = "id") Long id) throws BsmSoaException;

    /*Rekening Surat Berharga*/
    @WebMethod
    public String getInvestorSecSelectionWsdl(@BeanParam InvestorSecParam investorSecParam) throws BsmSoaException;

    @WebMethod
    public String getInvestorSecByIdWsdl(@WebParam(name = "id") Long id) throws BsmSoaException;

    @WebMethod
    public String postInvestorSecWsdl(SbnInvestorSecDTO sbnInvestorSecDTO) throws BsmSoaException;

    @WebMethod
    public String putInvestorSecEditWsdl(SbnInvestorSecDTO sbnInvestorSecDTO) throws BsmSoaException;

    @WebMethod
    public String deleteInvestorSecWsdl(@WebParam(name = "id") Long id) throws BsmSoaException;

    /* Pemesanan */
    @WebMethod
    public String postOrderWsdl(SbnOrderingDTO sbnOrderingDTO) throws BsmSoaException;

    @WebMethod
    public String getOrderByIdSeriesAndTrnIdWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;


    @WebMethod
    public String getOrderRedeemBySidWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public String getOrderHistoryBySeriesAndSidWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public String getOrderSeriesBySeriesAndSidWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public String getOrderSeriesBySeriesAndSidAndTrnCodeWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public String getOrderSeriesAllBySidWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public String getOrderSeriesOfferBySidWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public String getOrderByOrderCodeWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public String getOrderSeriesBySeriesWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public String getOrderSeriesBySeriesRangeWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public String getOrderByPaymentStatusWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public String getOrderSeriesAllWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public String getOrderSeriesAllRangeWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;

    @WebMethod
    public String getOrderBillingWsdl(@BeanParam OrderParam orderParam) throws BsmSoaException;


    /* Redeem */
    @WebMethod
    public String postRedeemWsdl(SbnRedeemDTO sbnOrderingDTO) throws BsmSoaException;

    @WebMethod
    public String geRedeemBySidWsdl(@BeanParam RedeemParam redeemParam) throws BsmSoaException;

    @WebMethod
    public String geRedeemBySeriesAndSidWsdl(@BeanParam RedeemParam redeemParam) throws BsmSoaException;

    @WebMethod
    public String geRedeemByRedeemCodeWsdl(@BeanParam RedeemParam redeemParam) throws BsmSoaException;

    @WebMethod
    public String geRedeemBySeriesAndDateWsdl(@BeanParam RedeemParam redeemParam) throws BsmSoaException;
}
