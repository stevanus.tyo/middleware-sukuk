package id.co.bsm.service.param;

import javax.ws.rs.PathParam;
import java.math.BigDecimal;

public class InvestorOrderParam {
    @PathParam("CifNo")
    private String cif;

    @PathParam("Series")
    private Long series;

    @PathParam("Amount")
    private BigDecimal amount;

    @PathParam("Branch")
    private String branch;

    public InvestorOrderParam () { }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public Long getSeries() {
        return series;
    }

    public void setSeries(Long series) {
        this.series = series;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}
