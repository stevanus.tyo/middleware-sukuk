package id.co.bsm.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SbnSeriesDTO {
    @JsonProperty("Id")
    private Long id;

    @JsonProperty("Seri")
    private String series;

    @JsonProperty("TingkatKupon")
    private Number couponLeven;

    @JsonProperty("BatasBawahKupon")
    private Number minCouponLimit;

    @JsonProperty("BatasAtasKupon")
    private Number maxCouponLimit;

    @JsonProperty("TingkatKuponAwal")
    private Number startCouponLevel;

    @JsonProperty("Spread")
    private Number spread;

    @JsonProperty("JenisKupon")
    private String couponType;

    @JsonProperty("TglBayarKupon")
    private String couponPayDate;

    @JsonProperty("TglSetelmen")
    private String settelmentDate;

    @JsonProperty("TglMulaiRedeem")
    private String redeemStartDate;

    @JsonProperty("TglAkhirRedeem")
    private String redeemEndDate;

    @JsonProperty("MinRedeem")
    private Number minRedeem;

    @JsonProperty("KelipatanRedeem")
    private Number multipleRedeem;

    @JsonProperty("MaxPcent")
    private Number maxPcent;

    @JsonProperty("MinKepemilikan")
    private Number minOwner;

    @JsonProperty("TglJatuhTempo")
    private String maturityDate;

    @JsonProperty("TglMulaiPemesanan")
    private String startDate;

    @JsonProperty("TglAkhirPemesanan")
    private String endDate;

    @JsonProperty("MinPemesanan")
    private Number minOrder;

    @JsonProperty("MaxPemesanan")
    private Number maxOrder;

    @JsonProperty("KelipatanPemesanan")
    private Number multipleOrder;

    @JsonProperty("Target")
    private Number target;

    @JsonProperty("FrekuensiKupon")
    private Number couponFreq;

    @JsonProperty("Tradability")
    private String tradability;

    @JsonProperty("LinkMemo")
    private String linkMemo;

    public SbnSeriesDTO() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public Number getCouponLeven() {
        return couponLeven;
    }

    public void setCouponLeven(Number couponLeven) {
        this.couponLeven = couponLeven;
    }

    public Number getMinCouponLimit() {
        return minCouponLimit;
    }

    public void setMinCouponLimit(Number minCouponLimit) {
        this.minCouponLimit = minCouponLimit;
    }

    public Number getMaxCouponLimit() {
        return maxCouponLimit;
    }

    public void setMaxCouponLimit(Number maxCouponLimit) {
        this.maxCouponLimit = maxCouponLimit;
    }

    public Number getStartCouponLevel() {
        return startCouponLevel;
    }

    public void setStartCouponLevel(Number startCouponLevel) {
        this.startCouponLevel = startCouponLevel;
    }

    public Number getSpread() {
        return spread;
    }

    public void setSpread(Number spread) {
        this.spread = spread;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public String getCouponPayDate() {
        return couponPayDate;
    }

    public void setCouponPayDate(String couponPayDate) {
        this.couponPayDate = couponPayDate;
    }

    public String getSettelmentDate() {
        return settelmentDate;
    }

    public void setSettelmentDate(String settelmentDate) {
        this.settelmentDate = settelmentDate;
    }

    public String getRedeemStartDate() {
        return redeemStartDate;
    }

    public void setRedeemStartDate(String redeemStartDate) {
        this.redeemStartDate = redeemStartDate;
    }

    public String getRedeemEndDate() {
        return redeemEndDate;
    }

    public void setRedeemEndDate(String redeemEndDate) {
        this.redeemEndDate = redeemEndDate;
    }

    public Number getMinRedeem() {
        return minRedeem;
    }

    public void setMinRedeem(Number minRedeem) {
        this.minRedeem = minRedeem;
    }

    public Number getMultipleRedeem() {
        return multipleRedeem;
    }

    public void setMultipleRedeem(Number multipleRedeem) {
        this.multipleRedeem = multipleRedeem;
    }

    public Number getMaxPcent() {
        return maxPcent;
    }

    public void setMaxPcent(Number maxPcent) {
        this.maxPcent = maxPcent;
    }

    public Number getMinOwner() {
        return minOwner;
    }

    public void setMinOwner(Number minOwner) {
        this.minOwner = minOwner;
    }

    public String getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(String maturityDate) {
        this.maturityDate = maturityDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Number getMinOrder() {
        return minOrder;
    }

    public void setMinOrder(Number minOrder) {
        this.minOrder = minOrder;
    }

    public Number getMaxOrder() {
        return maxOrder;
    }

    public void setMaxOrder(Number maxOrder) {
        this.maxOrder = maxOrder;
    }

    public Number getMultipleOrder() {
        return multipleOrder;
    }

    public void setMultipleOrder(Number multipleOrder) {
        this.multipleOrder = multipleOrder;
    }

    public Number getTarget() {
        return target;
    }

    public void setTarget(Number target) {
        this.target = target;
    }

    public Number getCouponFreq() {
        return couponFreq;
    }

    public void setCouponFreq(Number couponFreq) {
        this.couponFreq = couponFreq;
    }

    public String getTradability() {
        return tradability;
    }

    public void setTradability(String tradability) {
        this.tradability = tradability;
    }

    public String getLinkMemo() {
        return linkMemo;
    }

    public void setLinkMemo(String linkMemo) {
        this.linkMemo = linkMemo;
    }
}
