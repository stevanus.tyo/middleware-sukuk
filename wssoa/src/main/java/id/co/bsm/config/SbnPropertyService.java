package id.co.bsm.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.bsm.client.sbn.SbnRequestHandler;
import id.co.bsm.service.dto.SbnErrorResponseDTO;
import id.co.bsm.service.dto.SbnRequestDTO;
import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;

public class SbnPropertyService implements Serializable {
    private static final long serialVersionUID = 1L;

    @Value("${sbn.server.api.url}")
    private String sbnUrl;

    @Value( "${sbn.server.api.base}")
    private String sbnApiRef;

    @Value( "${sbn.server.api.id}")
    private String sbnApiId;

    @Value( "${sbn.server.api.key}")
    private String sbnApikey;

    private ObjectMapper mapper = new ObjectMapper();

    private SbnRequestHandler sbnRequestHandler = new SbnRequestHandler();

    private SbnErrorResponseDTO errorResponseDTO = null;

    private static SbnPropertyService instance = new SbnPropertyService();

    public SbnPropertyService() { }

    private void init() {
        this.sbnRequestHandler.setSbnRequestDTO((new SbnRequestDTO(sbnUrl, sbnApiId, sbnApikey, sbnApiRef)));
        this.mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        this.mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        this.mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    public String getSbnUrl() {
        return sbnUrl;
    }

    public void setSbnUrl(String sbnUrl) {
        this.sbnUrl = sbnUrl;
    }

    public String getSbnApiRef() {
        return sbnApiRef;
    }

    public void setSbnApiRef(String sbnApiRef) {
        this.sbnApiRef = sbnApiRef;
    }

    public String getSbnApiId() {
        return sbnApiId;
    }

    public void setSbnApiId(String sbnApiId) {
        this.sbnApiId = sbnApiId;
    }

    public String getSbnApikey() {
        return sbnApikey;
    }

    public void setSbnApikey(String sbnApikey) {
        this.sbnApikey = sbnApikey;
    }

    public ObjectMapper getMapper() {
        return mapper;
    }

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public SbnRequestHandler getSbnRequestHandler() {
        return sbnRequestHandler;
    }

    public void setSbnRequestHandler(SbnRequestHandler sbnRequestHandler) {
        this.sbnRequestHandler = sbnRequestHandler;
    }

    public SbnErrorResponseDTO getErrorResponseDTO() {
        return errorResponseDTO;
    }

    public void setErrorResponseDTO(SbnErrorResponseDTO errorResponseDTO) {
        this.errorResponseDTO = errorResponseDTO;
    }

    public static SbnPropertyService getInstance() {
        return instance;
    }

    public static void setInstance(SbnPropertyService instance) {
        SbnPropertyService.instance = instance;
    }
}
