package id.co.bsm.route;

import id.co.bsm.service.ws.SukukRestService;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.service.error.ErrorProcessor;
import id.co.bsm.service.param.*;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class SbnRestRoute extends RouteBuilder {
    private String resturi = "cxfrs:bean:restServer?resourceClasses=" + SukukRestService.class.getName();

    @Override
    public void configure() throws Exception {
        onException(BsmSoaException.class).handled(true).process(new ErrorProcessor()).end();

        from(resturi).to("log:input")
                .recipientList(simple("direct-vm:${header.operationName}"));

        from("direct-vm:getOccupationRest").to("log:input")
                .bean("sbnService", "getOccupation");

        from("direct-vm:getSexRest").to("log:input")
                .bean("sbnService", "getSex");

        from("direct-vm:getProvinceRest").to("log:input")
                .bean("sbnService", "getProvince");

        from("direct-vm:getCityRest").to("log:input")
                .bean("sbnService", "getCity");

        from("direct-vm:getBankRest").to("log:input")
                .bean("sbnService", "getBank");

        from("direct-vm:getBankPerceptionRest").to("log:input")
                .bean("sbnService", "getBankPerception");

        from("direct-vm:getChannelRest").to("log:input")
                .bean("sbnService", "getChannel");

        from("direct-vm:getSubRegistryRest").to("log:input")
                .bean("sbnService", "getSubRegistry");

        from("direct-vm:getParticipantRest").to("log:input")
                .bean("sbnService", "getParticipant");

        from("direct-vm:getTrnCodeRest").to("log:input")
                .bean("sbnService", "getTrnCode");

        from("direct-vm:getQuotaAllRest").to("log:input")
                .convertBodyTo(QuotaParam.class)
                .bean("sbnService", "getQuotaAll");

        from("direct-vm:getQuotaSidRest").to("log:input")
                .convertBodyTo(QuotaParam.class)
                .bean("sbnService", "getQuotaSid");

        /* Series */
        from("direct-vm:getSeriesRest").to("log:input")
                .bean("sbnService", "getSeries");

        from("direct-vm:getSeriesByIdRest").to("log:input")
                .bean("sbnService", "getSeriesById");

        from("direct-vm:getSeriesOfferRest").to("log:input")
                .bean("sbnService", "getSeriesOffer");

        from("direct-vm:getSeriesRedeemRest").to("log:input")
                .bean("sbnService", "getSeriesRedeem");

        from("direct-vm:getSeriesRedeemByIdRest").to("log:input")
                .bean("sbnService", "getSeriesRedeemById");

        from("direct-vm:getSeriesRedeemOfferRest").to("log:input")
                .bean("sbnService", "getSeriesRedeemOffer");

        /*investor*/

        from("direct-vm:getInvestorBySidRest").to("log:input")
                .bean("sbnService", "getInvestorBySid");

        from("direct-vm:getInvestorSelectionRest").to("log:input")
                .bean("sbnService", "getInvestorSelection");

        from("direct-vm:postInvestorRest").to("log:input")
                .bean("sbnService", "postInvestor");

        from("direct-vm:putInvestorEditRest").to("log:input")
                .bean("sbnService", "putInvestorEdit");

        from("direct-vm:putInvestorActivateRest").to("log:input")
                .bean("sbnService", "putInvestorActivate");

        from("direct-vm:putInvestorDeactivateRest").to("log:input")
                .bean("sbnService", "putInvestorDeactivate");

        /*Rekening Dana*/
        from("direct-vm:getInvestorBankSelectionRest").to("log:input")
                .convertBodyTo(InvestorBankParam.class)
                .bean("sbnService", "getInvestorBankSelection");

        from("direct-vm:getInvestorBankByIdRest").to("log:input")
                .bean("sbnService", "getInvestorBankById");

        from("direct-vm:postInvestorBankRest").to("log:input")
                .bean("sbnService", "postInvestorBank");

        from("direct-vm:putInvestorBankEditRest").to("log:input")
                .bean("sbnService", "putInvestorBankEdit");

        from("direct-vm:deleteInvestorBankRest").to("log:input")
                .bean("sbnService", "deleteInvestorBank");

        /*Rekening Surat Berharga*/

        from("direct-vm:getInvestorSecByIdRest").to("log:input")
                .bean("sbnService", "getInvestorSecById");

        from("direct-vm:getInvestorSecSelectionRest").to("log:input")
                .convertBodyTo(InvestorSecParam.class)
                .bean("sbnService", "getInvestorSecSelection");

        from("direct-vm:postInvestorSecRest").to("log:input")
                .bean("sbnService", "postInvestorSec");

        from("direct-vm:putInvestorSecEditRest").to("log:input")
                .bean("sbnService", "putInvestorSecEdit");

        from("direct-vm:deleteInvestorSecRest").to("log:input")
                .bean("sbnService", "deleteInvestorSec");


        /* Pemesanan */
        from("direct-vm:postOrderRest").to("log:input")
                .bean("sbnService", "postOrder");

        from("direct-vm:getOrderByIdSeriesAndTrnIdRest").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderByIdSeriesAndTrnId");

        from("direct-vm:getOrderRedeemBySidRest").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderRedeemBySid");

        from("direct-vm:getOrderSeriesBySeriesAndSidRest").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderSeriesBySeriesAndSid");

        from("direct-vm:getOrderSeriesBySeriesRest").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderSeriesBySeries");

        from("direct-vm:getOrderSeriesAllBySidRest").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderSeriesAllBySid");

        from("direct-vm:getOrderByOrderCodeRest").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderByOrderCode");

        from("direct-vm:getOrderBillingRest").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderBilling");

        /*Redeem*/
        from("direct-vm:postRedeemRest").to("log:input")
                .bean("sbnService", "postRedeem");

        from("direct-vm:geRedeemBySidRest").to("log:input")
                .convertBodyTo(RedeemParam.class)
                .bean("sbnService", "geRedeemBySid");

        from("direct-vm:geRedeemBySeriesAndSidRest").to("log:input")
                .convertBodyTo(RedeemParam.class)
                .bean("sbnService", "geRedeemBySeriesAndSid");

        from("direct-vm:geRedeemByRedeemCodeRest").to("log:input")
                .convertBodyTo(RedeemParam.class)
                .bean("sbnService", "geRedeemByRedeemCode");

        from("direct-vm:geRedeemBySeriesAndDateRest").to("log:input")
                .convertBodyTo(RedeemParam.class)
                .bean("sbnService", "geRedeemBySeriesAndDate");
    }
}
