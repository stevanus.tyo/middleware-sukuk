package id.co.bsm.route;


import id.co.bsm.processor.ListObjectWsdlProcessor;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.service.error.FaultProcessor;
import id.co.bsm.service.ws.SukukWsdlService;
import id.co.bsm.service.param.InvestorBankParam;
import id.co.bsm.service.param.OrderParam;
import id.co.bsm.service.param.QuotaParam;
import id.co.bsm.service.param.RedeemParam;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.cxf.interceptor.Fault;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class SbnWsdlRoute extends RouteBuilder {
    private String wsdluri = "cxf:/sbn/wsdl/api/wsdl?serviceClass=" + SukukWsdlService.class.getName();

    @Override
    public void configure() throws Exception {
        onException(Fault.class).handled(true).process(new FaultProcessor()).end();
        onException(BsmSoaException.class).handled(true).process(new FaultProcessor()).end();

        from(wsdluri).to("log:input")
                .recipientList(simple("direct-vm:${header.operationName}"))
                .process(new ListObjectWsdlProcessor());

        from("direct-vm:getOccupationWsdl").to("log:input")
                .bean("sbnService", "getOccupation");

        from("direct-vm:getSexWsdl").to("log:input")
                .bean("sbnService", "getSex");

        from("direct-vm:getProvinceWsdl").to("log:input")
                .bean("sbnService", "getProvince");

        from("direct-vm:getCityWsdl").to("log:input")
                .bean("sbnService", "getCity");

        from("direct-vm:getBankWsdl").to("log:input")
                .bean("sbnService", "getBank");

        from("direct-vm:getBankPerceptionWsdl").to("log:input")
                .bean("sbnService", "getBankPerception");

        from("direct-vm:getChannelWsdl").to("log:input")
                .bean("sbnService", "getChannel");

        from("direct-vm:getSubRegistryWsdl").to("log:input")
                .bean("sbnService", "getSubRegistry");

        from("direct-vm:getParticipantWsdl").to("log:input")
                .bean("sbnService", "getParticipant");

        from("direct-vm:getTrnCodeWsdl").to("log:input")
                .bean("sbnService", "getTrnCode");

        from("direct-vm:getQuotaAllWsdl").to("log:input")
                .bean("sbnService", "getQuotaAll");

        from("direct-vm:getQuotaSidWsdl").to("log:input")
                .convertBodyTo(QuotaParam.class)
                .bean("sbnService", "getQuotaSid");

        /* Series */
        from("direct-vm:getSeriesWsdl").to("log:input")
                .bean("sbnService", "getSeries");

        from("direct-vm:getSeriesByIdWsdl").to("log:input")
                .bean("sbnService", "getSeriesById");

        from("direct-vm:getSeriesOfferWsdl").to("log:input")
                .bean("sbnService", "getSeriesOffer");

        from("direct-vm:getSeriesRedeemWsdl").to("log:input")
                .bean("sbnService", "getSeriesRedeem");

        from("direct-vm:getSeriesRedeemByIdWsdl").to("log:input")
                .bean("sbnService", "getSeriesRedeemById");

        from("direct-vm:getSeriesRedeemOfferWsdl").to("log:input")
                .bean("sbnService", "getSeriesRedeemOffer");

        /*investor*/

        from("direct-vm:getInvestorBySidWsdl").to("log:input")
                .bean("sbnService", "getInvestorBySid");

        from("direct-vm:getInvestorSelectionWsdl").to("log:input")
                .bean("sbnService", "getInvestorSelection");

        from("direct-vm:postInvestorWsdl").to("log:input")
                .bean("sbnService", "postInvestor");

        from("direct-vm:putInvestorEditWsdl").to("log:input")
                .bean("sbnService", "putInvestorEdit");

        from("direct-vm:putInvestorActivateWsdl").to("log:input")
                .bean("sbnService", "putInvestorActivate");

        from("direct-vm:putInvestorDeactivateWsdl").to("log:input")
                .bean("sbnService", "putInvestorDeactivate");

        /*Rekening Dana*/
        from("direct-vm:getInvestorBankSelectionWsdl").to("log:input")
                .convertBodyTo(InvestorBankParam.class)
                .bean("sbnService", "getInvestorBankSelection");

        from("direct-vm:getInvestorBankByIdWsdl").to("log:input")
                .bean("sbnService", "getInvestorBankById");

        from("direct-vm:postInvestorBankWsdl").to("log:input")
                .bean("sbnService", "postInvestorBank");

        from("direct-vm:putInvestorBankEditWsdl").to("log:input")
                .bean("sbnService", "putInvestorBankEdit");

        from("direct-vm:deleteInvestorBankWsdl").to("log:input")
                .bean("sbnService", "deleteInvestorBank");

        /*Rekening Surat Berharga*/

        from("direct-vm:getInvestorSecByIdWsdl").to("log:input")
                .bean("sbnService", "getInvestorSecById");

        from("direct-vm:getInvestorSecSelectionWsdl").to("log:input")
                .bean("sbnService", "getInvestorSecSelection");

        from("direct-vm:postInvestorSecWsdl").to("log:input")
                .bean("sbnService", "postInvestorSec");

        from("direct-vm:putInvestorSecEditWsdl").to("log:input")
                .bean("sbnService", "putInvestorSecEdit");

        from("direct-vm:deleteInvestorSecWsdl").to("log:input")
                .bean("sbnService", "deleteInvestorSec");

        /* Pemesanan */
        from("direct-vm:postOrderWsdl").to("log:input")
                .bean("sbnService", "postOrder");

        from("direct-vm:getOrderByIdSeriesAndTrnIdWsdl").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderByIdSeriesAndTrnId");

        from("direct-vm:getOrderRedeemBySidWsdl").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderRedeemBySid");

        from("direct-vm:getOrderSeriesBySeriesAndSidWsdl").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderSeriesBySeriesAndSid");

        from("direct-vm:getOrderSeriesBySeriesWsdl").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderSeriesBySeries");

        from("direct-vm:getOrderSeriesAllBySidWsdl").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderSeriesAllBySid");

        from("direct-vm:getOrderByOrderCodeWsdl").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderByOrderCode");

        from("direct-vm:getOrderBillingWsdl").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderBilling");

        /*Redeem*/
        from("direct-vm:postRedeemWsdl").to("log:input")
                .bean("sbnService", "postRedeem");

        from("direct-vm:geRedeemBySidWsdl").to("log:input")
                .convertBodyTo(RedeemParam.class)
                .bean("sbnService", "geRedeemBySid");

        from("direct-vm:geRedeemBySeriesAndSidWsdl").to("log:input")
                .convertBodyTo(RedeemParam.class)
                .bean("sbnService", "geRedeemBySeriesAndSid");

        from("direct-vm:geRedeemByRedeemCodeWsdl").to("log:input")
                .convertBodyTo(RedeemParam.class)
                .bean("sbnService", "geRedeemByRedeemCode");

        from("direct-vm:geRedeemBySeriesAndDateWsdl").to("log:input")
                .convertBodyTo(RedeemParam.class)
                .bean("sbnService", "geRedeemBySeriesAndDate");
    }
}
