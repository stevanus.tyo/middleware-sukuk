package id.co.bsm.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.service.error.FaultProcessor;
import id.co.bsm.service.ws.SukukWsdlJService;
import id.co.bsm.service.param.InvestorBankParam;
import id.co.bsm.service.param.OrderParam;
import id.co.bsm.service.param.QuotaParam;
import id.co.bsm.service.param.RedeemParam;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.cxf.interceptor.Fault;
// import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class SbnWsdlJRoute extends RouteBuilder {
    private String wsdluri = "cxf:/sbn/wsdlj/api/wsdlj?serviceClass=" + SukukWsdlJService.class.getName();
    private ObjectMapper objectMapper = new ObjectMapper();
    
    @Override
    public void configure() throws Exception {
        onException(Fault.class).handled(true).process(new FaultProcessor()).end();
        onException(BsmSoaException.class).handled(true).process(new FaultProcessor()).end();

        from(wsdluri).to("log:input")
                .recipientList(simple("direct-vm:${header.operationName}"))
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        String jdata = objectMapper.writeValueAsString(exchange.getIn().getBody());
                        exchange.getOut().setBody(jdata);
                    }
                });

        from("direct-vm:getOccupationWsdlJ").to("log:input")
                .bean("sbnService", "getOccupation");

        from("direct-vm:getSexWsdlJ").to("log:input")
                .bean("sbnService", "getSex");

        from("direct-vm:getProvinceWsdlJ").to("log:input")
                .bean("sbnService", "getProvince");

        from("direct-vm:getCityWsdlJ").to("log:input")
                .bean("sbnService", "getCity");

        from("direct-vm:getBankWsdlJ").to("log:input")
                .bean("sbnService", "getBank");

        from("direct-vm:getBankPerceptionWsdlJ").to("log:input")
                .bean("sbnService", "getBankPerception");

        from("direct-vm:getChannelWsdlJ").to("log:input")
                .bean("sbnService", "getChannel");

        from("direct-vm:getSubRegistryWsdlJ").to("log:input")
                .bean("sbnService", "getSubRegistry");

        from("direct-vm:getParticipantWsdlJ").to("log:input")
                .bean("sbnService", "getParticipant");

        from("direct-vm:getTrnCodeWsdlJ").to("log:input")
                .bean("sbnService", "getTrnCode");

        from("direct-vm:getQuotaAllWsdlJ").to("log:input")
                .bean("sbnService", "getQuotaAll");

        from("direct-vm:getQuotaSidWsdlJ").to("log:input")
                .convertBodyTo(QuotaParam.class)
                .bean("sbnService", "getQuotaSid");

        /* Series */
        from("direct-vm:getSeriesWsdlJ").to("log:input")
                .bean("sbnService", "getSeries");

        from("direct-vm:getSeriesByIdWsdlJ").to("log:input")
                .bean("sbnService", "getSeriesById");

        from("direct-vm:getSeriesOfferWsdlJ").to("log:input")
                .bean("sbnService", "getSeriesOffer");

        from("direct-vm:getSeriesRedeemWsdlJ").to("log:input")
                .bean("sbnService", "getSeriesRedeem");

        from("direct-vm:getSeriesRedeemByIdWsdlJ").to("log:input")
                .bean("sbnService", "getSeriesRedeemById");

        from("direct-vm:getSeriesRedeemOfferWsdlJ").to("log:input")
                .bean("sbnService", "getSeriesRedeemOffer");

        /*investor*/

        from("direct-vm:getInvestorBySidWsdlJ").to("log:input")
                .bean("sbnService", "getInvestorBySid");

        from("direct-vm:getInvestorSelectionWsdlJ").to("log:input")
                .bean("sbnService", "getInvestorSelection");

        from("direct-vm:postInvestorWsdlJ").to("log:input")
                .bean("sbnService", "postInvestor");

        from("direct-vm:putInvestorEditWsdlJ").to("log:input")
                .bean("sbnService", "putInvestorEdit");

        from("direct-vm:putInvestorActivateWsdlJ").to("log:input")
                .bean("sbnService", "putInvestorActivate");

        from("direct-vm:putInvestorDeactivateWsdlJ").to("log:input")
                .bean("sbnService", "putInvestorDeactivate");

        /*Rekening Dana*/
        from("direct-vm:getInvestorBankSelectionWsdlJ").to("log:input")
                .convertBodyTo(InvestorBankParam.class)
                .bean("sbnService", "getInvestorBankSelection");

        from("direct-vm:getInvestorBankByIdWsdlJ").to("log:input")
                .bean("sbnService", "getInvestorBankById");

        from("direct-vm:postInvestorBankWsdlJ").to("log:input")
                .bean("sbnService", "postInvestorBank");

        from("direct-vm:putInvestorBankEditWsdlJ").to("log:input")
                .bean("sbnService", "putInvestorBankEdit");

        from("direct-vm:deleteInvestorBankWsdlJ").to("log:input")
                .bean("sbnService", "deleteInvestorBank");

        /*Rekening Surat Berharga*/

        from("direct-vm:getInvestorSecByIdWsdlJ").to("log:input")
                .bean("sbnService", "getInvestorSecById");

        from("direct-vm:getInvestorSecSelectionWsdlJ").to("log:input")
                .bean("sbnService", "getInvestorSecSelection");

        from("direct-vm:postInvestorSecWsdlJ").to("log:input")
                .bean("sbnService", "postInvestorSec");

        from("direct-vm:putInvestorSecEditWsdlJ").to("log:input")
                .bean("sbnService", "putInvestorSecEdit");

        from("direct-vm:deleteInvestorSecWsdlJ").to("log:input")
                .bean("sbnService", "deleteInvestorSec");

        /* Pemesanan */
        from("direct-vm:postOrderWsdlJ").to("log:input")
                .bean("sbnService", "postOrder");

        from("direct-vm:getOrderByIdSeriesAndTrnIdWsdlJ").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderByIdSeriesAndTrnId");

        from("direct-vm:getOrderRedeemBySidWsdlJ").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderRedeemBySid");

        from("direct-vm:getOrderSeriesBySeriesAndSidWsdlJ").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderSeriesBySeriesAndSid");

        from("direct-vm:getOrderSeriesBySeriesWsdlJ").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderSeriesBySeries");

        from("direct-vm:getOrderSeriesAllBySidWsdlJ").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderSeriesAllBySid");

        from("direct-vm:getOrderByOrderCodeWsdlJ").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderByOrderCode");

        from("direct-vm:getOrderBillingWsdlJ").to("log:input")
                .convertBodyTo(OrderParam.class)
                .bean("sbnService", "getOrderBilling");

        /*Redeem*/
        from("direct-vm:postRedeemWsdlJ").to("log:input")
                .bean("sbnService", "postRedeem");

        from("direct-vm:geRedeemBySidWsdlJ").to("log:input")
                .convertBodyTo(RedeemParam.class)
                .bean("sbnService", "geRedeemBySid");

        from("direct-vm:geRedeemBySeriesAndSidWsdlJ").to("log:input")
                .convertBodyTo(RedeemParam.class)
                .bean("sbnService", "geRedeemBySeriesAndSid");

        from("direct-vm:geRedeemByRedeemCodeWsdlJ").to("log:input")
                .convertBodyTo(RedeemParam.class)
                .bean("sbnService", "geRedeemByRedeemCode");

        from("direct-vm:geRedeemBySeriesAndDateWsdlJ").to("log:input")
                .convertBodyTo(RedeemParam.class)
                .bean("sbnService", "geRedeemBySeriesAndDate");
    }
}
