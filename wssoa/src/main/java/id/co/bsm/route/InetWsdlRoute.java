package id.co.bsm.route;

import id.co.bsm.processor.ListObjectWsdlProcessor;
import id.co.bsm.service.error.BsmSoaException;
import id.co.bsm.service.error.FaultProcessor;
import id.co.bsm.service.ws.InetWsdlService;
import org.apache.camel.builder.RouteBuilder;
import org.apache.cxf.interceptor.Fault;
import org.springframework.stereotype.Component;


@Component
public class InetWsdlRoute extends RouteBuilder {
    private String wsdluri = "cxf:/sbn/wsdl/inet/wsdl?serviceClass=" + InetWsdlService.class.getName();

    @Override
    public void configure() throws Exception {
        onException(Fault.class).handled(true).process(new FaultProcessor()).end();
        onException(BsmSoaException.class).handled(true).process(new FaultProcessor()).end();

        from(wsdluri).to("log:input")
                .recipientList(simple("direct-vm:${header.operationName}"))
                .process(new ListObjectWsdlProcessor());

        /* Investor */
        from("direct-vm:netInvestorStatusWsdl").to("log:input")
                .bean("inetService", "getInvestorStatus");

        from("direct-vm:netInvestorProfileWsdl").to("log:input")
                .bean("inetService", "getInvestorProfile");

        from("direct-vm:netInvestorNewWsdl").to("log:input")
                .bean("inetService", "postInvestorNew");

        from("direct-vm:netInvestorRegisterWsdl").to("log:input")
                .bean("inetService", "startInvestorRegister")
                .bean("inetService", "postSbnInvestorRegister") /* Sbn Api Register Investor */
                .bean("inetService", "postSbnInvestorBankRegister") /* Sbn Api Register Investor Sumber Dana */
                .bean("inetService", "postSbnInvestorSecRegister") /* Sbn Api Register Investor Surat Berharga */
                .bean("inetService", "endInvestorRegister");

        /* Series */
        from("direct-vm:netSeriesOfferWsdl").to("log:input")
                .bean("sbnService", "getSeriesOffer");

        from("direct-vm:netSeriesAllWsdl").to("log:input")
                .bean("sbnService", "getSeries");

        /* Quota */
        from("direct-vm:netQuotaWsdl").to("log:input")
                .bean("inetService", "getQuota");

        /* Order */
        from("direct-vm:netOrderWsdl").to("log:input")
                .bean("inetService", "postOrder");

        /* Order history */

        from("direct-vm:netOrderHistoryFromApiWsdl").to("log:input")
                .bean("inetService", "getHitoryFromApi");

        from("direct-vm:netOrderHistoryBySeriesFromApiWsdl").to("log:input")
                .bean("inetService", "getHitoryBySeriesFromApi");

        from("direct-vm:netOrderHistoryFromDbWsdl").to("log:input")
                .bean("inetService", "getHistoryFromDb");

        from("direct-vm:netOrderRedeemableWsdl").to("log:input")
                .bean("inetService", "getOrderRedeemable");

        /* Redeem */
        from("direct-vm:netRedeemWsdl").to("log:input")
                .bean("inetService", "readeem");

        /* Redeem history */
        from("direct-vm:netOrderRedemtionWsdl").to("log:input")
                .bean("inetService", "getRedeemByCif");

        from("direct-vm:netOrderRedemtionBySeriesWsdl").to("log:input")
                .bean("inetService", "getRedeemByCifAndSeries");

        /* all branch */
        from("direct-vm:netBranchAll").to("log:input")
                .bean("inetService", "getAllBranch");
    }
}
